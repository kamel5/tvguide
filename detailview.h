#ifndef __TVGUIDE_DETAILVIEW_H
#define __TVGUIDE_DETAILVIEW_H

#include <vdr/plugin.h>
#include <vdr/epg.h>
#include "config.h"
#include "tools.h"
#include "styledpixmap.h"
#include "footer.h"
#include "view.h"

// --- cDetailView  -------------------------------------------------------------

class cDetailView : public cThread {
private:
    const cEvent *event = NULL;
    const cRecording *recording = NULL;
    cFooter *footer = NULL;
    cView *view = NULL;
    void InitiateView(void);
    cList<Epgsearch_searchresults_v1_0::cServiceSearchResult> *LoadReruns(void);
    std::string RerunstoText(void);
    std::string LoadRecordingInformation(void);
    std::string StripXmlTag(std::string &Line, const char *Tag);
    int ReadSizeVdr(const char *strPath);
    void Action(void);
public:
    cDetailView(const cEvent *event = NULL, const cRecording *recording = NULL, int fromView = fromEpgGrid);
    virtual ~cDetailView(void);
    const cEvent *GetEvent(void) { return event; };
    eOSState ProcessKey(eKeys Key);
};

#endif //__TVGUIDE_DETAILVIEW_H
