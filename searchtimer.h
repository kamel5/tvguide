#ifndef __TVGUIDE_SEARCHTIMER_H
#define __TVGUIDE_SEARCHTIMER_H


class cTVGuideSearchTimer {
friend class cRecMenuSearchTimerEdit;
protected:
    std::string strTimer;
    int         ID;
    std::string searchString;
    int         useTime;
    int         startTime;
    int      	stopTime;
    int         useChannel;
    const cChannel *channelMin;
    const cChannel *channelMax;
    std::string channelGroup;
    int      	useCase;
    int      	mode;
    int         useTitle;
    int         useSubtitle;
    int         useDescription;
    int         useDuration;
    int         minDuration;
    int         maxDuration;
    int         useAsSearchTimer;
    int         useDayOfWeek;
    int         dayOfWeek;
    int         useEpisode;
    std::string directory;
    int         priority;
    int         lifetime;
    int         marginStart;
    int         marginStop;
    int         useVPS;
    int         action;
    int         useExtEPGInfo;
    std::string extEPGInfoValues;
    int         avoidRepeats;
    int         allowedRepeats;
    int         compareTitle;
    int         compareSubtitle;
    int         compareSummary;
    unsigned long catvaluesAvoidRepeat;
    int         repeatsWithinDays;
    int         delAfterDays;
    int         recordingsKeep;
    int         switchMinsBefore;
    int         pauseOnNrRecordings;
    int         blacklistMode;
    std::string blacklists;
    int         fuzzyTolerance;
    int         useInFavorites;
    int         menuTemplate;
    int         delMode;
    int         delAfterCountRecs;
    int         delAfterDaysOfFirstRec;
    int         useAsSearchTimerFrom;
    int         useAsSearchTimerTil;
    int         ignoreMissingEPGCats;
    int         unmuteSoundOnSwitch;
    int         compareSummaryMatchInPercent;
    std::string contentsFilter;
    int         compareDate;
public:
    cTVGuideSearchTimer(void);
    virtual ~cTVGuideSearchTimer(void);
    bool operator < (const cTVGuideSearchTimer& other) const;
    void SetEPGSearchString(std::string strTimer) { this->strTimer = strTimer; };
    void SetTemplate(std::string tmpl);
    bool Parse(bool readTemplate = false);
    std::string BuildSearchString(void);
    int GetID(void) { return ID; };
    //GETTER
    std::string GetSearchString(void) const { return searchString; };
    bool IsActive(void);
    bool UseInFavorites(void) { return useInFavorites; };
    //SETTER
    void SetSearchString(std::string searchString) { this->searchString = searchString; };
    //COMMON
    int GetNumTimers(void);
    int GetNumRecordings(void);
    void GetSearchModes(std::vector<std::string> *searchModes);
    void GetSubTitleModes(std::vector<std::string> *subTitleModes);
    void GetUseChannelModes(std::vector<std::string> *useChannelModes);
    void GetSearchTimerModes(std::vector<std::string> *searchTimerModes);
    void GetCompareDateModes(std::vector<std::string> *compareDateModes);
    void GetDelModes(std::vector<std::string> *delModes);
    void Dump(void);
};

#endif //__TVGUIDE_SEARCHTIMER_H
