#ifndef __TVGUIDE_HEADERGRID_H
#define __TVGUIDE_HEADERGRID_H

#include "gridelement.h"

// --- cHeaderGrid  -------------------------------------------------------------

class cHeaderGrid : public cGridElement {
private:
    cPixmap *pixmapLogo;
    void DrawChannelHorizontal(const cChannel *channel);
    void DrawChannelVertical(const cChannel *channel);
public:
    cHeaderGrid(void);
    virtual ~cHeaderGrid(void);
    void CreateBackground(int num);
    void DrawChannel(const cChannel *channel);
    void SetPosition(int num);
};

#endif //__TVGUIDE_HEADERGRID_H
