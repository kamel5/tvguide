#include <stdlib.h>
#include <vector>

#include "config.h"
#include "services/epgsearch.h"
#include "services/remotetimers.h"

#include "tools.h"
#include "setup.h"

#include "tvguideosd.h"

cTvGuideOsd::cTvGuideOsd(void) {
    osdManager = NULL;
}

cTvGuideOsd::~cTvGuideOsd() {
    delete timeManager;
    channels.Clear();
    if (config.displayStatusHeader) {
        delete statusHeader;
    }
    if (detailView)
        delete detailView;
    delete timeLine;
    delete channelGroups;
    delete footer;
    delete recMenuView;
    if (channelJumper)
        delete channelJumper;
    delete osdManager;
}

void cTvGuideOsd::Show(void) {
    int start = cTimeMs::Now();
    bool ok = false;
    config.SetDefaultPathes();
    config.SetDynamicValues();
    osdManager = new cOsdManager();
    ok = osdManager->CreateOsd();
    if (ok) {
        pRemoteTimers = cPluginManager::GetPlugin("remotetimers");
        if (pRemoteTimers) {
            isyslog("tvguide: remotetimers-plugin is available");
        }
        if (config.useRemoteTimers && pRemoteTimers) {
            cString errorMsg;
            if (!pRemoteTimers->Service("RemoteTimers::RefreshTimers-v1.0", &errorMsg)) {
                esyslog("tvguide: %s", *errorMsg);
            }
        }

        SwitchTimers.Load(AddDirectory(cPlugin::ConfigDirectory("epgsearch"), "epgsearchswitchtimers.conf"));
        recMenuView = new cRecMenuView();

        bool geoChanged = geoManager.SetGeometry(cOsd::OsdWidth(), cOsd::OsdHeight());
        timeManager = new cTimeManager();

        bool themeChanged = config.LoadTheme();
        config.SetStyle();
        if (themeChanged || geoChanged || !fontManager || !imgCache) {
            delete fontManager;
            fontManager = new cFontManager();
            delete imgCache;
            imgCache = new cImageCache();
        }
        timeLine = new cTimeLine();
        channelGroups = new cChannelGroups();
        Button_Parameter_t bp;
        bp.top = geoManager.footerY;
        bp.width = geoManager.osdWidth;
        bp.height = geoManager.footerHeight;
        bp.channelGroups = channelGroups;
        footer = new cFooter(bp);
        DrawOsd();
    }
    esyslog("tvguide: Rendering took %d ms", int(cTimeMs::Now()-start));
}

void cTvGuideOsd::DrawOsd() {
    int numBack = config.numGrids / 2;
    int offset = 0;
#if VDRVERSNUM >= 20301
    const cChannels *allchannels = NULL;
    {
    LOCK_CHANNELS_READ;
    allchannels = Channels;
    }
#else
    cChannels *allchannels = &Channels;
#endif
    const cChannel *startChannel = allchannels->GetByNumber(cDevice::CurrentChannel());
    for (; startChannel ; startChannel = allchannels->Prev(startChannel)) {
        if (startChannel && !startChannel->GroupSep()) {
            offset++;
        }
        if (offset == numBack)
            break;
    }
    if (!startChannel)
        startChannel = allchannels->First();
    offset--;
    if (offset < 0)
        offset = 0;
    
    CreateChannels(startChannel, offset, true);
}

void cTvGuideOsd::CreateChannels(const cChannel *channelStart, int activeChannel, bool init) {
    dsyslog ("tvguide: %s %s %d %s\n", __FILE__, __func__,  __LINE__, channelStart->Name());
    int i = 0;
    bool foundEnough = false;
    channels.Clear();
    if (!channelStart)
        return;
#if VDRVERSNUM >= 20301
    const cChannels *allchannels = NULL;
    {
    LOCK_CHANNELS_READ;
    allchannels = Channels;
    }
#else
    cChannels *allchannels = &Channels;
#endif
    for (const cChannel *channel = channelStart; channel; channel = allchannels->Next(channel)) {
        if (!channel->GroupSep()) {
            if (channelGroups->IsInLastGroup(channel)) {
                break;
            }
            cChannelEpg *channelEpg = new cChannelEpg(i, channel);
            if (channelEpg->ReadGrids()) {
                channels.Add(channelEpg);
                i++;
            } else {
                delete channelEpg;
            }
        }
        if (i == config.numGrids) {
            foundEnough = true;
            break;
        }
    }
    if (!foundEnough) {
        int numCurrent = channels.Count();
        int numBack = config.numGrids - numCurrent;
        int newChannelNumber = channels.First()->GetChannel()->Number() - numBack;
        const cChannel *newStart = allchannels->GetByNumber(newChannelNumber);
        CreateChannels(newStart, activeChannel);
    } else {
        cPixmap::Lock();
        if (config.displayStatusHeader) {
            DELETENULL(statusHeader);
            statusHeader = new cStatusHeader();
            statusHeader->Draw();
            osdManager->ScaleVideo();
        }
        timeLine->Draw();
        DrawGridsChannelJump(activeChannel);
        osdManager->SetBackground();
        osdManager->Flush();
        cPixmap::Unlock();
    }
}

void cTvGuideOsd::DrawGridsChannelJump(int offset) { // OK
    if (channels.Count() == 0)
        return;
    activeGrid = channels.Get(offset)->GetActive();
    if (activeGrid)
        activeGrid->SetActive();
    if (config.displayStatusHeader) {
        statusHeader->DrawInfoText(activeGrid);
    }
    if (config.displayChannelGroups) {
        channelGroups->Draw(channels.First()->GetChannel(), channels.Last()->GetChannel());
    }
    for (cChannelEpg *channelEpg = channels.First(); channelEpg; channelEpg = channels.Next(channelEpg)) {
        channelEpg->CreateHeader();
        channelEpg->DrawGrids();
    }
    if (activeGrid && (config.channelJumpMode == eGroupJump)) {
        footer->UpdateGroupButtons(activeGrid->owner->GetChannel());
    }
}

void cTvGuideOsd::DrawGridsTimeJump(bool last) { // OK
    if (channels.Count() == 0)
        return;
    cChannelEpg *colActive = NULL;
    if (activeGrid) {
        colActive = activeGrid->owner;
    } else {
        colActive = channels.First();
    }
    for (cChannelEpg *channelEpg = channels.First(); channelEpg; channelEpg = channels.Next(channelEpg)) {
        channelEpg->ClearGrids();
        channelEpg->ReadGrids();
        channelEpg->DrawGrids();
    }
    activeGrid = colActive->GetActive(last);
    if (activeGrid) {
        activeGrid->SetActive();
        activeGrid->Draw();
        if (config.displayStatusHeader) {
            statusHeader->DrawInfoText(activeGrid);
        }
    }
    timeLine->Draw();
}

void cTvGuideOsd::SetActiveGrid(cGridElement *newActive) {
    if (!newActive || !activeGrid) {
        return;
    }
    activeGrid->SetInActive();
    activeGrid->Draw(); 
    activeGrid = newActive;
    activeGrid->SetActive();
    activeGrid->Draw();
    if (config.displayStatusHeader) {
        statusHeader->DrawInfoText(activeGrid);
    }
}

void cTvGuideOsd::ChannelStep(int channelDirection) { // OK
    if (!activeGrid)
        return;
    cChannelEpg *channel = (channelDirection == eForward) ? channels.Next(activeGrid->owner)
                                                          : channels.Prev(activeGrid->owner);
    if (channel) {
        cGridElement *neighbor = channel->GetNeighbor(activeGrid);
        if (neighbor) {
            SetActiveGrid(neighbor);
            if (config.channelJumpMode == eGroupJump)
                footer->UpdateGroupButtons(activeGrid->owner->GetChannel());
        }
        osdManager->Flush();
        return;
    }
    //start/end of grid reached, scrolling one channel or half page back/forward
    int numJump = 1;
    const cChannel *currentChannel = activeGrid->owner->GetChannel();
    //insert new channels at end/start
    int numInserted = 0;
    cChannelEpg *channelEpg;
    const cChannels *allChannels;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    allChannels = Channels;
    }
#else
    allChannels = &Channels;
#endif
    for (const cChannel *channel = (channelDirection == eForward) ? (const cChannel*)currentChannel->Next() : (const cChannel*)currentChannel->Prev(); channel ; channel = (channelDirection == eForward) ? allChannels->Next(channel) : allChannels->Prev(channel)) {
        if (channel->GroupSep()) {
            continue;
        }
        if ((channelDirection == eForward) && config.hideLastGroup && channelGroups->IsInLastGroup(channel)) {
            break;
        }
        channelEpg = new cChannelEpg((channelDirection == eForward) ? config.jumpChannels - 1 : 0, channel);
        if (channelEpg->ReadGrids()) {
            (channelDirection == eForward) ? channels.Add(channelEpg)
	                                   : channels.Ins(channelEpg, channels.First());
            channelEpg->CreateHeader();
            numInserted++;
            if (numInserted == 1) {
                cGridElement *neighbor = channelEpg->GetNeighbor(activeGrid);
                if (neighbor) {
                    SetActiveGrid(neighbor);
                }
            }
        } else {
            delete channelEpg;
            channelEpg = NULL;
        }
        if (numInserted == numJump)
            break;
    }
    //delete first/last channels
    for (int i = 0; i < numInserted; i++) {
        cChannelEpg *channel = (channelDirection == eForward) ? channels.First() : channels.Last();
        channels.Del(channel);
    }
    //renumber channels
    int newPos = 0;
    for (cChannelEpg *channelEpg = channels.First(); channelEpg; channelEpg = channels.Next(channelEpg)) {
        channelEpg->SetNum(newPos);
        channelEpg->DrawHeader();
        channelEpg->DrawGrids();
        newPos++;
    }
    if (activeGrid && config.channelJumpMode == eGroupJump)
        footer->UpdateGroupButtons(activeGrid->owner->GetChannel());
    if (config.displayChannelGroups)
        channelGroups->Draw(channels.First()->GetChannel(), channels.Last()->GetChannel());
    osdManager->Flush();
}

void cTvGuideOsd::TimeStep(int timeDirection) { // OK
    if (!activeGrid) {
        return;
    }
    bool actionDone = false;
    time_t time = (timeDirection == eBack) ? (activeGrid->StartTime() - timeManager->GetStart())
                                           : (timeManager->GetEnd() - activeGrid->EndTime());
    if (time / 60 < 30 ) {
        Scroll(timeDirection);
        actionDone = true;
    }
    cGridElement *element = (timeDirection == eBack) ? activeGrid->owner->GetPrev(activeGrid)
                                                     : activeGrid->owner->GetNext(activeGrid);
    if (element) {
        if ((timeDirection == eBack) ? ((element->StartTime() > timeManager->GetStart()) || ((element->EndTime() - timeManager->GetStart()) / 60 >= 30) || (element->IsFirst()))
                                     : ((element->EndTime() < timeManager->GetEnd()) || ( (timeManager->GetEnd() - element->StartTime()) / 60 >= 30))) {
            SetActiveGrid(element);
            actionDone = true;
        }
    }
    if (!actionDone) {
        Scroll(timeDirection);
    }
    osdManager->Flush();
}

void cTvGuideOsd::Scroll(int scrollMode) {
    timeManager->ScrollMinutes(eTime, scrollMode);
    if (config.useHWAccel) {
        DrawGridsTimeJump(scrollMode);
        return;
    }
    timeLine->Draw();
    for (cChannelEpg *channelEpg = channels.First(); channelEpg; channelEpg = channels.Next(channelEpg)) {
        if (scrollMode) {
            channelEpg->AddNewGridsAtEnd();
            channelEpg->ClearOutdatedStart();
        } else {
            channelEpg->AddNewGridsAtStart();
            channelEpg->ClearOutdatedEnd();
        }
        channelEpg->DrawGrids();
    }
}

const cChannel *cTvGuideOsd::GetChannelNumJump(int &activeChannel, int seekMode) {
//    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    const cChannel *currentChannel = channels.First()->GetChannel();
    const cChannel *destChannel = NULL;
    int found = 0;
    LOCK_CHANNELS_READ;
    const cChannels* allChannels = Channels;
    for (destChannel = currentChannel; destChannel ; destChannel = (seekMode) ? allChannels->Next(destChannel) : allChannels->Prev(destChannel)) {
        if (destChannel->GroupSep()) {
            continue;
        }
        if (seekMode && channelGroups->IsInLastGroup(destChannel)) {
            destChannel = allChannels->GetByNumber(GetLastValidChannel());
            activeChannel = config.numGrids - 1;
            break;
        }
        if (found == config.jumpChannels)
            break;
        found++;
    }
    if (!destChannel)
        if (seekMode) {
            destChannel = allChannels->Last();
            activeChannel = config.numGrids - 1;
        } else {
            destChannel = allChannels->First();
            activeChannel = 0;
        }
    return destChannel;
}

const cChannel *cTvGuideOsd::GetChannelGroupJump(int seekMode) {
  //    dsyslog ("tvguide: %s %s %d seekmode = %i\n", __FILE__, __func__,  __LINE__, seekMode);
      LOCK_CHANNELS_READ;

      if (!activeGrid)
          return (seekMode) ? Channels->Last() : Channels->First();

      int currentGroup = channelGroups->GetGroup(activeGrid->owner->GetChannel());
      int ChannelNumber = (seekMode) ? channelGroups->GetNextGroupFirstChannel(currentGroup)
                                     : channelGroups->GetPrevGroupFirstChannel(currentGroup);
      const cChannel *channel = Channels->GetByNumber(ChannelNumber);
      if (channel)
          return channel;
      return (seekMode) ? Channels->Last(): Channels->First();
  }

void cTvGuideOsd::Key1(int key) {
//    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    if (activeGrid == NULL)
        return;

    int activeChannel = activeGrid->owner->GetNum(); //0;
    const cChannel *nextStart = NULL;
    if (config.channelJumpMode == eNumJump) {
        nextStart = GetChannelNumJump(activeChannel, key == kYellow);
    } else if (config.channelJumpMode == eGroupJump) {
        switch (key) {
            case kGreen:  if (channelGroups->IsInFirstGroup(activeGrid->owner->GetChannel())) return;
                          nextStart = GetChannelGroupJump(eBack); break;
            case kYellow: if (channelGroups->IsInLastGroup(activeGrid->owner->GetChannel()) || (config.hideLastGroup && channelGroups->IsInSecondLastGroup(activeGrid->owner->GetChannel()))) return;
                          nextStart = GetChannelGroupJump(eForward); break;
            default:      break;
        }
        activeChannel = 0;
    }
    if (nextStart) {
        CreateChannels(nextStart, activeChannel);
    }
}

void cTvGuideOsd::KeyRed() {
    if  ((activeGrid == NULL) || activeGrid->IsDummy())
        return;
    recMenuView->Start(activeGrid->GetEvent());
}

eOSState cTvGuideOsd::KeyBlue(const cEvent *e) {
    if (config.blueKeyMode == eBlueKeySwitch) {
        return ChannelSwitch(e);
    } else if (config.blueKeyMode == eBlueKeyEPG) {
        DetailView(e);
    } else if (config.blueKeyMode == eBlueKeyFavorites) {
        recMenuView->DisplayFavorites();
    }
    return osContinue;
}

eOSState cTvGuideOsd::KeyInfo(const cEvent *e) {
    DetailView(e);
    return osContinue;
}

eOSState cTvGuideOsd::KeyOk(const cEvent *e) {
    if (config.blueKeyMode == eBlueKeySwitch) {
        DetailView(e);
    } else if (config.blueKeyMode == eBlueKeyEPG) {
        return ChannelSwitch(e);
    } else if (config.blueKeyMode == eBlueKeyFavorites) {
        DetailView(e);
    }
    return osContinue;
}

eOSState cTvGuideOsd::ChannelSwitch(const cEvent *e) {
    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    bool running = false;
    if (e) {
        time_t now = time(0);
        if (((e->StartTime() - 5 * 60) <= now) && (e->EndTime() >= now))
            running = true;
    }
    eOSState state = osContinue;
    if (running) { // || !config.intelligentSwitch) {
        const cChannel *currentChannel = NULL;
        {
        LOCK_CHANNELS_READ;
        currentChannel = Channels->GetByChannelID(e->ChannelID());
        }
        if (!currentChannel) {
            return state;
        }
        if (cDevice::PrimaryDevice()->CurrentChannel() == currentChannel->Number()) {
            return state;
        }
        if (config.closeOnSwitch) {
            state = osEnd;
        }
        cDevice::PrimaryDevice()->SwitchChannel(currentChannel, true);
    } else {
        if (detailView)
            CloseDetailedView();
//        CreateSwitchTimer(e);
        SetTimers();
        osdManager->Flush();
    }
    return state;
}

eOSState cTvGuideOsd::ChannelSwitch(bool *alreadyUnlocked) {
    if (activeGrid == NULL)
        return osContinue;
    const cChannel *currentChannel = activeGrid->owner->GetChannel();
    if (currentChannel) {
        cPixmap::Unlock();
        *alreadyUnlocked = true;
        cDevice::PrimaryDevice()->SwitchChannel(currentChannel, true);
        if (config.closeOnSwitch) {
            DELETENULL(detailView);
            return osEnd;
        }
    }
    return osContinue;
}

void cTvGuideOsd::DetailView(const cEvent *e, const cRecording *r) {
    if (!e && !r)
        return;

    if (e && activeGrid->IsDummy())
        return;

    if (recMenuView->IsActive())
        recMenuView->Hide();
    detailView = new cDetailView(e, r, (recMenuView->IsActive()) ? fromRecMenu : fromEpgGrid);
    detailView->Start();
}

void cTvGuideOsd::CloseDetailedView(void) {
    DELETENULL(detailView);
    if (recMenuView->IsActive()) {
        recMenuView->Activate(true);
        osdManager->Flush();
    } else
        osdManager->Flush();
}

void cTvGuideOsd::DetailedEPG() {
    if (!activeGrid->IsDummy()) {
        detailView = new cDetailView(activeGrid->GetEvent());
        detailView->Start();
//        osdManager->Flush();
    }
}

void cTvGuideOsd::NumericKey(eKeys key) {
    if (config.numkeyMode == 0) {
        switch (key) {
//            case k2: CreateInstantTimer(); break;
//            case k5: DisplaySearchRecordings(); break;
//            case k8: DisplaySearchEPG(); break;
            case k1: case k3: case k4:
            case k6: case k7: case k9: TimeJump(key); break; //timely jumps with 1,3,4,6,7,9
            default: break;
        }
    } else {
        //jump to specific channel
        ChannelJump(key - k0);
    }
}

void cTvGuideOsd::TimeJump(eKeys key) {
    bool rebuildgrid = true;
    switch (key) {
        case k1:
        case kFastRew: {
                 rebuildgrid = timeManager->ScrollMinutes(eTimeBig, eBack);
             }
             break;
        case k3:
        case kFastFwd: {
                 rebuildgrid = timeManager->ScrollMinutes(eTimeBig, eForward);
             }
             break;
        case k4:
        case kPrev: {
                 rebuildgrid = timeManager->ScrollMinutes(eTimeHuge, eBack);
             }
             break;
        case k6:
        case kNext: {
                 rebuildgrid = timeManager->ScrollMinutes(eTimeHuge, eForward);
             }
             break;
        case k7: {
             time_t prevPrime = timeManager->GetPrevPrimetime(timeManager->GetStart());
             if (timeManager->TooFarInPast(prevPrime))
                 return;
             timeManager->SetTime(prevPrime);
             }
             break;
        case k9: {
             time_t nextPrime = timeManager->GetNextPrimetime(timeManager->GetStart());
             timeManager->SetTime(nextPrime);
             }
             break;
        default:
             return;
    }
    if (rebuildgrid) {
        DrawGridsTimeJump();
        osdManager->Flush();
    }
}

int cTvGuideOsd::GetLastValidChannel(void) {
    return channelGroups->GetLastValidChannel();
}

void cTvGuideOsd::ChannelJump(int key) {
    if (!channelJumper) {
        int lastValidChannel = GetLastValidChannel();
        channelJumper = new cChannelJump(lastValidChannel);
    }
    channelJumper->Set(key);
    osdManager->Flush();
}

void cTvGuideOsd::CheckTimeout(void) {
    if (!channelJumper)
        return;
    if (!channelJumper->TimeOut())
        return;
    int newChannelNum = channelJumper->GetChannel(); 
    DELETENULL(channelJumper);
    const cChannel *newChannel = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    newChannel = Channels->GetByNumber(newChannelNum);
    }
#else
    newChannel = Channels.GetByNumber(newChannelNum);
#endif
    if (!newChannel)
        return;
    CreateChannels(newChannel);
}

void cTvGuideOsd::SetTimers() {
    for (cChannelEpg *channelEpg = channels.First(); channelEpg; channelEpg = channels.Next(channelEpg)) {
        channelEpg->SetTimers();
    }
}

eOSState cTvGuideOsd::ProcessKey(eKeys Key) {
    eOSState state = osContinue;
    cPixmap::Lock();
    bool alreadyUnlocked = false;
    if (detailView) {
        //Detail View Key Handling
        switch (Key & ~k_Repeat) {
            case kRed:
                CloseDetailedView();
                KeyRed();
                break;
            case kBlue:
                if (!(detailView->GetEvent()) || (config.blueKeyMode == eBlueKeyEPG)) {
                    CloseDetailedView();
		} else if ((config.blueKeyMode == eBlueKeySwitch) || (config.blueKeyMode == eBlueKeyFavorites)) {
                    state = ChannelSwitch(detailView->GetEvent());
                }
                break;
            case kOk:
                if ((detailView->GetEvent()) && (config.blueKeyMode == eBlueKeyEPG)) {
                    state = ChannelSwitch(activeGrid->GetEvent());
                } else {
                    CloseDetailedView();
                }
                break;
            case kInfo:
            case kBack:
                CloseDetailedView();
                break;
            default:
                state = detailView->ProcessKey(Key);
                if (state == osEnd) {
                    CloseDetailedView();
                }
                break;
        }
    } else if (recMenuView->IsActive()) {
        state = recMenuView->ProcessKey(Key);
        if (state == osEnd) {
            SetTimers();
            osdManager->Flush();
        } else if (state == osUser1) {
            DetailView(recMenuView->GetEvent());
        } else if (state == osUser3) {
            DetailView(NULL, recMenuView->GetRecording());
        }
        state = osContinue;
   } else {
        switch (Key & ~k_Repeat) {
            case kLeft:     (config.displayMode == eVertical) ? ChannelStep(eBack) : TimeStep(eBack);
                            break;
            case kRight:    (config.displayMode == eVertical) ? ChannelStep(eForward) : TimeStep(eForward);
                            break;
            case kUp:       (config.displayMode == eVertical) ? TimeStep(eBack) : ChannelStep(eBack);
                            break;
            case kDown:     (config.displayMode == eVertical) ? TimeStep(eForward) : ChannelStep(eForward);
                            break;
            case kRed:      KeyRed(); break;
            case kGreen:
            case kYellow:   Key1(Key); break;
            case kInfo:     state = KeyInfo(activeGrid->GetEvent()); break;
            case kBlue:     state = KeyBlue(activeGrid->GetEvent()); break;
            case kOk:       state = KeyOk(activeGrid->GetEvent()); break;
            case kBack:     state = osEnd; break;
            case k0 ... k9: NumericKey(Key); break;
            case kFastRew:  // Doesnt work without patch, if used from timeshiftmode
            case kFastFwd:
            case kPrev:
            case kNext:     TimeJump(Key); break;
            case kNone:     if (channelJumper) CheckTimeout(); break;
            default:        break;
        }
        if (timeLine->DrawClock()) {
            osdManager->Flush();
        }
    }
    if (!alreadyUnlocked) {
        cPixmap::Unlock();
    }
    return state;
}

void cTvGuideOsd::Dump() {
    esyslog("tvguide: ------Dumping Content---------");
    activeGrid->Debug();
//    int i=1;
    for (cChannelEpg *col = channels.First(); col; col = channels.Next(col)) {
        col->DumpGrids();
    }
}
