#ifndef __TVGUIDE_STYLEDPIXMAP_H
#define __TVGUIDE_STYLEDPIXMAP_H

#include <vdr/osd.h>
#include <vdr/epg.h>

enum eBackgroundType {
    bgGrid,
    bgStatusHeaderFull,
    bgStatusHeaderWindowed,
    bgChannelHeader,
    bgChannelGroup,
    bgClock,
    bgButton,
    bgRecMenuBack,
    bgChannelJump,
};

// --- cStyledPixmap -------------------------------------------------------------

class cStyledPixmap {
private:
    void DrawVerticalLine(int x, int yStart, int yStop, tColor col);
    void DrawHorizontalLine(int y, int xStart, int xStop, tColor col);
    void DrawBackgroundButton(bool active);
protected:
    cPixmap *pixmap;
    tColor color;
    tColor colorBlending;
    void SetPixmap(cPixmap *pixmap);
public:
    cStyledPixmap(void);
    cStyledPixmap(cPixmap *pixmap);
    virtual ~cStyledPixmap(void);
    void DrawBackground(void);
    void DrawBackgroundGraphical(eBackgroundType type, bool active = false);
    void DrawBlendedBackground(void);
    void DrawSparsedBackground(void);
    void DrawBorder(void);
    void DrawBoldBorder(void);
    void DrawDefaultBorder(int width, int height);
    void DrawRoundedCorners(int width, int height, int radius);
    void SetColor(tColor color, tColor colorBlending) { this->color = color; this->colorBlending = colorBlending; };
    void SetAlpha(int alpha) { pixmap->SetAlpha(alpha); };
    void SetLayer(int layer) { pixmap->SetLayer(layer); };
    void Fill(tColor clr) { pixmap->Fill(clr); };
    void DrawText(const cPoint &Point, const char *s, tColor ColorFg, tColor ColorBg, const cFont *Font, int Width = 0, int Height = 0, int Alignment = taDefault);
    void DrawImage(const cPoint &Point, const cImage &Image);
    void DrawRectangle(const cRect &Rect, tColor Color);
    void DrawEllipse(const cRect &Rect, tColor Color, int Quadrant);
    void SetViewPort(const cRect &Rect);
    int Width(void) { return (pixmap) ? pixmap->ViewPort().Width() : 0; };
    int Height(void) { return (pixmap) ? pixmap->ViewPort().Height() : 0; };
};

#endif //__TVGUIDE_STYLEDPIXMAP_H
