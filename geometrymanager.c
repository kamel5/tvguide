#include <vdr/osd.h>

#include "config.h"
#include "geometrymanager.h"

cGeometryManager::cGeometryManager() {
    osdWidth = 0;
    osdHeight = 0;
}

cGeometryManager::~cGeometryManager() {
}

bool cGeometryManager::SetGeometry(int osdWidth, int osdHeight, bool force) {
    if (!force && (this->osdWidth == osdWidth) && (this->osdHeight == osdHeight)) {
        esyslog("tvgudie: GeoManager SetGeometry nothing to change");
        return false;
    }
    this->osdWidth = osdWidth;
    this->osdHeight = osdHeight;
    esyslog("tvguide: Set OSD to %d x %d px", osdWidth, osdHeight);

    statusHeaderHeight = (config.displayStatusHeader) ? (config.headerHeightPercent * osdHeight / 100) : 0;
    tvFrameWidth = statusHeaderHeight * 16 / 9;
    headerContentWidth = (config.scaleVideo) ? (osdWidth -  tvFrameWidth):osdWidth;
    channelGroupsWidth = (config.displayChannelGroups) ? (config.channelGroupsPercent * osdWidth / 100) : 0;
    channelGroupsHeight = (config.displayChannelGroups) ? (config.channelGroupsPercent * osdHeight / 100) : 0;
    channelHeaderWidth = config.channelHeaderWidthPercent * osdWidth / 100;
    channelHeaderHeight = config.channelHeaderHeightPercent * osdHeight / 100;
    timeLineWidth = config.timeLineWidthPercent * osdWidth / 100;
    timeLineHeight = config.timeLineHeightPercent * osdHeight / 100;
    footerHeight = config.footerHeightPercent * osdHeight / 100;
    footerY = osdHeight - footerHeight;

    if (config.displayMode == eVertical) {
        colWidth = (osdWidth - timeLineWidth) / config.channelCols;
        rowHeight = 0;
        minutePixel = (double)(osdHeight - statusHeaderHeight - channelGroupsHeight - channelHeaderHeight - footerHeight) / (double)config.displayMinutesVertical;
        channelLogoWidth = colWidth;
        channelLogoHeight = channelHeaderHeight;
        logoWidth = channelLogoWidth / 2 - 15;
        logoHeight = logoWidth * config.logoHeightRatio / config.logoWidthRatio;
        timeLineGridWidth = timeLineWidth;
        timeLineGridHeight = minutePixel * 30;
        dateVieverWidth = timeLineWidth;
        dateVieverHeight = (channelHeaderHeight + channelGroupsHeight) * 2 / 3;
        clockWidth = dateVieverWidth;
        clockHeight = (channelHeaderHeight + channelGroupsHeight) - dateVieverHeight;
    } else if (config.displayMode == eHorizontal) {
        colWidth = 0;
        rowHeight = (osdHeight - statusHeaderHeight - timeLineHeight - footerHeight) / config.channelRows;
        minutePixel = (double)(osdWidth - channelHeaderWidth - channelGroupsWidth) / (double)config.displayMinutesHorizontal;
        channelLogoWidth = channelHeaderWidth;
        channelLogoHeight = rowHeight;
        logoWidth = channelLogoHeight * config.logoWidthRatio / config.logoHeightRatio;
        logoHeight = channelLogoHeight;
        timeLineGridWidth = geoManager.minutePixel * 30;
        timeLineGridHeight = geoManager.timeLineHeight;
        dateVieverWidth = (channelHeaderWidth + channelGroupsWidth) * 3 / 5;
        dateVieverHeight = timeLineHeight;
        clockWidth = (channelHeaderWidth + channelGroupsWidth) - dateVieverWidth;
        clockHeight = timeLineHeight;
    }
    buttonBorder = footerHeight / 6;
    buttonWidth = osdWidth / 4 - 2 * buttonBorder;
    buttonHeight = footerHeight - 3 * buttonBorder;

    epgViewHeaderHeight = config.headerHeightPercent * osdHeight / 100;

    borderRecMenus = 10;

    channelJumpWidth = osdWidth * 30 / 100;
    channelJumpHeight = osdHeight * 20 / 100;

    return true;
}
