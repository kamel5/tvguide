#include "services/remotetimers.h"
#include "tools.h"
#include "epgelement.h"

cEpgElement::cEpgElement(cChannelEpg *owner, const cEvent *event) : cGridElement(owner) {
    this->event = event;
    extText = new cTextWrapper();
    hasTimer = false;
    timerIsActive = false;
    SetTimer();
    hasSwitchTimer = false;
    SetSwitchTimer();
    dummy = false;
}

cEpgElement::~cEpgElement(void) {
    delete extText;
}

void cEpgElement::SetViewportHeight(void) {
    int viewportHeightOld = viewportHeight;
    if ( owner->Start() > StartTime() ) {
        viewportHeight = (std::min((int)EndTime(), owner->Stop()) - owner->Start()) / 60;
    } else if ( owner->Stop() < EndTime() ) {
        viewportHeight = (owner->Stop() - StartTime()) / 60;
        if (viewportHeight < 0) viewportHeight = 0;
    } else {
        viewportHeight = Duration() / 60;
    }
    viewportHeight *= geoManager.minutePixel;
    if (viewportHeight != viewportHeightOld)
        dirty = true;
}

void cEpgElement::PositionPixmap(void) {
    if (config.displayMode == eVertical) {
        int x0 = owner->GetX();
        int y0 = geoManager.statusHeaderHeight + geoManager.channelHeaderHeight + geoManager.channelGroupsHeight;
        if ( owner->Start() < StartTime() ) {
            y0 += (StartTime() - owner->Start()) / 60 * geoManager.minutePixel;
        }
        if (!pixmap) {
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, geoManager.colWidth, viewportHeight),
                                                cRect(0, 0, geoManager.colWidth, Duration() / 60 * geoManager.minutePixel));
        } else {
            osdManager->SetViewPort(pixmap, cRect(x0, y0, geoManager.colWidth, viewportHeight));
        }
    } else if (config.displayMode == eHorizontal) {
        int x0 = geoManager.channelHeaderWidth + geoManager.channelGroupsWidth;
        int y0 = owner->GetY();
        if ( owner->Start() < StartTime() ) {
            x0 += (StartTime() - owner->Start()) / 60 * geoManager.minutePixel;
        }
        if (!pixmap) {
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, viewportHeight, geoManager.rowHeight),
                                                cRect(0, 0, Duration() / 60 * geoManager.minutePixel, geoManager.rowHeight));
        } else {
            osdManager->SetViewPort(pixmap, cRect(x0, y0, viewportHeight, geoManager.rowHeight ));
        }
   }
}

void cEpgElement::SetTimer(void) {
    hasTimer = false;
    if (config.useRemoteTimers && pRemoteTimers) {
        RemoteTimers_Event_v1_0 rt;
        rt.event = event;
        if (pRemoteTimers->Service("RemoteTimers::GetTimerByEvent-v1.0", &rt))
            hasTimer = true;
    } else {
        eTimerMatch TimerMatch = tmNone;
        LOCK_TIMERS_READ;
        const cTimer *timer = Timers->GetMatch(event, &TimerMatch);
        if (TimerMatch == tmFull) {
            hasTimer = true;
            timerIsActive = timer->HasFlags(tfActive);
        }
    }
}

void cEpgElement::SetSwitchTimer(void) {
    if (owner->HasSwitchTimer()) {
        hasSwitchTimer = SwitchTimers.EventInSwitchList(event);
    } else {
        hasSwitchTimer = false;
    }
}

void cEpgElement::SetText(void) {
    if (config.displayMode == eVertical) {
        text->Set(event->Title(), fontManager->FontGrid, geoManager.colWidth - 2 * borderWidth);
        extText->Set(event->ShortText(), fontManager->FontGridSmall, geoManager.colWidth - 2 * borderWidth);
    }
    if (config.showTimeInGrid) {
        timeString = cString::sprintf("%s - %s:", *(event->GetTimeString()), *(event->GetEndTimeString()));
    }
}

void cEpgElement::DrawText(void) {
    tColor colorText = (active) ? theme.Color(clrFontActive) : theme.Color(clrFont);
    tColor colorTextBack;
    if (config.style == eStyleFlat)
        colorTextBack = color;
    else if (config.style == eStyleGraphical)
        colorTextBack = (active) ? theme.Color(clrGridActiveFontBack) : theme.Color(clrGridFontBack);
    else
        colorTextBack = clrTransparent;
    if (config.displayMode == eVertical) {
        if (Height() / geoManager.minutePixel < 6)
            return;
        int textHeight = fontManager->FontGrid->Height();
        int textHeightSmall = fontManager->FontGridSmall->Height();
        int textLines = text->Lines();
        int titleY = borderWidth;
        if (config.showTimeInGrid) { // mit Zeitangabe im Grid
            osdManager->DrawText(pixmap, cPoint(borderWidth, borderWidth), *timeString, colorText, colorTextBack, fontManager->FontGridSmall);
            titleY += textHeightSmall;
        }
        for (int i = 0; i < textLines; i++) {
            osdManager->DrawText(pixmap, cPoint(borderWidth, titleY + i * textHeight), text->GetLine(i), colorText, colorTextBack, fontManager->FontGrid);
        }
        int extTextLines = extText->Lines();
        int offset = titleY + (textLines + 0.5) * textHeight;
        if ((Height() - textHeightSmall - 10) > offset) {
            for (int i = 0; i < extTextLines; i++) {
                osdManager->DrawText(pixmap, cPoint(borderWidth, offset + i * textHeightSmall), extText->GetLine(i), colorText, colorTextBack, fontManager->FontGridSmall);
            }
        }
    } else if (config.displayMode == eHorizontal) {
        cString strTitle = cString::sprintf("%s", CutText(event->Title(), viewportHeight - borderWidth, fontManager->FontGridHorizontal).c_str());
        int titleY = 0;
        if (config.showTimeInGrid) { // mit Zeitangabe im Grid
            osdManager->DrawText(pixmap, cPoint(borderWidth, borderWidth), *timeString, colorText, colorTextBack, fontManager->FontGridHorizontalSmall);
            titleY = fontManager->FontGridHorizontalSmall->Height() + (geoManager.rowHeight - fontManager->FontGridHorizontalSmall->Height() - fontManager->FontGridHorizontal->Height()) / 2;
        } else {
            titleY = (geoManager.rowHeight - fontManager->FontGridHorizontal->Height()) / 2;
        }
        osdManager->DrawText(pixmap, cPoint(borderWidth, titleY), *strTitle, colorText, colorTextBack, fontManager->FontGridHorizontal);
    }
    if (hasSwitchTimer)
        DrawIcon("Switch", theme.Color(clrButtonYellow));
    if (hasTimer) {
        const cTimer *timer = NULL;
        {
        LOCK_TIMERS_READ;
        timer = Timers->GetMatch(event);
        }
	if (timer)
#ifdef SWITCHONLYPATCH
           if (timer->HasFlags(tfSwitchOnly))
              DrawIcon("Switch", theme.Color(clrButtonYellow));
           else if (timer->HasFlags(tfActive))
#else /* SWITCHONLY */
           if (timer->HasFlags(tfActive))
#endif /* SWITCHONLY */
              DrawIcon("REC", theme.Color(clrButtonRed));
           else
              DrawIcon("REC", theme.Color(clrButtonGreen));
        }
}

void cEpgElement::DrawIcon(cString iconText, tColor color) {
    const cFont *font = (config.displayMode == eVertical) ? fontManager->FontGrid : fontManager->FontGridHorizontalSmall;
    int textWidth = font->Width(*iconText) + 2 * borderWidth;
    int textHeight = font->Height() + 10;
    //ToDo Höhe anpassen???
    if ((config.displayMode == eHorizontal) && ((Width() - 2 * textWidth) < 0))
        osdManager->DrawEllipse(pixmap, cRect(Width() - textHeight / 2 - borderWidth, Height() - textHeight - borderWidth, textHeight / 2, textHeight / 2), color);
    else if ((config.displayMode == eVertical) && ((Height() - 2 * textHeight) < 0))
        osdManager->DrawEllipse(pixmap, cRect(Width() - textHeight / 2 - borderWidth, borderWidth, textHeight / 2, textHeight / 2), color);
    else {
        osdManager->DrawEllipse(pixmap, cRect(Width() - textWidth - borderWidth, Height() - textHeight - borderWidth, textWidth, textHeight), color);
        osdManager->DrawText(pixmap, cPoint(Width() - textWidth, Height() - textHeight - borderWidth / 2), *iconText, theme.Color(clrFont), clrTransparent, font);
    }
}

cString cEpgElement::GetTimeString(void) {
    return cString::sprintf("%s - %s", *(event->GetTimeString()), *(event->GetEndTimeString()));
}

void cEpgElement::Debug() {
    esyslog("tvguide epggrid: %s: %s, %s, viewportHeight: %d px, Duration: %d min, active: %d",
                owner->Name(),
                *(event->GetTimeString()),
                event->Title(),
                viewportHeight,
                event->Duration()/60,
                active);
}
