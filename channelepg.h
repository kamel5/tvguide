#ifndef __TVGUIDE_CHANNELEPG_H
#define __TVGUIDE_CHANNELEPG_H

#include <vdr/tools.h>
#include "gridelement.h"
#include "epgelement.h"
#include "headergrid.h"
#include "switchtimer.h"

class cHeaderGrid;

// --- cChannelEpg  -------------------------------------------------------------

class cChannelEpg : public cListObject, public cStyledPixmap {
private:
    int num;
    const cChannel *channel;
    cHeaderGrid *header;
    cList<cGridElement> grids;
    bool hasTimer;
    bool hasSwitchTimer;
    cGridElement *AddEpgGrid(const cEvent *event, cGridElement *after = NULL, bool color = false);
    cGridElement *AddDummyGrid(time_t start, time_t end, cGridElement *after = NULL, bool color = false);
    void SetSwitchTimer(void) { hasSwitchTimer = SwitchTimers.ChannelInSwitchList(channel); };
public:
    cChannelEpg(int num, const cChannel *channel);
    virtual ~cChannelEpg(void);
    void CreateHeader(void);
    void DrawHeader(void);
    bool ReadGrids(void);
    void DrawGrids(void);
    int GetX(void);
    int GetY(void);
    int Start(void) { return timeManager->GetStart(); };
    int Stop(void)  { return timeManager->GetEnd(); };
    const char *Name(void) { return channel->Name(); };
    const cChannel *GetChannel(void) { return channel; };
    cGridElement *GetActive(bool last = false);
    cGridElement *GetNext(cGridElement *activeGrid);
    cGridElement *GetPrev(cGridElement *activeGrid);
    cGridElement *GetNeighbor(cGridElement *activeGrid);
    bool IsFirst(cGridElement *grid);
    void AddNewGridsAtStart(void);
    void AddNewGridsAtEnd(void);
    void ClearOutdatedStart(void);
    void ClearOutdatedEnd(void);
    int GetNum(void) { return num; };
    void SetNum(int num) { this->num = num; };
    void SetTimers(void);
    bool HasSwitchTimer(void) { return hasSwitchTimer; };
    void ClearGrids(void);
    void DumpGrids(void);
};

#endif //__TVGUIDE_CHANNELEPG_H
