#include <list>
#include "config.h"
#include "recmenu.h"

// --- cRecMenu  -------------------------------------------------------------

cRecMenu::cRecMenu(void) {
    border = geoManager.borderRecMenus;
    height = 2 * border;
    headerHeight = 0;
    footerHeight = 0;
    buttonsHight = (config.displayColorButtonsInRecMenu) ? geoManager.footerHeight : 0;
    currentHeight = 0;
    deleteMenuItems = true;
    scrollable = false;
    scrollbarWidth = 3 * border;
    pixmapScrollBar = NULL;
    imgScrollBar = NULL;
    startIndex = 0;
    stopIndex = 0;
    numItems = 0;
    header = NULL;
    footer = NULL;
    buttons = NULL;
}

cRecMenu::~cRecMenu(void) {
    if (header)
        delete header;
    ClearMenuItems(true);
    if (footer)
        delete footer;
    if (buttons)
        delete buttons;
    if (pixmapScrollBar)
        osdManager->DestroyPixmap(pixmapScrollBar);
    if (imgScrollBar)
        delete imgScrollBar;
}

void cRecMenu::SetWidthPercent(int percentOSDWidth) {
    width = geoManager.osdWidth * percentOSDWidth / 100;
    x = (geoManager.osdWidth - width) / 2;
}

void cRecMenu::SetWidthPixel(int pixel) {
    width = pixel;
    x = (geoManager.osdWidth - width) / 2;
}

int cRecMenu::CalculateOptimalWidth(void) {
    int optWidth = 0;
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        int itemWidth = (*item)->GetWidth();
        if (itemWidth > optWidth)
            optWidth = itemWidth;
    }
    return optWidth;
}

void cRecMenu::CalculateHeight(bool reDraw) {
    int newHeight = 0;
    bool createPixmap = false;
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        newHeight += (*item)->GetHeight();
    }
    int scrollBarHeight = newHeight;
    newHeight += 2 * border;
    if (header) {
        newHeight += headerHeight;
    }
    if (footer) {
        newHeight += footerHeight;
    }

    y = (geoManager.osdHeight - newHeight - buttonsHight) / 2;

    if (newHeight != height) {
        height = newHeight;
        createPixmap = true;
    }
    if (scrollable && !reDraw) {
        width += scrollbarWidth + border;
        createPixmap = true;
    }
    if (!createPixmap && pixmap)
        return;

    CreatePixmap(scrollBarHeight);
}

void cRecMenu::CreatePixmap(int scrollBarHeight) {
    pixmap = osdManager->DestroyPixmap(pixmap);
    pixmap = osdManager->CreatePixmap(__func__, "pixmap", 3, cRect(x, y, width, height));
    if (scrollable) {
        int scrollBarX = x + width - scrollbarWidth - border;
        int scrollBarY = y + border + headerHeight;
        pixmapScrollBar = osdManager->DestroyPixmap(pixmapScrollBar);
        pixmapScrollBar = osdManager->CreatePixmap(__func__, "pixmapScrollBar", 4, cRect(scrollBarX, scrollBarY, scrollbarWidth, scrollBarHeight));
    } else 
        pixmapScrollBar = NULL;
}

void cRecMenu::AddHeader(cRecMenuItem *header) {
    this->header = header;
    headerHeight = header->GetHeight();
    height += headerHeight;
}

void cRecMenu::AddFooter(cRecMenuItem *footer) {
    this->footer = footer;
    footerHeight = footer->GetHeight();
    height += footerHeight;
}

void cRecMenu::AddButtons(std::string a, std::string b, std::string c, std::string d) {
    if (!pixmap || !config.displayColorButtonsInRecMenu)
        return;

    DELETENULL(buttons);
    Button_Parameter_t bp;
    bp.a = a;
    bp.b = b;
    bp.c = c;
    bp.d = d;
    bp.left = x;
    bp.width = width;
    bp.top = pixmap->ViewPort().Top() + pixmap->ViewPort().Height() - 2;
    bp.height = buttonsHight;
    bp.fromView = fromSearchMenu;
    bp.color = theme.Color(clrBackground);
    buttons = new cFooter(bp);
}

void cRecMenu::ClearMenuItems(bool destructor) { 
    for (std::list<cRecMenuItem*>::iterator it = menuItems.begin(); it != menuItems.end(); it++) {
        if (deleteMenuItems)
            delete *it;
        else if (!destructor)
            (*it)->Hide();
    }
    menuItems.clear();
};

void cRecMenu::InitMenu(bool complete) {
    currentHeight = 0;
    numItems = 0;
    if (scrollable) {
        width -= scrollbarWidth + border;
        pixmapScrollBar = osdManager->DestroyPixmap(pixmapScrollBar);
        DELETENULL(imgScrollBar);
    }
    pixmap = osdManager->DestroyPixmap(pixmap);
    for (std::list<cRecMenuItem*>::iterator it = menuItems.begin(); it != menuItems.end(); it++) {
        if (deleteMenuItems)
            delete *it;
        else
            (*it)->Hide();
    }
    menuItems.clear();
    if (complete) {
        startIndex = 0;
        stopIndex = 0;
        scrollable = false;
    } else {
        stopIndex = startIndex;
    }

}

void cRecMenu::AddMenuItem(cRecMenuItem *item, bool inFront) {
    item->Show();
    if (!inFront)
        menuItems.push_back(item);
    else
        menuItems.push_front(item);
}

bool cRecMenu::AddMenuItemInitial(cRecMenuItem *item, bool inFront) {
    currentHeight += item->GetHeight();
    int totalHeight = headerHeight + footerHeight + buttonsHight + currentHeight + 2 * border;
    if (totalHeight >= geoManager.osdHeight - 80) {
        scrollable = true;
        currentHeight -= item->GetHeight();
        if (deleteMenuItems) {
            delete item;
        }
        return false;
    }
    numItems++;
    if (!inFront) {
        stopIndex++;
        menuItems.push_back(item);
    } else {
        startIndex--;
        menuItems.push_front(item);
    }
    return true;
}

void cRecMenu::Activate(cRecMenuItem *itemOld, cRecMenuItem *item) {
    itemOld->SetInactive();
    itemOld->SetBackground();
    itemOld->Draw();
    item->SetActive();
    item->SetBackground();
    item->Draw();
}

bool cRecMenu::ActivateNew(eScrollMode scrollMode) {
    cRecMenuItem *activeItem = GetActiveMenuItem();
    if (!scrollMode && !scrollable && footer && footer->IsActive()) {
        if (menuItems.size() > 0) {
            cRecMenuItem *itemLast = menuItems.back();
            Activate(footer, itemLast);
            return true;
        }
    } else if (activeItem) {
        cRecMenuItem *newActiveItem = NULL;
        bool foundActive = false;
        for (std::list<cRecMenuItem*>::iterator item = (scrollMode) ? menuItems.begin() : menuItems.end(); item != ((scrollMode) ? menuItems.end() : menuItems.begin()); ) {
            if (!scrollMode) item--;
            if (!foundActive) {
                if (*item == activeItem) {
                    foundActive = true;
                }
            } else if ((*item)->IsSelectable()) {
                newActiveItem = *item;
                break;
            }
            if (scrollMode) item++;
        }
        if (newActiveItem) {
            Activate(activeItem , newActiveItem);
            return true;
        } else if (scrollMode && !scrollable && footer && footer->IsSelectable() && !footer->IsActive()) {
            Activate(activeItem , footer);
            return true;
        }
    }
    return false;
}

bool cRecMenu::Scroll(eScrollMode scrollMode, bool retry) {
//    ScrollMode eBack -> ScrollUp, eForward -> ScrollDown
    if (!scrollMode && footer && footer->IsActive()) {
        if (menuItems.size() > 0)
            Activate(footer, menuItems.back());
        return true;
    }

    //get x items
    int numNewItems = numItems / 2;
    int numAdded = 0;
    cRecMenuItem *newItem = NULL;
    while (newItem = GetMenuItem(scrollMode ? stopIndex : (startIndex - 1))) {
        if (scrollMode)
            menuItems.push_back(newItem);
        else
            AddMenuItem(newItem, true);
        cRecMenuItem *item = (scrollMode) ? menuItems.front() : menuItems.back();
        if (deleteMenuItems) {
            delete item;
        } else {
            item->SetInactive();
            item->Hide();
        }
        if (scrollMode) {
            menuItems.pop_front();
            startIndex++;
            stopIndex++;
        } else {
            menuItems.pop_back();
            stopIndex--;
            startIndex--;
        }
        numAdded++;
        if (numAdded >= numNewItems)
            break;
    }
    if (numAdded != 0) {
        scrollable = true;
        CalculateHeight(true);
        Arrange(deleteMenuItems);
        Display(deleteMenuItems);
        ActivateNew(scrollMode ? eForward : eBack);
    } else {
        //last item reached, activate footer if not already active
        if (scrollMode && footer && !(footer->IsActive())) {
            cRecMenuItem *activeItem = GetActiveMenuItem();
            Activate(activeItem , footer);
        } else {
            return false;
        }
    }
    return true;
}

void cRecMenu::Page(eScrollMode pageMode) {
//    pageMode eBack -> PageUp, eForward -> PageDown
    cRecMenuItem *activeItem = GetActiveMenuItem();
    if (!activeItem)
        return;

    if (pageMode && footer && activeItem == footer)
        return;

    if (!scrollable) {
        Jump(pageMode ? eEnd : eBeginn);
        return;
    }

    if (!pageMode && footer && activeItem == footer) {
        Activate(footer, menuItems.front());
        return;
    }

    int newActive = (pageMode) ? (GetNumActive() + numItems) : (GetNumActive() - numItems);
    if (newActive < 0)
        newActive = 0;
    activeItem->SetInactive();
    activeItem->SetBackground();
    ClearMenuItems();
    currentHeight = 0;
    numItems = 0;
    if (pageMode)
        startIndex = stopIndex;
    else
        stopIndex = startIndex;
    cRecMenuItem *newItem = NULL;
    bool spaceLeft = true;
    while (newItem = GetMenuItem((pageMode) ? stopIndex : (startIndex - 1))) {
        if ((pageMode && stopIndex == newActive) || (!pageMode && startIndex - 1 == newActive))
            newItem->SetActive();
        spaceLeft = AddMenuItemInitial(newItem, !pageMode);
        if (!spaceLeft)
            break;
    }
    if (spaceLeft) {
        while (newItem = GetMenuItem((pageMode) ? (startIndex - 1) : stopIndex)) {
            spaceLeft = AddMenuItemInitial(newItem, pageMode);
            if (!spaceLeft)
                break;
        }
    }
    if (pageMode && GetNumActive() == GetTotalNumMenuItems())
        menuItems.back()->SetActive();
    else if (!pageMode && GetNumActive() == numItems)
        menuItems.front()->SetActive();
    CalculateHeight(true);
    Arrange(deleteMenuItems);
    Display(deleteMenuItems);
}

void cRecMenu::Jump(eJumpMode jumpMode) {
    cRecMenuItem *activeItem = GetActiveMenuItem();
    if (!activeItem)
        return;

    if (!scrollable) {
        cRecMenuItem *selectable= NULL;
        if (jumpMode == eBeginn) {
            for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
                if ((*item)->IsSelectable()) {
                    selectable = *item;
                    break;
                }
            }
        } else { // eEnd
            if (footer && footer->IsSelectable()) {
                selectable = footer;
            } else {
                for (std::list<cRecMenuItem*>::iterator item = menuItems.end(); item != menuItems.begin(); ) {
                    item--;
                    if ((*item)->IsSelectable()) {
                        selectable = *item;
                        break;
                    }
                }
            }
        }
        if (selectable) {
            Activate(activeItem , selectable);
        }
    } else {
        activeItem->SetInactive();
        activeItem->SetBackground();
        if (jumpMode == eBeginn && footer)
            footer->Draw();
        ClearMenuItems();
        int totalNumItems = GetTotalNumMenuItems();
        int currentItem = (jumpMode == eBeginn) ? 0 : totalNumItems - 1;
        int itemsAdded = 0;
        cRecMenuItem *newItem = NULL;
        while (newItem = GetMenuItem(currentItem)) {
            AddMenuItem(newItem, jumpMode);
            if (jumpMode == eBeginn) {
                currentItem++;
                if (currentItem >= numItems)
                    break;
            } else {
                currentItem--;
                itemsAdded++;
                if (itemsAdded >= numItems)
                    break;
            }
        }
        CalculateHeight(true);
        Arrange(false);
        startIndex = (jumpMode == eBeginn) ? 0 : totalNumItems - numItems;
        stopIndex = (jumpMode == eBeginn) ? currentItem : totalNumItems;
        cRecMenuItem *item = NULL;
        if (jumpMode == eBeginn) {
            item = menuItems.front();
        } else if (footer) {
            item = footer;
        } else {
            item = menuItems.back();
        }
        item->SetActive();
        item->SetBackground();
        item->Draw();
        Display(false);
    }
}

void cRecMenu::Arrange(bool scroll) {
    int xElement = x + border;
    int yElement = y + border;
    int widthElement = width - 2 * border;
    if (scrollable)
        widthElement -= scrollbarWidth + border;

    if (header) {
        if (!scroll) {
            header->SetGeometry(xElement, yElement, widthElement);
            header->SetPixmaps();
            header->SetBackground();
        }
        yElement += header->GetHeight();
    }
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        (*item)->SetGeometry(xElement, yElement, widthElement);
        (*item)->SetPixmaps();
        yElement += (*item)->GetHeight();
    }
    if (footer && !scroll) {
        footer->SetGeometry(xElement, yElement, widthElement);
        footer->SetPixmaps();
        footer->SetBackground();
    }
}

void cRecMenu::Display(bool scroll) {
    if (config.style == eStyleGraphical) {
        DrawBackgroundGraphical(bgRecMenuBack);
    } else {
        osdManager->Fill(pixmap, theme.Color(clrBackground));
        DrawBorder();
    }
    if (header && !scroll) {
        header->SetBackground();
        header->Draw();
    }
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        (*item)->SetBackground();
        (*item)->Show();
        (*item)->Draw();
    }
    if (footer && !scroll) {
        footer->SetBackground();
        footer->Draw();
    }
    if (scrollable)
        DrawScrollBar();
}

void cRecMenu::Hide(void) {
    osdManager->SetLayer(pixmap, -1);
    if (pixmapScrollBar)
        pixmapScrollBar->SetLayer(-1);
    if (header)
        header->Hide();
    if (footer)
        footer->Hide();
    if (buttons)
        buttons->Hide();
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        (*item)->Hide();
    }
}

void cRecMenu::Show(void) {
    osdManager->SetLayer(pixmap, 3);
    if (pixmapScrollBar)
        pixmapScrollBar->SetLayer(3);
    if (header)
        header->Show();
    if (footer)
        footer->Show();
    if (buttons)
        buttons->Show();
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        (*item)->Show();
    }
}

void cRecMenu::UpdateActiveMenuItem(void) {
    cRecMenuItem *activeItem = GetActiveMenuItem();
    if (activeItem)
        activeItem->Draw();   
}


void cRecMenu::DrawScrollBar(void) {
    if (!pixmapScrollBar)
       return;

    pixmapScrollBar->Fill(theme.Color(clrBorder));
    pixmapScrollBar->DrawRectangle(cRect(2, 2, pixmapScrollBar->ViewPort().Width() - 4, pixmapScrollBar->ViewPort().Height() - 4), theme.Color(clrBackground));
    
    int totalNumItems = GetTotalNumMenuItems();
    if (!totalNumItems)
       return;

    if (!imgScrollBar) {
        int scrollBarImgHeight = (pixmapScrollBar->ViewPort().Height() - 8) * numItems / totalNumItems;
        imgScrollBar = CreateScrollbar(pixmapScrollBar->ViewPort().Width() - 8, scrollBarImgHeight, theme.Color(clrHighlight), theme.Color(clrHighlightBlending));
    }
    int offset = (pixmapScrollBar->ViewPort().Height() - 8) * startIndex / totalNumItems;
    pixmapScrollBar->DrawImage(cPoint(4, 2 + offset), *imgScrollBar);
}

cRecMenuItem *cRecMenu::GetActiveMenuItem(void) {
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        if ((*item)->IsActive())
            return *item;
    }
    if (footer && footer->IsActive())
        return footer;
    return NULL;
}

cRecMenuItem *cRecMenu::GetMenuItemAt(int num) {
    int current = 0;
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        if (current == num)
            return *item;
        current++;
    }
    return NULL;
}

int cRecMenu::GetNumActive(void) {
    int numActive = startIndex;
    for (std::list<cRecMenuItem*>::iterator item = menuItems.begin(); item != menuItems.end(); item++) {
        if ((*item)->IsActive()) {
            break;
        }
        numActive++;
    }
    return numActive;
}

eRecMenuState cRecMenu::ProcessKey(eKeys Key) {
    cRecMenuItem *activeItem = GetActiveMenuItem();
    eRecMenuState state = rmsContinue;
    if (!activeItem)
        return state;

    state = activeItem->ProcessKey(Key);
    if (state == rmsRefresh) {
        CreateMenuItems();
        Display();
    } else if (state == rmsNotConsumed) {
        switch (Key & ~k_Repeat) {
            case kUp:    if (!ActivateNew(eBack))
                             if (!Scroll(eBack, false))
                                 Jump(eEnd);
                         state = rmsConsumed;
                         break;
            case kDown:  if (!ActivateNew(eForward))
                             if (!Scroll(eForward, false))
                                 Jump(eBeginn);
                         state = rmsConsumed;
                         break;
            case kLeft:  Page(eBack);
                         state = rmsConsumed;
                         break;
            case kRight: Page(eForward);
                         state = rmsConsumed;
                         break;
            default:     break;
        }
    }
    return state;
}

cImage *cRecMenu::CreateScrollbar(int width, int height, tColor clrBgr, tColor clrBlend) {
    cImage *image = new cImage(cSize(width, height));
    image->Fill(clrBgr);
    if (height >= 32 && config.style != eStyleFlat) {
        int numSteps = 64;
        int alphaStep = 0x03;
        if (height < 100) {
            numSteps = 32;
            alphaStep = 0x06;
        }
        int stepY = std::max(1, (int)(0.5 * height / numSteps));
        int alpha = 0x40;
        tColor clr;
        for (int i = 0; i < numSteps; i++) {
            clr = AlphaBlend(clrBgr, clrBlend, alpha);
            for (int y = i * stepY; y < (i + 1) * stepY; y++) {
                for (int x = 0; x < width; x++) {
                    image->SetPixel(cPoint(x, y), clr);
                }
            }
            alpha += alphaStep;
        }
    }
    return image;
}
