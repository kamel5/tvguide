#ifndef __TVGUIDE_FOOTER_H
#define __TVGUIDE_FOOTER_H

#include <vdr/osd.h>
#include "config.h"
#include "channelgroups.h"

// --- cFooter  -------------------------------------------------------------

typedef struct {
    cChannelGroups *channelGroups = NULL;
    std::string a = "";
    std::string b = "";
    std::string c = "";
    std::string d = "";
    int left = 0;
    int width = 0;
    int top = 0;
    int height = 0;
    int fromView = fromEpgGrid;
    bool isRecording = false;
    tColor color = clrTransparent;
} Button_Parameter_t;

class cFooter  {
private:
    cPixmap *pixmapFooter = NULL;
    int buttonWidth = 0;
    int buttonY;
    int positionButtons[4];
    int currentGroup;
    int channelGroupLast;
    int layer;
    Button_Parameter_t bp;
    void SetButtonPositions(void);
    void DrawButton(const char *text, tColor color, tColor borderColor, eOsdElementType buttonType, int num);
    void ClearButton(int num);
    void DrawFooter(int currentGroup = -1);
public:
    cFooter(Button_Parameter_t bp);
    virtual ~cFooter(void);
    void UpdateGroupButtons(const cChannel *channel, bool force = false);
    void Hide(void);
    void Show(void);
};

#endif //__TVGUIDE_FOOTER_H
