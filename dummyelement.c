#include "dummyelement.h"

cDummyElement::cDummyElement(cChannelEpg *owner, time_t start, time_t end) : cGridElement(owner) {
    this->start = start;
    this->end = end;
    strText = tr("No EPG Information available");
    dummy = true;
    hasTimer = false;
}

cDummyElement::~cDummyElement(void) {
}

time_t cDummyElement::Duration(void) { 
    //max Duration 5h
    if (end - start > 18000)
        return 18000;
    return (end - start);
};

void cDummyElement::SetViewportHeight(void) {
    int viewportHeightOld = viewportHeight;
    viewportHeight = Duration() / 60 * geoManager.minutePixel;
    if (viewportHeight != viewportHeightOld)
        dirty = true;
}

void cDummyElement::PositionPixmap(void) {
    int x0, y0;
    if (config.displayMode == eVertical) {
        x0 = owner->GetX();
        y0 = geoManager.statusHeaderHeight + geoManager.channelHeaderHeight + geoManager.channelGroupsHeight;
        if ( owner->Start() < StartTime() ) {
            y0 += (StartTime() - owner->Start()) / 60 * geoManager.minutePixel;
        }
        if (!pixmap) {
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, geoManager.colWidth, viewportHeight));
        } else if (dirty) {
            pixmap = osdManager->DestroyPixmap(pixmap);
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, geoManager.colWidth, viewportHeight));
        } else {
            osdManager->SetViewPort(pixmap, cRect(x0, y0, geoManager.colWidth, viewportHeight));
        }
    } else if (config.displayMode == eHorizontal) {
        x0 = geoManager.channelHeaderWidth + geoManager.channelGroupsWidth;
        y0 = owner->GetY();
        if ( owner->Start() < StartTime() ) {
            x0 += (StartTime() - owner->Start()) / 60 * geoManager.minutePixel;
        }
        if (!pixmap) {
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, viewportHeight, geoManager.rowHeight));
        } else if (dirty) {
            pixmap = osdManager->DestroyPixmap(pixmap);
            pixmap = osdManager->CreatePixmap(__func__, "pixmap", -1, cRect(x0, y0, viewportHeight, geoManager.rowHeight));
        } else {
            osdManager->SetViewPort(pixmap, cRect(x0, y0, viewportHeight, geoManager.rowHeight));
        }
    }
}

void cDummyElement::SetText(void) {
    if (config.displayMode == eVertical) {
        text->Set(*strText, fontManager->FontGrid, geoManager.colWidth - 2 * borderWidth);
    }
}

void cDummyElement::DrawText(void) {
    tColor colorText = (active)?theme.Color(clrFontActive):theme.Color(clrFont);
    tColor colorTextBack;
    if (config.style == eStyleFlat)
        colorTextBack = color;
    else if (config.style == eStyleGraphical)
        colorTextBack = (active)?theme.Color(clrGridActiveFontBack):theme.Color(clrGridFontBack);
    else
        colorTextBack = clrTransparent;
    if (config.displayMode == eVertical) {
        if (Height() / geoManager.minutePixel < 6)
            return;
        int textHeight = fontManager->FontGrid->Height();
        int textLines = text->Lines();
        for (int i=0; i<textLines; i++) {
            osdManager->DrawText(pixmap, cPoint(borderWidth, borderWidth + i*textHeight), text->GetLine(i), colorText, colorTextBack, fontManager->FontGrid);
        }
    } else if (config.displayMode == eHorizontal) {
        if (Width() / geoManager.minutePixel < 10) {
            int titleY = (geoManager.rowHeight - fontManager->FontGridHorizontal->Height()) / 2;
            osdManager->DrawText(pixmap, cPoint(borderWidth - 2, titleY), "...", colorText, colorTextBack, fontManager->FontGridHorizontal);
            return;
        }
        int titleY = (geoManager.rowHeight - fontManager->FontGridHorizontal->Height()) / 2;
        osdManager->DrawText(pixmap, cPoint(borderWidth, titleY), *strText, colorText, colorTextBack, fontManager->FontGridHorizontal);
    }
}
cString cDummyElement::GetText(void) {
    return strText;
}

cString cDummyElement::GetTimeString(void) {
    return cString::sprintf("%s - %s", *TimeString(start), *TimeString(end));
}

void cDummyElement::Debug(void) {
    esyslog("tvguide dummyelement: %s: %s, %s, viewportHeight: %d px, Duration: %ld min, active: %d", 
                owner->Name(),
                *cTimeManager::PrintTime(start), 
                *cTimeManager::PrintTime(end), 
                viewportHeight, 
                Duration()/60,
                active);
}
