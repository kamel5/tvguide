#include "config.h"
#include "osdmanager.h"

cOsdManager *osdManager;

cOsdManager::cOsdManager(void) {
    osd = NULL;
}

cOsdManager::~cOsdManager(void) {
    DeleteOsd();
    dsyslog ("tvguide: delete osd\n");
}

void cOsdManager::Lock(void) {
    mutex.Lock();
}

void cOsdManager::Unlock(void) {
    mutex.Unlock();
}

bool cOsdManager::CreateOsd(void) {
    dsyslog ("tvguide: create osd\n");
    osd = cOsdProvider::NewOsd(cOsd::OsdLeft(), cOsd::OsdTop());
    if (osd) {
        tArea Area = { 0, 0, cOsd::OsdWidth() - 1, cOsd::OsdHeight() - 1,  32 };
        if (osd->SetAreas(&Area, 1) == oeOk) {  
            return true;
        }
    }
    return false;
}

void cOsdManager::DeleteOsd(void) {
    Lock();
    DELETENULL(osd);
    Unlock();
    if (config.scaleVideo) {
        cRect vidWin = cDevice::PrimaryDevice()->CanScaleVideo(cRect::Null);
        cDevice::PrimaryDevice()->ScaleVideo(vidWin);
    }
}

void cOsdManager::ScaleVideo(void) {
    if (config.scaleVideo) {
        int height = geoManager.statusHeaderHeight;
        int width = height * 16 / 9;
        int x = Left() + geoManager.osdWidth - width;
        int y = Top();
        cRect availableRect(x, y, width, height);
        cRect vidWin = cDevice::PrimaryDevice()->CanScaleVideo(availableRect);
        cDevice::PrimaryDevice()->ScaleVideo(vidWin);
    }
}

void cOsdManager::SetBackground(void) {
    
    if (config.displayStatusHeader && config.scaleVideo) {
        int widthStatus = cOsd::OsdWidth() - geoManager.statusHeaderHeight * 16 / 9;
        osd->DrawRectangle(0, 0, widthStatus, geoManager.statusHeaderHeight, theme.Color(clrBackgroundOSD));
        osd->DrawRectangle(0, geoManager.statusHeaderHeight, Width(), Height(), theme.Color(clrBackgroundOSD));    
    }
    else
        osd->DrawRectangle(0, 0, Width(), Height(), theme.Color(clrBackgroundOSD));
    
}

cPixmap *cOsdManager::CreatePixmap(const char *func, cString Name, int Layer, const cRect &ViewPort, const cRect &DrawPort) {
    if (!osd)
        return NULL;

    if (cPixmap *pixmap = osd->CreatePixmap(Layer, ViewPort, DrawPort)) {
        return pixmap;
    }

    esyslog("tvguide: Could not create pixmap: %s \"%s\" of size %i x %i", func, *Name, DrawPort.Size().Width(), DrawPort.Size().Height());
    cRect NewDrawPort = DrawPort;
    int width = std::min(DrawPort.Size().Width(), osd->MaxPixmapSize().Width());
    int height = std::min(DrawPort.Size().Height(), osd->MaxPixmapSize().Height());
    NewDrawPort.SetSize(width, height);
    if (cPixmap *pixmap = osd->CreatePixmap(Layer, ViewPort, NewDrawPort)) {
        esyslog("tvguide: Create pixmap: %s \"%s\" of reduced size %i x %i", func, *Name, width, height);
        return pixmap;
    }

    esyslog("tvguide: Could not create pixmap: %s \"%s\" of reduced size %i x %i", func, *Name, width, height);
    return NULL;
}

cPixmap *cOsdManager::DestroyPixmap(cPixmap *pixmap, const char *func, cString Name) {
    if (!(osd && pixmap))
        return NULL;

    if (func)
        dsyslog("tvguide: Deleting pixmap: %s \"%s\"", func, *Name);
    osd->DestroyPixmap(pixmap);
    return NULL;
}

void cOsdManager::Flush(void) {
    if (osd) {
        osd->Flush();
        dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    }
}
