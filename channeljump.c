#include <vdr/channels.h>
#include "config.h"
#include "channeljump.h"

cChannelJump::cChannelJump(int lastValidChannel) {
    channel = 0;
    startTime = 0;
    maxChannels = lastValidChannel;
    timeout = Setup.ChannelEntryTimeout;

    CreatePixmaps();
    DrawHeader();
    DrawText();
}

cChannelJump::~cChannelJump(void) {
    osdManager->DestroyPixmap(pixmapBack);
    osdManager->DestroyPixmap(pixmapText);
}

void cChannelJump::CreatePixmaps(void) {
    int x = (geoManager.osdWidth - geoManager.channelJumpWidth) / 2;
    int y = (geoManager.osdHeight - geoManager.channelJumpHeight) / 2;

    pixmapBack = osdManager->CreatePixmap(__func__, "pixmapBack", 4, cRect(x, y, geoManager.channelJumpWidth, geoManager.channelJumpHeight));
    pixmap = osdManager->CreatePixmap(__func__, "pixmap", 5, cRect(x, y, geoManager.channelJumpWidth, geoManager.channelJumpHeight));
    pixmapText = osdManager->CreatePixmap(__func__, "pixmapText", 6, cRect(x, y, geoManager.channelJumpWidth, geoManager.channelJumpHeight));
}

void cChannelJump::DrawHeader(void) {
    if (config.style == eStyleGraphical) {
        DrawBackgroundGraphical(bgChannelJump);
    } else {
        osdManager->Fill(pixmap, theme.Color(clrBackground));
        DrawBorder();
    }
    osdManager->Fill(pixmapBack, clrTransparent);
    osdManager->DrawRectangle(pixmapBack, cRect(5, Height() / 2, Width() - 10, Height() - 3), theme.Color(clrBackground));
}

void cChannelJump::DrawText(void) {
    osdManager->Fill(pixmapText, clrTransparent);

    cString header = cString::sprintf("%s:", tr("Channel"));

    const cFont *font = fontManager->FontMessageBox;
    const cFont *fontHeader = fontManager->FontMessageBoxLarge;

    int xHeader = (Width() - fontHeader->Width(*header)) / 2;
    int yHeader = (Height() / 2 - fontHeader->Height()) / 2;
    osdManager->DrawText(pixmapText, cPoint(xHeader, yHeader), *header, theme.Color(clrFont), clrTransparent, fontHeader);

    cString strChannel = BuildChannelString();
    int xChannel = (Width() - font->Width(*strChannel)) / 2;
    int yChannel = Height() / 2 + (Height() / 2 - font->Height()) / 2;
    osdManager->DrawText(pixmapText, cPoint(xChannel, yChannel), *strChannel, theme.Color(clrFont), clrTransparent, font);
}

void cChannelJump::Set(int num) {
    startTime = cTimeMs::Now();
    if (channel == 0) {
        channel = num;
    } else {
        int newChannel = channel * 10 + num;
        if (newChannel <= maxChannels)
            channel = newChannel;
    }
    DrawText();
}

cString cChannelJump::BuildChannelString(void) {
    if (channel * 10 <= maxChannels)
        return cString::sprintf("%d-", channel);
    else
        return cString::sprintf("%d", channel); 
}

bool cChannelJump::TimeOut(void) {
    if ((cTimeMs::Now() - startTime) > timeout)
        return true;
    return false;
}
