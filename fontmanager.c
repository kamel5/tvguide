#include "config.h"
#include "fontmanager.h"

cFontManager *fontManager;

cFontManager::cFontManager() {
    dsyslog ("tvguide: Creating Fonts\n");
    InitialiseFontType();
    //Common Fonts
    FontButton = CreateFont(geoManager.footerHeight/3 + 4 + config.FontButtonDelta);
    FontDetailView = CreateFont(geoManager.osdHeight/30 + config.FontDetailViewDelta);
    FontDetailViewSmall = CreateFont(geoManager.osdHeight/40 + config.FontDetailViewSmallDelta);
    FontDetailHeader = CreateFont(geoManager.osdHeight/27 + config.FontDetailHeaderDelta);
    FontDetailHeaderLarge = CreateFont(geoManager.osdHeight/20 + config.FontDetailHeaderDelta);
    FontMessageBox = CreateFont(geoManager.osdHeight/33 + config.FontMessageBoxDelta);
    FontMessageBoxLarge = CreateFont(geoManager.osdHeight/30 + config.FontMessageBoxLargeDelta);
    FontStatusHeader = CreateFont(geoManager.statusHeaderHeight/6 - 4 + config.FontStatusHeaderDelta);
    FontStatusHeaderLarge = CreateFont(geoManager.statusHeaderHeight/5 + config.FontStatusHeaderLargeDelta);
    //Fonts for vertical Display 
    FontChannelHeader = CreateFont(geoManager.colWidth/10 + config.FontChannelHeaderDelta);
    FontChannelGroups = CreateFont(geoManager.colWidth/8 + config.FontChannelGroupsDelta);
    FontGrid = CreateFont(geoManager.colWidth/12 + config.FontGridDelta);
    FontGridSmall = CreateFont(geoManager.colWidth/12 + config.FontGridSmallDelta);
    FontTimeLineWeekday = CreateFont(geoManager.timeLineWidth/3 + config.FontTimeLineWeekdayDelta);
    FontTimeLineDate = CreateFont(geoManager.timeLineWidth/4 + config.FontTimeLineDateDelta);
    FontTimeLineTime = CreateFont(geoManager.timeLineWidth/4 + config.FontTimeLineTimeDelta);
    //Fonts for horizontal Display 
    FontChannelHeaderHorizontal = CreateFont(geoManager.rowHeight/3 + config.FontChannelHeaderHorizontalDelta);
    FontChannelGroupsHorizontal = CreateFont(geoManager.rowHeight/3 + 5 + config.FontChannelGroupsHorizontalDelta);
    FontGridHorizontal = CreateFont(geoManager.rowHeight/3 + 5 + config.FontGridHorizontalDelta);
    FontGridHorizontalSmall = CreateFont(geoManager.rowHeight/4 + config.FontGridHorizontalSmallDelta);
    FontTimeLineDateHorizontal = CreateFont(geoManager.timeLineHeight/2 + 5 + config.FontTimeLineDateHorizontalDelta);
    FontTimeLineTimeHorizontal = CreateFont(geoManager.timeLineHeight/2 + config.FontTimeLineTimeHorizontalDelta);
    //Fonts for RecMenu
    FontRecMenuItem = CreateFont(geoManager.osdHeight/30 + config.FontRecMenuItemDelta);
    FontRecMenuItemSmall = CreateFont(geoManager.osdHeight/40 + config.FontRecMenuItemSmallDelta);
    FontRecMenuItemLarge = CreateFont(geoManager.osdHeight/25 + config.FontRecMenuItemLargeDelta);
}

cFontManager::~cFontManager() {
    delete FontButton;
    delete FontDetailView;
    delete FontDetailViewSmall;
    delete FontDetailHeader;
    delete FontDetailHeaderLarge;
    delete FontMessageBox;
    delete FontMessageBoxLarge;
    delete FontStatusHeader;
    delete FontStatusHeaderLarge;
    delete FontChannelHeader;
    delete FontChannelGroups;
    delete FontGrid;
    delete FontGridSmall;
    delete FontTimeLineWeekday;
    delete FontTimeLineDate;
    delete FontTimeLineTime;
    delete FontChannelHeaderHorizontal;
    delete FontChannelGroupsHorizontal;
    delete FontGridHorizontal;
    delete FontGridHorizontalSmall;
    delete FontTimeLineDateHorizontal;
    delete FontTimeLineTimeHorizontal;
    delete FontRecMenuItem;
    delete FontRecMenuItemSmall;
    delete FontRecMenuItemLarge;
    dsyslog ("tvguide: Deleting Fonts\n");
}

void cFontManager::InitialiseFontType(void) {
    if (config.fontIndex == 0) {
        fontName = config.fontNameDefault;
    } else {
        cStringList availableFonts;
        cFont::GetAvailableFontNames(&availableFonts);
        if (availableFonts[config.fontIndex-1]) {
            fontName = availableFonts[config.fontIndex-1];
        } else 
            fontName = config.fontNameDefault;
    }
    cFont *test = NULL;
    test = cFont::CreateFont(*fontName, 30);
    if (!test) {
        fontName = DefaultFontSml;
    }
    delete test;
    esyslog("tvguide: Set Font to %s", *fontName);
}

cFont *cFontManager::CreateFont(int size) {
    return cFont::CreateFont(*fontName, size);
}

