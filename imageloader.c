#include <math.h>
#include <string>
#include <dirent.h>
#include <iostream>
#include <vdr/channels.h>

#include "config.h"
#include "imageloader.h"
#include "tools.h"

#ifdef IMAGEMAGICK
#if MagickLibVersion >= 0x700
#define IMAGEMAGICK7 
#endif  
#endif

using namespace Magick;

cImageLoader::cImageLoader() {
}

cImageLoader::~cImageLoader() {
}

bool cImageLoader::LoadLogo(const cChannel *channel, int width, int height) {
    if (!channel || (width == 0)||(height==0))
        return false;
    std::string channelID = StrToLowerCase(*(channel->GetChannelID().ToString()));
    std::string logoLower = StrToLowerCase(channel->Name());
    cString extension;
    if (config.logoExtension == 0) {
        extension = "png";
    } else if (config.logoExtension == 1) {
        extension = "jpg";
    }
    bool success = false;
    if (config.logoPathSet) {
        success = LoadImage(channelID.c_str(), *config.logoPath, *extension);
        if (!success) {
            success = LoadImage(logoLower.c_str(), *config.logoPath, *extension);
        }
    }
    if (!success)
        success = LoadImage(channelID.c_str(), *config.logoPathDefault, *extension);
    if (!success)
        success = LoadImage(logoLower.c_str(), *config.logoPathDefault, *extension);
    if (success)
        buffer.sample(Geometry(width, height));
    return success;
}

bool cImageLoader::LoadEPGImage(int eventID, int width, int height) {
    if ((width == 0)||(height==0))
        return false;
    bool success = false;
    success = LoadImage(*cString::sprintf("%d", eventID), *config.epgImagePath, "jpg");
    if (!success)
        success = LoadImage(*cString::sprintf("%d_0", eventID), *config.epgImagePath, "jpg");
    if (!success)
        return false;
    buffer.sample( Geometry(width, height));
    return true;
}

bool cImageLoader::LoadAdditionalEPGImage(cString name) {
    int width = config.epgImageWidthLarge;
    int height = config.epgImageHeightLarge;
    if ((width == 0)||(height==0))
        return false;
    bool success = false;
    success = LoadImage(*name, *config.epgImagePath, "jpg");
    if (!success)
        return false;
    if (height != 0 || width != 0) {
        buffer.sample( Geometry(width, height));
    }
    return true;
}

bool cImageLoader::LoadPoster(const char *poster, int width, int height, bool scale) {
    if (LoadImage(poster)) {
        if (scale)
            buffer.sample(Geometry(width, height));
        return true;
    }
    return false;
}

bool cImageLoader::LoadIcon(const char *cIcon, int size) {
    if (size==0)
        return false;
    bool success = false;
    if (config.iconsPathSet) {
        cString iconPathTheme = cString::sprintf("%s%s/recmenuicons/", *config.iconPath, *config.themeName);
        success = LoadImage(cIcon, *iconPathTheme, "png");
        if (!success) {
            cString iconPath = cString::sprintf("%srecmenuicons/", *config.iconPath);
            success = LoadImage(cIcon, *iconPath, "png");
        }
    }
    if (!success) {
        cString iconPathTheme = cString::sprintf("%s%s/recmenuicons/", *config.iconPathDefault, *config.themeName);
        success = LoadImage(cIcon, *iconPathTheme, "png");
        if (!success) {
            cString iconPath = cString::sprintf("%srecmenuicons/", *config.iconPathDefault);
            success = LoadImage(cIcon, *iconPath, "png");
        }
    }
    if (!success)
        return false;
    buffer.sample(Geometry(size, size));
    return true;
}

bool cImageLoader::LoadOsdElement(cString name, int width, int height) {
    if ((width == 0)||(height==0))
        return false;
    bool success = false;
    if (config.iconsPathSet) {
        cString path = cString::sprintf("%s%s%s", *config.iconPath, *config.themeName, "/osdElements/");
        success = LoadImage(*name, *path, "png");
        if (!success) {
            path = cString::sprintf("%s%s", *config.iconPath, "/osdElements/");
            success = LoadImage(*name, *path, "png");
        }
    }
    if (!success) {
        cString path = cString::sprintf("%s%s%s", *config.iconPathDefault, *config.themeName, "/osdElements/");
        success = LoadImage(*name, *path, "png");
    }
    if (!success) {
        cString path = cString::sprintf("%s%s", *config.iconPathDefault, "/osdElements/");
        success = LoadImage(*name, *path, "png");
    }
    if (!success)
        return false;
    Geometry size(width, height);
    size.aspect(true);
    buffer.sample(size);
    return true;
}

cImage cImageLoader::GetImage() {
    return CreateImageCopy();
}
