#ifndef __TVGUIDE_CHANNELGROUPS_H
#define __TVGUIDE_CHANNELGROUPS_H

#include <vector>
#include <vdr/channels.h>
#include <vdr/tools.h>
#include "config.h"
#include "styledpixmap.h"


// --- cChannelGroup  -------------------------------------------------------------

class cChannelGroup {
private:
    int channelStart;
    int channelStop;
    const char *name;
public:
    cChannelGroup(const char *name);
    virtual ~cChannelGroup(void);
    void SetChannelStart(int start) { channelStart = start; };
    int StartChannel(void) { return channelStart; };
    void SetChannelStop(int stop) { channelStop = stop; };
    int StopChannel(void) { return channelStop; };
    const char *GetName(void) { return name; };
    void Dump(void);
};

// --- cChannelGroupGrid  -------------------------------------------------------------

class cChannelGroupGrid : public cListObject, public cStyledPixmap {
private:
    const char *name;
    bool isColor1;
    void DrawHorizontal(tColor colorText, tColor colorTextBack);
    void DrawVertical(tColor colorText, tColor colorTextBack);
public:
    cChannelGroupGrid(const char *name);
    virtual ~cChannelGroupGrid(void);
    void SetColor(bool color) { isColor1 = color; };
    void SetBackground(void);
    void SetGeometry(int start, int end);
    void Draw(void);
};

// --- cChannelGroups  -------------------------------------------------------------

class cChannelGroups {
private:
    std::vector<cChannelGroup> channelGroups;
    cList<cChannelGroupGrid> groupGrids;
    void Init(void);
public:
    cChannelGroups(void);
    virtual ~cChannelGroups(void);
    int GetGroup(const cChannel *channel);
    int GetPrevGroupChannelNumber(const cChannel *channel);
    int GetNextGroupChannelNumber(const cChannel *channel);
    const char *GetPrevGroupName(int group);
    const char *GetNextGroupName(int group);
    int GetPrevGroupFirstChannel(int group);
    int GetNextGroupFirstChannel(int group);
    bool IsInFirstGroup(const cChannel *channel);
    bool IsInLastGroup(const cChannel *channel);
    bool IsInSecondLastGroup(const cChannel *channel);
    void Draw(const cChannel *start, const cChannel *stop);
    void CreateGroupGrid(const char *name, int number, int start, int end);
    int GetLastValidChannel(void);
    void DumpGroups(void);
};

#endif //__TVGUIDE_CHANNELGROUPS_H
