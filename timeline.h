#ifndef __TVGUIDE_TIMELINE_H
#define __TVGUIDE_TIMELINE_H

#include "timemanager.h"
#include "styledpixmap.h"

// --- cTimeLine  -------------------------------------------------------------

class cTimeLine {
private:
    cStyledPixmap *dateViewer = NULL;
    cPixmap *timeline = NULL;
    cStyledPixmap *clock = NULL;
    cPixmap *timeBase = NULL;
    cString lastClock;
    void DecorateTile(int posX, int posY, int tileWidth, int tileHeight);
    void DrawRoundedCorners(int posX, int posY, int width, int height, int radius);
    cImage *CreateBackgroundImage(int width, int height, tColor clrBgr, tColor clrBlend);
    void DrawDateViewer(void);
    void DrawTimeline(void);
public:
    cTimeLine(void);
    virtual ~cTimeLine(void);
    void Draw(void);
    void DrawTimeIndicator(void);
    bool DrawClock(void);
};

#endif //__TVGUIDE_TIMELINE_H
