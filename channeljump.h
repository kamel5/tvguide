#ifndef __TVGUIDE_CHANNELJUMP_H
#define __TVGUIDE_CHANNELJUMP_H

#include "styledpixmap.h"

// --- cChannelJump  -------------------------------------------------------------

class cChannelJump : public cStyledPixmap {
private:
    int channel;
    int maxChannels;
    long unsigned int startTime;
    long unsigned int timeout;
    cPixmap *pixmapBack = NULL;
    cPixmap *pixmapText = NULL;
    void DrawHeader(void);
    void DrawText(void);
    void CreatePixmaps(void);
    cString BuildChannelString(void);
public:
    cChannelJump(int lastValidChannel);
    virtual ~cChannelJump(void);
    void Set(int num);
    bool TimeOut(void);
    int GetChannel(void) { return channel; };
};

#endif //__TVGUIDE_CHANNELJUMP_H
