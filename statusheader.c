#include "config.h"
#include "tools.h"
#include "services/scraper2vdr.h"
#include "imageloader.h"
#include "statusheader.h"

cStatusHeader::cStatusHeader(void) {
    color = theme.Color(clrStatusHeader);
    colorBlending = theme.Color(clrStatusHeaderBlending);
    height = geoManager.statusHeaderHeight;
    width = geoManager.headerContentWidth;
    tvFrameWidth = geoManager.tvFrameWidth;
    pixmap = osdManager->CreatePixmap(__func__, "pixmap", 1, cRect(0, 0, width, height));
    pixmapText = osdManager->CreatePixmap(__func__, "pixmapText", 2, cRect(0, 0, width, height));
    pixmapTVFrame = osdManager->CreatePixmap(__func__, "pixmapTVFrame", 1, cRect(width, 0, tvFrameWidth, height));
}

cStatusHeader::~cStatusHeader(void) {
    osdManager->DestroyPixmap(pixmapText);
    osdManager->DestroyPixmap(pixmapTVFrame);
}

void cStatusHeader::Draw(void) {
    osdManager->Fill(pixmapText, clrTransparent);
    osdManager->Fill(pixmapTVFrame, clrTransparent);
    if (config.style == eStyleGraphical) {
        if (config.scaleVideo) {
            DrawBackgroundGraphical(bgStatusHeaderWindowed);
            cImage *tvFrameBack = imgCache->GetOsdElement(oeStatusHeaderTVFrame);
            if (tvFrameBack)
                osdManager->DrawImage(pixmapTVFrame, cPoint(0,0), *tvFrameBack);
        } else {
            DrawBackgroundGraphical(bgStatusHeaderFull);
        }
    } else {
        if (config.decorateVideo) {
            DecorateVideoFrame();
        }
        DrawBackground();
        DrawBorder();
    }
}

void cStatusHeader::DrawInfoText(cGridElement *grid) {
    int border = 10;
    int textWidth = 0;
    textWidth = width - 2 * border;
    tColor colorTextBack = (config.style == eStyleFlat)?color:clrTransparent;
    osdManager->Fill(pixmapText, clrTransparent);
    int x = border;
    int y = border;
    if (!grid->IsDummy()) {
        const cEvent *event = grid->GetEvent();
        int newX = DrawPoster(event, x, y, height - 2 * border, border);
        if (newX > 0) {
            textWidth -= (newX - x);
            x += newX;
        }
        cString time = grid->GetTimeString();
        cString title = cString::sprintf("%s", event->Title());
        cString shorttext = cString::sprintf("%s", event->ShortText() ? event->ShortText() : "");
        cString header = cString::sprintf("%s: %s", *time, *title);
        cString cuttedHeader = cString::sprintf("%s", CutText(*header, textWidth, fontManager->FontStatusHeaderLarge).c_str());
        cString cuttedShortText = cString::sprintf("%s", CutText(*shorttext, textWidth, fontManager->FontStatusHeader).c_str());
        osdManager->DrawText(pixmapText, cPoint(x,y), *cuttedHeader, theme.Color(clrFont), colorTextBack, fontManager->FontStatusHeaderLarge);
        y += fontManager->FontStatusHeaderLarge->Height() + border;
        if (!isempty(cuttedShortText)) {
            osdManager->DrawText(pixmapText, cPoint(x,y), *cuttedShortText, theme.Color(clrFont), colorTextBack, fontManager->FontStatusHeader);
            y += fontManager->FontStatusHeader->Height() + border;
            }
        int heightText = (pixmapText) ? pixmapText->ViewPort().Height() - y : 0;
        cTextWrapper description;
        description.Set(event->Description(), fontManager->FontStatusHeader, textWidth);
        int lineHeight = fontManager->FontStatusHeader->Height();
        int textLines = description.Lines();
        int maxLines = heightText / lineHeight;
        int lines = std::min(textLines, maxLines);
        for (int i = 0; i < lines-1; i++) {
            osdManager->DrawText(pixmapText, cPoint(x,y), description.GetLine(i), theme.Color(clrFont), colorTextBack, fontManager->FontStatusHeader);
            y += lineHeight;
        }
        cString lastLine = description.GetLine(lines-1);
        if (textLines > maxLines) {
            lastLine = cString::sprintf("%s...", *lastLine);
        }
        osdManager->DrawText(pixmapText, cPoint(x,y), *lastLine, theme.Color(clrFont), colorTextBack, fontManager->FontStatusHeader);
        return;
    }
    int heightText = (pixmapText) ? pixmapText->ViewPort().Height() - y : 0;
    y += (heightText - fontManager->FontStatusHeaderLarge->Height() - 2 * border) / 2;
    osdManager->DrawText(pixmapText, cPoint(x,y), *grid->GetText(), theme.Color(clrFont), colorTextBack, fontManager->FontStatusHeaderLarge);
}

int cStatusHeader::DrawPoster(const cEvent *event, int x, int y, int height, int border) {
    ScraperGetPoster posterScraper2Vdr;
    static cPlugin *pScraper = GetScraperPlugin();
    if (pScraper) {
        posterScraper2Vdr.event = event;
        posterScraper2Vdr.recording = NULL;
        if (pScraper->Service("GetPoster", &posterScraper2Vdr)) {
            int posterWidthMax = (width - 2 * border) / 3;
            int posterWidth = posterScraper2Vdr.poster.width;
            int posterHeight = posterScraper2Vdr.poster.height;
	    if (posterWidth > posterWidthMax) {
                posterWidth = posterWidthMax;
            }
            if (posterHeight > height) {
                posterHeight = height;
            }
            cImageLoader imgLoader;
            if (imgLoader.LoadPoster(posterScraper2Vdr.poster.path.c_str(), posterWidth, posterHeight)) {
                cImage poster = imgLoader.GetImage();
                osdManager->DrawImage(pixmapText, cPoint(x, y + (height - poster.Height()) / 2), poster);
                return poster.Width() + border;
            }
        }
    }
    return 0;
}

void cStatusHeader::DecorateVideoFrame(void) {
    int radius = 16;
    int frame = 2;
    osdManager->DrawRectangle(pixmapTVFrame, cRect(0, 0, tvFrameWidth, frame), theme.Color(clrBackgroundOSD));
    osdManager->DrawEllipse(pixmapTVFrame, cRect(frame,frame,radius,radius), theme.Color(clrBackgroundOSD), -2);
    osdManager->DrawRectangle(pixmapTVFrame, cRect(tvFrameWidth - frame, frame, frame, height - 2 * frame), theme.Color(clrBackgroundOSD));      
    osdManager->DrawEllipse(pixmapTVFrame, cRect(tvFrameWidth - radius - frame, frame, radius, radius), theme.Color(clrBackgroundOSD), -1);
    osdManager->DrawRectangle(pixmapTVFrame, cRect(0, frame, frame, height - 2 * frame), theme.Color(clrBackgroundOSD)); 
    osdManager->DrawEllipse(pixmapTVFrame, cRect(frame, height - radius - frame, radius, radius), theme.Color(clrBackgroundOSD), -3);
    osdManager->DrawRectangle(pixmapTVFrame, cRect(0, height - frame, tvFrameWidth, frame), theme.Color(clrBackgroundOSD));
    osdManager->DrawEllipse(pixmapTVFrame, cRect(tvFrameWidth - radius - frame, height - radius - frame, radius, radius), theme.Color(clrBackgroundOSD), -4);
}
