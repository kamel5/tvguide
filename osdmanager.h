#ifndef __TVGUIDE_OSDMANAGER_H
#define __TVGUIDE_OSDMANAGER_H

#include <vdr/osd.h>
#include "styledpixmap.h"

class cOsdManager {
private:
    cOsd *osd;
    cMutex mutex;
    void DeleteOsd(void);
public:
    cOsdManager(void);
    virtual ~cOsdManager(void);
    void Lock(void);
    void Unlock(void);
    bool CreateOsd(void);
    void ScaleVideo(void);
    void SetBackground(void);
    cPixmap *CreatePixmap(const char *func, cString Name, int Layer, const cRect &ViewPort, const cRect &DrawPort = cRect::Null);
    cPixmap *DestroyPixmap(cPixmap *pixmap, const char *func = NULL, cString Name = "");
    void Fill(cPixmap *p, tColor Color) { if (p) p->Fill(Color); };
    void Fill(cStyledPixmap *p, tColor Color) { if (p) p->Fill(Color); };
    void SetLayer(cPixmap *p, int Layer) { if (p) p->SetLayer(Layer); };
    void SetLayer(cStyledPixmap *p, int Layer) { if (p) p->SetLayer(Layer); };
    void SetAlpha(cPixmap *p, int Alpha) { if (p) p->SetAlpha(Alpha); };
    void DrawRectangle(cPixmap *p, const cRect &Rect, tColor Color) { if (p) p->DrawRectangle(Rect, Color); };
    void DrawRectangle(cStyledPixmap *p, const cRect &Rect, tColor Color) { if (p) p->DrawRectangle(Rect, Color); };
    void DrawEllipse(cPixmap *p, const cRect &Rect, tColor Color, int Quadrants = 0) { if (p) p->DrawEllipse(Rect, Color, Quadrants); };
    void DrawEllipse(cStyledPixmap *p, const cRect &Rect, tColor Color, int Quadrants = 0) { if (p) p->DrawEllipse(Rect, Color, Quadrants); };
    void DrawImage(cPixmap *p, const cPoint &Point, const cImage &Image) {if (p) p->DrawImage(Point, Image); };
    void DrawImage(cStyledPixmap *p, const cPoint &Point, const cImage &Image) {if (p) p->DrawImage(Point, Image); };
    void DrawText(cPixmap *p, const cPoint &Point, const char *s, tColor ColorFg, tColor ColorBg, const cFont *Font, int Width = 0, int Height = 0, int Alignment = taDefault) { if (p) p->DrawText(Point, s, ColorFg, ColorBg, Font, Width, Height, Alignment); };
    void DrawText(cStyledPixmap *p, const cPoint &Point, const char *s, tColor ColorFg, tColor ColorBg, const cFont *Font, int Width = 0, int Height = 0, int Alignment = taDefault) { if (p) p->DrawText(Point, s, ColorFg, ColorBg, Font, Width, Height, Alignment); };
    void SetViewPort(cPixmap *p, const cRect &Rect) { if (p) p->SetViewPort(Rect); };
    void SetViewPort(cStyledPixmap *p, const cRect &Rect) { if (p) p->SetViewPort(Rect); };
    void Flush(void);
    int Width(void) { return osd->Width(); };
    int Height(void) { return osd->Height(); };
    int Top(void) { return osd->Top(); };
    int Left(void) { return osd->Left(); };
};

extern cOsdManager *osdManager;

#endif //__TVGUIDE_OSDMANAGER_H
