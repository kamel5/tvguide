#ifndef __TVGUIDE_DUMMYELEMENT_H
#define __TVGUIDE_DUMMYELEMENT_H

#include "gridelement.h"
#include "channelepg.h"

// --- cDummyElement  -------------------------------------------------------------

class cDummyElement : public cGridElement {
private:
    time_t start;
    time_t end;
    cString strText;
    void DrawText(void);
    time_t Duration(void);
public:
    cDummyElement(cChannelEpg *owner, time_t start, time_t end);
    virtual ~cDummyElement(void);
    void SetViewportHeight(void);
    void PositionPixmap(void);
    void SetText(void);
    const cEvent *GetEvent(void) {return NULL;};
    time_t StartTime(void) { return start; };
    time_t EndTime(void) { return end; };
    void SetStartTime(time_t start) { this->start = start; };
    void SetEndTime(time_t end) { this->end = end; };
    int CalcOverlap(cGridElement *neighbor);
    void SetTimer(void) {};
    cString GetText(void);
    cString GetTimeString(void);
    void Debug(void);
};

#endif //__TVGUIDE_DUMMYELEMENT_H
