#include <time.h>
#include <vdr/tools.h>
#include "config.h"
#include "timemanager.h"

cTimeManager *timeManager;

cTimeManager::cTimeManager(void) {
    if (config.displayMode == eVertical) {
        displaySeconds = (geoManager.osdHeight - geoManager.statusHeaderHeight - geoManager.channelHeaderHeight - geoManager.channelGroupsHeight - geoManager.footerHeight) / geoManager.minutePixel * 60;
    } else if (config.displayMode == eHorizontal) {
        displaySeconds = (geoManager.osdWidth - geoManager.channelHeaderWidth - geoManager.channelGroupsWidth) / geoManager.minutePixel * 60;
    }
    Now();
}

cTimeManager::~cTimeManager(void) {
}

cString cTimeManager::PrintTime(time_t displayTime) {
    struct tm *ts;
    ts = localtime(&displayTime);
    cString strTime = cString::sprintf("%d.%d-%d:%d.%d", ts->tm_mday, ts->tm_mon + 1, ts->tm_hour, ts->tm_min, ts->tm_sec);
    return strTime;
}

void cTimeManager::Now(void) {
    tNow = time(0);
    tStart = tNow;
    tStart = GetRounded();
    tEnd   = tStart + displaySeconds;
}

bool cTimeManager::ScrollMinutes(int timeMode, int scrollMode) {
    int minutes = (timeMode == eTimeHuge) ? (((config.displayMode == eVertical) ? config.hugeStepHours : config.hugeStepHoursHorizontal) * 60)
                                          : (timeMode == eTimeBig) ? (((config.displayMode == eVertical) ? config.bigStepHours : config.bigStepHoursHorizontal) * 60)
                                                                   : config.stepMinutes;
    int step = (scrollMode == eForward) ? minutes : -minutes;
    if ((timeMode == eTimeHuge || timeMode == eTimeBig) && (tStart + step * 60) + 30 * 60 < tNow) {
        time_t tStartAlt = tStart;
        Now();
        if (tStart == tStartAlt)
            return false;
    } else if (((tStart + step * 60) + 30 * 60) < (tNow - Setup.EPGLinger * 60)) {
        return false;
    } else {
        tStart += step * 60;
        tEnd    = tStart + displaySeconds;
    }
    return true;
}

void cTimeManager::SetTime(time_t newTime) {
    tStart = newTime;
    tEnd   = tStart + displaySeconds;
}

time_t cTimeManager::GetPrevPrimetime(time_t current) {
    tm *st = localtime(&current);
    if (st->tm_hour < 21) {
        current -= 24 * 60 * 60;
        st = localtime(&current);
    }
    st->tm_hour = 20;
    st->tm_min = 0;
    time_t primeTime = mktime(st);
    return primeTime;
}

time_t cTimeManager::GetNextPrimetime(time_t current){
    tm *st = localtime(&current);
    if (st->tm_hour > 19) {
        current += 24 * 60 * 60;
        st = localtime(&current);
    }
    st->tm_hour = 20;
    st->tm_min = 0;
    time_t primeTime = mktime(st);
    return primeTime;
}

bool cTimeManager::TooFarInPast(time_t current) {
    if (current < tNow) {
        return true;
    }
    return false;
}

cString cTimeManager::GetCurrentTime(void) {
    char buf[25];
    tNow = time(0);
    tm *st = localtime(&tNow);
    //snprintf(text, sizeof(text), "%d:%02d", st->tm_hour, st->tm_min);
    if (config.timeFormat == e12Hours) {
        strftime(buf, sizeof(buf), "%I:%M %p", st);
    } else if (config.timeFormat == e24Hours)
        strftime(buf, sizeof(buf), "%H:%M", st);
    return buf;
    
}

cString cTimeManager::GetDate(void) {
    char text[6];
    volatile int text_size = sizeof(text);
    tm *st = localtime(&tStart);
    snprintf(text, text_size, "%d.%d", st->tm_mday, st->tm_mon + 1);
    return text;
}

cString cTimeManager::GetWeekday(void) {
    return WeekDayName(tStart);
}

int cTimeManager::GetTimelineOffset(void) {
    tm *st = localtime(&tStart);
    int offset = st->tm_hour * 60;
    offset += st->tm_min;
    return offset;
}

time_t cTimeManager::GetRounded(void) {
    tm *rounded = localtime ( &tStart );
    rounded->tm_sec = 0;
    if (rounded->tm_min > 29)
        rounded->tm_min = 30;
    else
        rounded->tm_min = 0;
    return mktime(rounded);
}

bool cTimeManager::NowVisible(void) {
    if (tNow > tStart)
        return true;
    return false;
}

void cTimeManager::Debug(void) {
    esyslog("tvguide: now: %s, tStart: %s, tEnd: %s", *TimeString(tNow), *TimeString(tStart), *TimeString(tEnd));
}

// --- cTimeInterval ------------------------------------------------------------- 

cTimeInterval::cTimeInterval(time_t start, time_t stop) {
    this->start = start;
    this->stop = stop;
}

cTimeInterval::~cTimeInterval(void) {
}

cTimeInterval *cTimeInterval::Intersect(cTimeInterval *interval) {
    time_t startIntersect, stopIntersect;
    
    if ((stop <= interval->Start()) || (interval->Stop() <= start)) {
        return NULL;
    }
    
    if (start <= interval->Start()) {
        startIntersect = interval->Start();
    } else {
        startIntersect = start;
    }
    if (stop <= interval->Stop()) {
        stopIntersect = stop;
    } else {
        stopIntersect = interval->Stop();
    }
    return new cTimeInterval(startIntersect, stopIntersect);
}

cTimeInterval *cTimeInterval::Union(cTimeInterval *interval) {
    time_t startUnion, stopUnion;
    
    if (start <= interval->Start()) {
        startUnion = start;
    } else {
        startUnion = interval->Start();
    }
    if (stop <= interval->Stop()) {
        stopUnion = interval->Stop();
    } else {
        stopUnion = stop;
    }
    return new cTimeInterval(startUnion, stopUnion);
}
