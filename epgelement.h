#ifndef __TVGUIDE_EPGELEMENT_H
#define __TVGUIDE_EPGELEMENT_H

#include <vdr/epg.h>
#include "config.h"
#include "gridelement.h"
#include "channelepg.h"

// --- cEpgElement  -------------------------------------------------------------

class cEpgElement : public cGridElement {
private:
    const cTimer *timer;
    const cEvent *event;
    cTextWrapper *extText;
    cString timeString;
    void DrawText(void);
    void DrawIcon(cString iconText, tColor color);
    time_t Duration(void) { return event->Duration(); };
public:
    cEpgElement(cChannelEpg *owner, const cEvent *event = NULL);
    virtual ~cEpgElement(void);
    void SetViewportHeight(void);
    void PositionPixmap(void);
    void SetText(void);
    const cEvent *GetEvent(void) { return event; };
    time_t StartTime(void) { return event->StartTime(); };
    time_t EndTime(void) { return event->EndTime(); };
    void SetTimer(void);
    void SetSwitchTimer(void);
    cString GetTimeString(void);
    void Debug(void);
};

#endif //__TVGUIDE_EPGELEMENT_H
