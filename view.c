#include "config.h"
#include "detailview.h"
#include "switchtimer.h"

/********************************************************************************************
* cView
********************************************************************************************/

cView::cView(void) : cThread("View") {
    activeView = 0;
    scrollable = false;
    tabbed = false;
    font = NULL;
    fontSmall = NULL;
    fontHeader = NULL;
    fontHeaderLarge = NULL;
    title = "";
    subTitle = "";
    dateTime = "";
    infoText = "";
    channel = NULL;
    eventID = 0;
    event = NULL;
    x = 0;
    y = 0;
    width = 0;
    height = 0;
    border = 0;
    headerHeight = 0;
    contentHeight = 0;
    tabHeight = 0;
    headerDrawn = false;
}

cView::~cView(void) {
    osdManager->DestroyPixmap(pixmapBackground, __func__, "pixmapBackground");
    if (pixmapHeader)
        delete pixmapHeader;
    osdManager->DestroyPixmap(pixmapHeaderLogo, __func__, "pixmapHeaderLogo");
    osdManager->DestroyPixmap(pixmapContent, __func__, "pixmapContent");
    osdManager->DestroyPixmap(pixmapPoster, __func__, "pixmapPoster");
    osdManager->DestroyPixmap(pixmapTabs, __func__, "pixmapTabs");
    osdManager->DestroyPixmap(pixmapScrollbar, __func__, "pixmapScrollbar");
    osdManager->DestroyPixmap(pixmapScrollbarBack, __func__, "pixmapScrollbar");
}

void cView::SetFonts(void) {
    font = fontManager->FontDetailView;
    fontSmall = fontManager->FontDetailViewSmall;
    fontHeaderLarge = fontManager->FontDetailHeaderLarge;
    fontHeader = fontManager->FontDetailHeader;
}

void cView::SetGeometry(void) { 
    x = 0;
    y = 0;
    scrollbarWidth = 40;
    width = geoManager.osdWidth - scrollbarWidth;
    height = geoManager.osdHeight;
    border = config.epgViewBorder;
    headerWidth = geoManager.headerContentWidth;
    headerHeight = geoManager.epgViewHeaderHeight;
    if (tabbed)
        tabHeight = font->Height() * 3 / 2;
    contentHeight = height - headerHeight - tabHeight - geoManager.footerHeight;
}

void cView::DrawHeader(void) {
    if (!pixmapHeader) {
        pixmapHeader = new cStyledPixmap(osdManager->CreatePixmap(__func__, "pixmapHeader", 5, cRect(0, 0, headerWidth, headerHeight)));
        pixmapHeader->SetColor(theme.Color(clrHeader), theme.Color(clrHeaderBlending));
    }
    if (!pixmapHeaderLogo) {
        pixmapHeaderLogo = osdManager->CreatePixmap(__func__, "pixmapHeaderLogo", 6, cRect(0, 0, width, headerHeight));
    }
    osdManager->Fill(pixmapHeader, clrTransparent);
    osdManager->Fill(pixmapHeaderLogo, clrTransparent);

    if (!pixmapHeader)
        return;

    if (config.style == eStyleGraphical) {
        if (config.scaleVideo) {
            pixmapHeader->DrawBackgroundGraphical(bgStatusHeaderWindowed);
        } else {
            pixmapHeader->DrawBackgroundGraphical(bgStatusHeaderFull);
        }
    } else {
        pixmapHeader->DrawBackground();
        pixmapHeader->DrawBoldBorder();
    }
    //Channel Logo
    int logoHeight = 2 * headerHeight / 3;
    int logoWidth = logoHeight * config.logoWidthRatio / config.logoHeightRatio;
    int xText = border / 2;
    if (channel && !config.hideChannelLogos) {
        cImageLoader imgLoader;
        if (imgLoader.LoadLogo(channel, logoWidth, logoHeight)) {
            cImage logo = imgLoader.GetImage();
            const int logoheight = logo.Height();
            const int logowidth = logo.Width();
            osdManager->DrawImage(pixmapHeaderLogo, cPoint(xText + ((logoWidth - logowidth) / 2), ((headerHeight - logoheight) / 2)), logo);
            xText += logoWidth + border / 2;
        }
    }
    //Date and Time, Title, Subtitle
    int textWidthMax = headerWidth - xText - border / 2;
    wrapper.Set(title.c_str(), fontHeaderLarge, textWidthMax);
    int lineHeight = fontHeaderLarge->Height();
    int textLines = wrapper.Lines();
    int yDateTime = border / 2;
    int yTitle = (headerHeight - textLines * lineHeight) / 2;
    int ySubtitle = headerHeight - fontHeader->Height() - border / 3;

    pixmapHeader->DrawText(cPoint(xText, yDateTime), CutText(dateTime, textWidthMax, fontHeader).c_str(), theme.Color(clrFont), theme.Color(clrStatusHeader), fontHeader);
    for (int i = 0; i < textLines; i++) {
        pixmapHeader->DrawText(cPoint(xText, yTitle + i * lineHeight), wrapper.GetLine(i), theme.Color(clrFont), theme.Color(clrStatusHeader), fontHeaderLarge);
    }
    pixmapHeader->DrawText(cPoint(xText, ySubtitle), CutText(subTitle, textWidthMax, fontHeader).c_str(), theme.Color(clrFont), theme.Color(clrStatusHeader), fontHeader);
    //REC Icon
    eTimerMatch timerMatch = tmNone; 
    if (!event)
        return;
    const cTimer *ti;
    if (config.useRemoteTimers && pRemoteTimers) {
        RemoteTimers_GetMatch_v1_0 rtMatch;
        rtMatch.event = event;
        pRemoteTimers->Service("RemoteTimers::GetMatch-v1.0", &rtMatch);
        timerMatch = (eTimerMatch)rtMatch.timerMatch;
        ti = rtMatch.timer;
    } else {
        LOCK_TIMERS_READ;
        ti = Timers->GetMatch(event, &timerMatch);
    }
    bool hasSwitchTimer = SwitchTimers.EventInSwitchList(event);
    if (hasSwitchTimer || (ti && timerMatch == tmFull)) {
        tColor iconColor;
        bool switchOnly = false;
        bool timerActive = ti && ti->HasFlags(tfActive);
        cString recIconText;
#ifdef SWITCHONLYPATCH
        switchOnly = ti && ti->HasFlags(tfSwitchOnly);
#endif
        (hasSwitchTimer || switchOnly) ? recIconText = "Switch" : recIconText = " REC ";
        iconColor = (hasSwitchTimer || switchOnly) ? theme.Color(clrButtonYellow) : timerActive ? theme.Color(clrButtonRed) : theme.Color(clrButtonGreen);
        int widthIcon = fontManager->FontDetailHeader->Width(*recIconText) + 10;
        int height = fontManager->FontDetailHeader->Height() + 10;
        int posX = headerWidth - widthIcon - 25;
        int posY = ySubtitle - 5;
        osdManager->DrawRectangle(pixmapHeader, cRect(posX, posY, widthIcon, height), iconColor);
        osdManager->DrawText(pixmapHeader, cPoint(posX + 5, posY + 5), *recIconText, theme.Color(clrFont), iconColor, fontManager->FontDetailHeader);
    }
}

void cView::DrawTabs(void) {
    if (!pixmapTabs) {
        pixmapTabs = osdManager->CreatePixmap(__func__, "pixmapTabs", 4, cRect(0, y + headerHeight + contentHeight, width + scrollbarWidth, tabHeight));
    }
    tColor bgColor = theme.Color(clrTabInactive);
    osdManager->Fill(pixmapTabs, clrTransparent);
    osdManager->DrawRectangle(pixmapTabs, cRect(0, 0, width, 2), bgColor);
    int numTabs = tabs.size();
    int xCurrent = 0;
    for (int tab = 0; tab < numTabs; tab++) {
        std::string tabText = tabs[tab];
        int textWidth = font->Width(tabText.c_str());
        int tabWidth = textWidth + border;
        osdManager->DrawRectangle(pixmapTabs, cRect(xCurrent, (tab == activeView) ? 0 : 2, tabWidth - 2, tabHeight), bgColor);
        osdManager->DrawEllipse(pixmapTabs, cRect(xCurrent, tabHeight - border / 2, border / 2, border / 2), clrTransparent, -3);
        osdManager->DrawEllipse(pixmapTabs, cRect(xCurrent + tabWidth - border / 2 - 2, tabHeight - border / 2, border / 2, border / 2), clrTransparent, -4);
        if (tab == activeView) {
            osdManager->DrawRectangle(pixmapTabs, cRect(xCurrent + 2, 0, tabWidth - 6, tabHeight - border / 2 - 1), clrTransparent);
            osdManager->DrawRectangle(pixmapTabs, cRect(xCurrent + border / 2, tabHeight - border / 2 - 1, tabWidth - border, border / 2 - 1), clrTransparent);
            osdManager->DrawEllipse(pixmapTabs, cRect(xCurrent + 2, tabHeight - border / 2 - 2, border / 2, border / 2), clrTransparent, 3);
            osdManager->DrawEllipse(pixmapTabs, cRect(xCurrent + tabWidth - border / 2 - 4, tabHeight - border / 2 - 2, border / 2, border / 2), clrTransparent, 4);
        }
        osdManager->DrawText(pixmapTabs, cPoint(xCurrent + (tabWidth - textWidth) / 2, 2 + (tabHeight - font->Height()) / 2), tabText.c_str(), theme.Color(clrFont), (tab == activeView) ? clrTransparent : bgColor, font);
        xCurrent += tabWidth;
    }
}

void cView::ClearContent(void) {
    if (Running()) {
        pixmapContent = osdManager->DestroyPixmap(pixmapContent, __func__, "pixmapContent");
        pixmapBackground = osdManager->DestroyPixmap(pixmapBackground, __func__, "pixmapBackground");
        pixmapPoster = osdManager->DestroyPixmap(pixmapPoster, __func__, "pixmapPoster");
    }
}

void cView::CreateContent(int fullHeight) {
    scrollable = false;
    pixmapBackground = osdManager->CreatePixmap(__func__, "pixmapBackground", 3, cRect(x, y + headerHeight, width + scrollbarWidth, contentHeight + tabHeight));
    osdManager->Fill(pixmapBackground, theme.Color(clrBackground));

    int drawPortHeight = contentHeight; 
    if (fullHeight > contentHeight) {
        drawPortHeight = fullHeight;
        scrollable = true;
    }
    pixmapContent = osdManager->CreatePixmap(__func__, "pixmapContent", 4, cRect(x, y + headerHeight, width, contentHeight), cRect(0, 0, width, drawPortHeight));
    osdManager->Fill(pixmapContent, clrTransparent);
}

void cView::DrawContent(std::string *text, cTvMedia *img) {
//    osdManager->DestroyPixmap(pixmapPoster, __func__, "pixmapPoster");

    int logoWidth = border;
    if (img) {
        int imgWidth = (width - 2 * border) / 3;
        int imgHeight = contentHeight - 2 * border;

        cImageLoader imgLoader;
        if (imgLoader.LoadPoster(img->path.c_str(), imgWidth, imgHeight)) {
            cImage logo = imgLoader.GetImage();

            int xPoster = x + width - logo.Width() - 2 * border;
            int yPoster = y + headerHeight;
            pixmapPoster = osdManager->CreatePixmap(__func__, "pixmapPoster", 5,
                                                    cRect(xPoster,
                                                          yPoster,
                                                          logo.Width() + 2 * border,
                                                          contentHeight));
            if (Running() && pixmapPoster) {
                osdManager->Fill(pixmapPoster, clrTransparent);
                osdManager->DrawImage(pixmapPoster, cPoint(border, border), imgLoader.GetImage());
                logoWidth = pixmapPoster->ViewPort().Width();;
            }
        }
    }

    cTextWrapper wText;
    wText.Set(text->c_str(), font, width - border - logoWidth);
    int lineHeight = font->Height();
    int textLines = wText.Lines();
    int textHeight = lineHeight * textLines + 2 * border;
    int yText = border;
    CreateContent(textHeight);

    for (int i = 0; i < textLines; i++) {
        osdManager->DrawText(pixmapContent, cPoint(border, yText), wText.GetLine(i), theme.Color(clrFont), clrTransparent, font);
        yText += lineHeight;
    }
}

void cView::DrawFloatingContent(std::string *infoText, cTvMedia *img, cTvMedia *img2) {
    cTextWrapper wTextTall;
    cTextWrapper wTextFull;
    int imgWidth = img->width;
    int imgHeight = img->height;
    int imgWidth2 = 0;
    int imgHeight2 = 0;
    if (imgHeight > (contentHeight - 2 * border)) {
        imgHeight = contentHeight - 2 * border;
        imgWidth = imgWidth * ((double)imgHeight / (double)img->height);
    }
    int imgHeightTotal = imgHeight;
    if (img2) {
        imgWidth2 = imgWidth;
        imgHeight2 = img2->height * ((double)imgWidth2 / (double)img2->width);
        imgHeightTotal += imgHeight2 + border;
    }
    CreateFloatingTextWrapper(&wTextTall, &wTextFull, infoText, imgWidth, imgHeightTotal);
    int lineHeight = font->Height();
    int textLinesTall = wTextTall.Lines();
    int textLinesFull = wTextFull.Lines();
    int textHeight = lineHeight * (textLinesTall + textLinesFull);
    int yText = border;
    CreateContent(std::max(textHeight, imgHeight) + 2 * border);

    for (int i = 0; i < textLinesTall; i++) {
        osdManager->DrawText(pixmapContent, cPoint(border, yText), wTextTall.GetLine(i), theme.Color(clrFont), clrTransparent, font);
        yText += lineHeight;
    }
    for (int i = 0; i < textLinesFull; i++) {
        osdManager->DrawText(pixmapContent, cPoint(border, yText), wTextFull.GetLine(i), theme.Color(clrFont), clrTransparent, font);
        yText += lineHeight;
    }
    cImageLoader imgLoader;
    if (imgLoader.LoadPoster(img->path.c_str(), imgWidth, imgHeight)) {
        if (Running() && pixmapContent)
            osdManager->DrawImage(pixmapContent, cPoint(width - imgWidth - border, border), imgLoader.GetImage());
    }
    if (!img2)
        return;

    if (imgLoader.LoadPoster(img2->path.c_str(), imgWidth2, imgHeight2)) {
        if (Running() && pixmapContent)
            osdManager->DrawImage(pixmapContent, cPoint(width - imgWidth2 - border, imgHeight + 2 * border), imgLoader.GetImage());
    }
}

void cView::CreateFloatingTextWrapper(cTextWrapper *twNarrow, cTextWrapper *twFull, std::string *text, int widthImg, int heightImg) {
    int lineHeight = font->Height();
    int linesNarrow = (heightImg + border) / lineHeight;
    int linesDrawn = 0;
    int y = 0;
    int widthNarrow = width - 3 * border - widthImg;
    bool drawNarrow = true;
    
    splitstring s(text->c_str());
    std::vector<std::string> flds = s.split('\n', 1);

    if (flds.size() < 1)
        return;

    std::stringstream sstrTextTall;
    std::stringstream sstrTextFull;
      
    for (int i = 0; i < (int)flds.size(); i++) {
        if (!flds[i].size()) {
            //empty line
            linesDrawn++;
            y += lineHeight;
            if (drawNarrow)
                sstrTextTall << "\n";
            else
                sstrTextFull << "\n";
        } else {
            cTextWrapper wrapper;
            if (drawNarrow) {
                wrapper.Set((flds[i].c_str()), font, widthNarrow);
                int newLines = wrapper.Lines();
                //check if wrapper fits completely into narrow area
                if (linesDrawn + newLines < linesNarrow) {
                    for (int line = 0; line < wrapper.Lines(); line++) {
                        sstrTextTall << wrapper.GetLine(line) << " ";
                    }
                    sstrTextTall << "\n";
                    linesDrawn += newLines;
                } else {
                    //this wrapper has to be splitted
                    for (int line = 0; line < wrapper.Lines(); line++) {
                        if (line + linesDrawn < linesNarrow) {
                            sstrTextTall << wrapper.GetLine(line) << " ";
                        } else {
                            sstrTextFull << wrapper.GetLine(line) << " ";
                        }
                    }
                    sstrTextFull << "\n";
                    drawNarrow = false;
                }
            } else {
                wrapper.Set((flds[i].c_str()), font, width - 2 * border);
                for (int line = 0; line < wrapper.Lines(); line++) {
                    sstrTextFull << wrapper.GetLine(line) << " ";        
                }
                sstrTextFull << "\n";
            }
        }
    }
    twNarrow->Set(sstrTextTall.str().c_str(), font, widthNarrow);
    twFull->Set(sstrTextFull.str().c_str(), font, width - 2 * border);
}

void cView::DrawActors(std::vector<cActor> *actors) {
    int numActors = actors->size();
    int picsPerLine = config.numPicturesPerLine;
    int actorsToView = 0;
    int thumbWidth = 0;
    int thumbHeight = 0;
    cImageLoader imgLoader;

    if (numActors > 0) {
        thumbWidth = (width - 2 * picsPerLine * border) / picsPerLine;
        thumbHeight = ((double)actors->at(0).actorThumb.height / (double)actors->at(0).actorThumb.width) * thumbWidth;

        for (int a = 0; a < numActors; a++) {
            if (a == numActors)
                break;
            std::string path = actors->at(a).actorThumb.path;
            if (imgLoader.LoadPoster(path.c_str(), thumbWidth, thumbHeight, false)) {
                actorsToView++;
            }
        }
    }

    if (actorsToView < 1) {
        CreateContent(100);
        osdManager->DrawText(pixmapContent, cPoint(border, border), tr("No Cast available"), theme.Color(clrFont), clrTransparent, fontHeaderLarge);
        return;
    }

    int picLines = actorsToView / picsPerLine;
    if (actorsToView % picsPerLine != 0)
        picLines++;
    
    int totalHeight = picLines * (thumbHeight + 2 * fontSmall->Height() + border + border / 2) + 2 * border + fontHeaderLarge->Height();

    CreateContent(totalHeight);
    if (!pixmapContent)
        return;

    cString header = cString::sprintf("%s:", tr("Cast"));
    osdManager->DrawText(pixmapContent, cPoint(border, border), *header, theme.Color(clrFont), clrTransparent, fontHeaderLarge);

    int x = 0;
    int y = 2 * border + fontHeaderLarge->Height();
    if (!Running())
        return;

    int actor = 0;
    for (int row = 0; row < picLines; row++) {
        for (int col = 0; col < picsPerLine;) {
            if (!Running())
                return;
            if (actor == numActors)
                break;
            std::string path = actors->at(actor).actorThumb.path;
            if (imgLoader.LoadPoster(path.c_str(), thumbWidth, thumbHeight)) {
                std::string name = actors->at(actor).name;
                std::stringstream sstrRole;
                sstrRole << "\"" << actors->at(actor).role << "\"";
                std::string role = sstrRole.str();
                if (fontSmall->Width(name.c_str()) > thumbWidth + 2 * border)
                    name = CutText(name, thumbWidth + 2 * border, fontSmall);
                if (fontSmall->Width(role.c_str()) > thumbWidth + 2 * border)
                    role = CutText(role, thumbWidth + 2 * border, fontSmall);
                int xName = x + ((thumbWidth + 2 * border) - fontSmall->Width(name.c_str())) / 2;
                int xRole = x + ((thumbWidth + 2 * border) - fontSmall->Width(role.c_str())) / 2;
                osdManager->DrawImage(pixmapContent, cPoint(x + border, y), imgLoader.GetImage());
                osdManager->DrawText(pixmapContent, cPoint(xName, y + thumbHeight + border / 2), name.c_str(), theme.Color(clrFont), clrTransparent, fontSmall);
                osdManager->DrawText(pixmapContent, cPoint(xRole, y + thumbHeight + border / 2 + fontSmall->Height()), role.c_str(), theme.Color(clrFont), clrTransparent, fontSmall);
                x += thumbWidth + 2 * border;
                col++;
            }
            actor++;
        }
        x = 0;
        y += thumbHeight + 2 * fontSmall->Height() + border + border / 2;
    }
}

void cView::ClearScrollbar(void) {
    osdManager->Fill(pixmapScrollbar, clrTransparent);
    osdManager->Fill(pixmapScrollbarBack, clrTransparent);
}

void cView::DrawScrollbar(void) {
    ClearScrollbar();
    if (!scrollable || !pixmapContent)
        return;

    if (!pixmapScrollbar) {
        pixmapScrollbar = osdManager->CreatePixmap(__func__, "pixmapScrollbar", 6, cRect(width, y + headerHeight, scrollbarWidth, contentHeight));
        osdManager->Fill(pixmapScrollbar, clrTransparent);
    }
    if (!pixmapScrollbarBack) {
        pixmapScrollbarBack = osdManager->CreatePixmap(__func__, "pixmapScrollbarBack", 5, cRect(width, y + headerHeight, scrollbarWidth, contentHeight));
        osdManager->Fill(pixmapScrollbarBack, clrTransparent);
    }
    if (!pixmapScrollbar || !pixmapScrollbarBack)
        return;

    int totalBarHeight = pixmapScrollbar->ViewPort().Height() - 6;
    
    int aktHeight = (-1) * pixmapContent->DrawPort().Point().Y();
    int totalHeight = pixmapContent->DrawPort().Height();
    int screenHeight = pixmapContent->ViewPort().Height();

    int barHeight = (double)(screenHeight * totalBarHeight) / (double)totalHeight ;
    int barTop = (double)(aktHeight * totalBarHeight) / (double)totalHeight ;

    cImage *imgScrollBar = CreateScrollbarImage(pixmapScrollbar->ViewPort().Width() - 10, barHeight, theme.Color(clrHighlight), theme.Color(clrHighlightBlending));

    osdManager->Fill(pixmapScrollbarBack, theme.Color(clrHighlightBlending));
    osdManager->DrawRectangle(pixmapScrollbarBack, cRect(2, 2, pixmapScrollbar->ViewPort().Width() - 4, pixmapScrollbar->ViewPort().Height() - 4), theme.Color(clrHighlightBlending));

    osdManager->DrawImage(pixmapScrollbar, cPoint(3, 3 + barTop), *imgScrollBar);
}

cImage *cView::CreateScrollbarImage(int width, int height, tColor clrBgr, tColor clrBlend) {
    cImage *image = new cImage(cSize(width, height));
    image->Fill(clrBgr);
    if (config.style != eStyleFlat) {
        int numSteps = 64;
        int alphaStep = 0x03;
        if (height < 30)
            return image;
        else if (height < 100) {
            numSteps = 32;
            alphaStep = 0x06;
        }
        int stepY = 0.5 * height / numSteps;
        if (stepY == 0)
            stepY = 1;
        int alpha = 0x40;
        tColor clr;
        for (int i = 0; i < numSteps; i++) {
            clr = AlphaBlend(clrBgr, clrBlend, alpha);
            for (int y = i * stepY; y < (i + 1) * stepY; y++) {
                for (int x = 0; x < width; x++) {
                    image->SetPixel(cPoint(x, y), clr);
                }
            }
            alpha += alphaStep;
        }
    }
    return image;
}

bool cView::KeyUp(void) { 
    if (!scrollable || !pixmapContent)
        return false;

    int aktHeight = pixmapContent->DrawPort().Point().Y();
    if (aktHeight >= 0) {
        return false;
    }
    int step = config.detailedViewScrollStep * font->Height();
    int newY = aktHeight + step;
    if (newY > 0)
        newY = 0;
    pixmapContent->SetDrawPortPoint(cPoint(0, newY));
    return true;
}

bool cView::KeyDown(void) { 
    if (!scrollable || !pixmapContent)
        return false;

    int aktHeight = pixmapContent->DrawPort().Point().Y();
    int totalHeight = pixmapContent->DrawPort().Height();
    int screenHeight = pixmapContent->ViewPort().Height();
    
    if (totalHeight - ((-1) * aktHeight) == screenHeight) {
        return false;
    } 
    int step = config.detailedViewScrollStep * font->Height();
    int newY = aktHeight - step;
    if ((-1) * newY > totalHeight - screenHeight)
        newY = (-1) * (totalHeight - screenHeight);
    pixmapContent->SetDrawPortPoint(cPoint(0, newY));
    return true;
}

/********************************************************************************************
* cEPGView : cView
********************************************************************************************/

cEPGView::cEPGView(void) : cView() {
    tabbed = true;
    numEPGPics = -1;
    numTabs = 0;
}

cEPGView::~cEPGView(void) {
    Cancel(2);
}

void cEPGView::SetTabs(void) {
    tabs.push_back(tr("EPG Info"));
    if (eventID > 0)
        tabs.push_back(tr("Reruns"));
    else
        tabs.push_back(tr("Recording Information"));
    if (numEPGPics > 0)
        tabs.push_back(tr("Image Galery"));
    numTabs = tabs.size();
}

void cEPGView::CheckEPGImages(void) {
    if (eventID > 0) {
        for (int i = 1; i <= config.numAdditionalEPGPictures; i++) {
            cString epgimage;
            epgimage = cString::sprintf("%s%d_%d.jpg", *config.epgImagePath, eventID, i);
            FILE *fp = fopen(*epgimage, "r");
            if (fp) {
                std::stringstream ss;
                ss << i;
                epgPics.push_back(ss.str());
                fclose(fp);
            } else {
                break;
            }
        }
    } else {
        return;
    }
    numEPGPics = epgPics.size();
}

void cEPGView::DrawImages(void) {
    int imgWidth = config.epgImageWidthLarge;
    int imgHeight = config.epgImageHeightLarge;

    int totalHeight = numEPGPics * (imgHeight + border);

    CreateContent(totalHeight);

    cImageLoader imgLoader;
    int yPic = border;
    for (int pic = 0; pic < numEPGPics; pic++) {
        bool drawPic = false;
        if (eventID > 0) {
            cString epgimage = cString::sprintf("%d_%d", eventID, atoi(epgPics[pic].c_str()));
            if (imgLoader.LoadAdditionalEPGImage(epgimage)) {
                drawPic = true;
            }

        }
        if (drawPic) {
            osdManager->DrawImage(pixmapContent, cPoint((width - imgWidth) / 2, yPic), imgLoader.GetImage());
            yPic += imgHeight + border;
        }
    } 
}

void cEPGView::KeyLeft(void) { 
    if (Running())
        return;
    activeView--;
    if (activeView < 0)
        activeView = numTabs - 1; 
}

void cEPGView::KeyRight(void) { 
    if (Running())
        return;
    activeView = (activeView + 1) % numTabs;
}

void cEPGView::Action(void) {
    ClearContent();
    if (!headerDrawn) {
        DrawHeader();
        headerDrawn = true;
    }
    if (tabs.size() == 0) {
        CheckEPGImages();
        SetTabs();
    }
    DrawTabs();
    switch (activeView) {
        case evtInfo:
            DrawContent(&infoText);
            break;
        case evtAddInfo:
            DrawContent(&addInfoText);
            break;
        case evtImages:
            DrawImages();
            break;
    }
    DrawScrollbar();
    osdManager->Flush();
}

/********************************************************************************************
* cSeriesView : cView
********************************************************************************************/

cSeriesView::cSeriesView(int seriesId, int episodeId) : cView() {
    this->seriesId = seriesId;
    this->episodeId = episodeId;
    tvdbInfo = "";
    tabbed = true;
}

cSeriesView::~cSeriesView(void) {
    Cancel(2);
}

void cSeriesView::LoadMedia(void) {
    static cPlugin *pScraper = GetScraperPlugin();
    if (!pScraper)
        return;

    series.seriesId = seriesId;
    series.episodeId = episodeId;
    pScraper->Service("GetSeries", &series);
}

void cSeriesView::SetTabs(void) {
    tabs.push_back(tr("EPG Info"));
    if (eventID > 0)
        tabs.push_back(tr("Reruns"));
    else
        tabs.push_back(tr("Recording Information"));
    tabs.push_back(tr("Cast"));
    tabs.push_back(tr("TheTVDB Info"));
    tabs.push_back(tr("Image Galery"));
}

void cSeriesView::CreateTVDBInfo(void) {
    if (tvdbInfo.size() > 0)
        return;
    std::stringstream info;
    info << tr("TheTVDB Information") << ":\n\n";

    if (series.episode.name.size() > 0) {
        info << tr("Episode") << ": " << series.episode.name << " (" << tr("Season") << " " << series.episode.season << ", " << tr("Episode") << " " << series.episode.number << ")\n\n";
    }
    if (series.episode.overview.size() > 0) {
        info << tr("Episode Overview") << ": " << series.episode.overview << "\n\n";
    }
    if (series.episode.firstAired.size() > 0) {
        info << tr("First aired") << ": " << series.episode.firstAired << "\n\n";
    }
    if (series.episode.guestStars.size() > 0) {
        info << tr("Guest Stars") << ": " << series.episode.guestStars << "\n\n";
    }
    if (series.episode.rating > 0) {
        info << tr("TheTVDB Rating") << ": " << series.episode.rating << "\n\n";
    }
    if (series.overview.size() > 0) {
        info << tr("Series Overview") << ": " << series.overview << "\n\n";
    }
    if (series.firstAired.size() > 0) {
        info << tr("First aired") << ": " << series.firstAired << "\n\n";
    }
    if (series.genre.size() > 0) {
        info << tr("Genre") << ": " << series.genre << "\n\n";
    }
    if (series.network.size() > 0) {
        info << tr("Network") << ": " << series.network << "\n\n";
    }
    if (series.rating > 0) {
        info << tr("TheTVDB Rating") << ": " << series.rating << "\n\n";
    }
    if (series.status.size() > 0) {
        info << tr("Status") << ": " << series.status << "\n\n";
    }
    tvdbInfo = info.str();
}

void cSeriesView::DrawImages(void) {
    int numPosters = series.posters.size();
    int numFanarts = series.fanarts.size();
    int numBanners = series.banners.size();
    
    int totalHeight = border;
    //Fanart Height
    int fanartWidth = width - 2 * border;
    int fanartHeight = 0;
    if (numFanarts > 0 && series.fanarts[0].width > 0) {
        fanartHeight = series.fanarts[0].height * ((double)fanartWidth / (double)series.fanarts[0].width);
        if (fanartHeight > contentHeight - 2 * border) {
            int fanartHeightOrig = fanartHeight;
            fanartHeight = contentHeight - 2 * border;
            fanartWidth = fanartWidth * ((double)fanartHeight / (double)fanartHeightOrig);
        }
        totalHeight += series.fanarts.size() * (fanartHeight + border);
    }
    //Poster Height
    int posterWidth = (width - 4 * border) / 2;
    int posterHeight = 0;
    if (numPosters > 0 && series.posters[0].width > 0) {
        posterHeight = series.posters[0].height * ((double)posterWidth / (double)series.posters[0].width);
    }
    if (numPosters > 0)
        totalHeight += posterHeight + border;
    if (numPosters == 3)
        totalHeight += posterHeight + border;
    //Banners Height
    if (numBanners > 0)
        totalHeight += (series.banners[0].height + border) * numBanners;
       
    CreateContent(totalHeight);
    
    cImageLoader imgLoader;
    int yPic = border;
    for (int i = 0; i < numFanarts; i++) {
        if (numBanners > i) {
            if (imgLoader.LoadPoster(series.banners[i].path.c_str(), series.banners[i].width, series.banners[i].height) && Running()) {
                osdManager->DrawImage(pixmapContent, cPoint((width - series.banners[i].width) / 2, yPic), imgLoader.GetImage());
                yPic += series.banners[i].height + border;
            }
        }
        if (imgLoader.LoadPoster(series.fanarts[i].path.c_str(), fanartWidth, fanartHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - fanartWidth) / 2, yPic), imgLoader.GetImage());
            yPic += fanartHeight + border;
        }
    }
    if (numPosters >= 1) {
        if (imgLoader.LoadPoster(series.posters[0].path.c_str(), posterWidth, posterHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint(border, yPic), imgLoader.GetImage());
            yPic += posterHeight + border;
        }
    }
    if (numPosters >= 2) {
        if (imgLoader.LoadPoster(series.posters[1].path.c_str(), posterWidth, posterHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint(2 * border + posterWidth, yPic - posterHeight - border), imgLoader.GetImage());
        }
    }
    if (numPosters == 3) {
        if (imgLoader.LoadPoster(series.posters[2].path.c_str(), posterWidth, posterHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - posterWidth) / 2, yPic), imgLoader.GetImage());
        }
    }
}

int cSeriesView::GetRandomPoster(void) {
    int numPosters = series.posters.size();
    if (numPosters == 0)
        return -1;
    srand((unsigned)time(NULL));
    int randPoster = rand()%numPosters;
    return randPoster;
}

void cSeriesView::KeyLeft(void) { 
    if (Running())
        return;
    activeView--;
    if (activeView < 0)
        activeView = mvtCount - 1; 
}

void cSeriesView::KeyRight(void) { 
    if (Running())
        return;
    activeView = (activeView + 1) % mvtCount;
}

void cSeriesView::Action(void) {
    ClearContent();
    if (!headerDrawn) {
        DrawHeader();
        headerDrawn = true;
    }
    if (tabs.size() == 0) {
        SetTabs();
    }
    DrawTabs();
    int randomPoster = GetRandomPoster();
    switch (activeView) {
        case mvtInfo:
            if (randomPoster >= 0) {
                DrawContent(&infoText, &series.posters[randomPoster]);
            } else
                DrawContent(&infoText);
            break;
        case mvtAddInfo:
            if (randomPoster >= 0)
                DrawContent(&addInfoText, &series.posters[randomPoster]);
            else
                DrawContent(&addInfoText);
            break;
        case mvtCast:
            DrawActors(&series.actors);
            break;
        case mvtOnlineInfo:
            CreateTVDBInfo();
            if ((series.seasonPoster.path.size() > 0) && series.episode.episodeImage.path.size() > 0)
                DrawFloatingContent(&tvdbInfo, &series.episode.episodeImage, &series.seasonPoster);
            else if (series.seasonPoster.path.size() > 0)
                DrawFloatingContent(&tvdbInfo, &series.seasonPoster);
            else if (series.episode.episodeImage.path.size() > 0)
                DrawFloatingContent(&tvdbInfo, &series.episode.episodeImage);
            else if (randomPoster >= 0)
                DrawFloatingContent(&tvdbInfo, &series.posters[randomPoster]);
            else
                DrawContent(&tvdbInfo);
            break;
        case mvtImages:
            DrawImages();
            break;
    }
    DrawScrollbar();
    osdManager->Flush();
}

/********************************************************************************************
* cMovieView : cView
********************************************************************************************/

cMovieView::cMovieView(int movieId) : cView() {
    this->movieId = movieId;
    tabbed = true;
}

cMovieView::~cMovieView(void) {
    Cancel(2);
}

void cMovieView::LoadMedia(void) {
    static cPlugin *pScraper = GetScraperPlugin();
    if (!pScraper)
        return;

    movie.movieId = movieId;
    pScraper->Service("GetMovie", &movie);
}

void cMovieView::SetTabs(void) {
    tabs.push_back(tr("EPG Info"));
    if (eventID > 0)
        tabs.push_back(tr("Reruns"));
    else
        tabs.push_back(tr("Recording Information"));
    tabs.push_back(tr("Cast"));
    tabs.push_back(tr("TheMovieDB Info"));
    tabs.push_back(tr("Image Galery"));
}

void cMovieView::CreateMovieDBInfo(void) {
    if (movieDBInfo.size() > 0)
        return;
    std::stringstream info;
    info << tr("TheMovieDB Information") << ":\n\n";
    if (movie.originalTitle.size() > 0) {
        info << tr("Original Title") << ": " << movie.originalTitle << "\n\n";
    }
    if (movie.tagline.size() > 0) {
        info << tr("Tagline") << ": " << movie.tagline << "\n\n";
    }
    if (movie.overview.size() > 0) {
        info << tr("Overview") << ": " << movie.overview << "\n\n";
    }
    std::string strAdult = (movie.adult) ? (trVDR("yes")) : (trVDR("no"));
    info << tr("Adult") << ": " << strAdult << "\n\n";
    if (movie.collectionName.size() > 0) {
        info << tr("Collection") << ": " << movie.collectionName << "\n\n";
    }
    if (movie.budget > 0) {
        info << tr("Budget") << ": " << movie.budget << "$\n\n";
    }
    if (movie.revenue > 0) {
        info << tr("Revenue") << ": " << movie.revenue << "$\n\n";
    }
    if (movie.genres.size() > 0) {
        info << tr("Genre") << ": " << movie.genres << "\n\n";
    }
    if (movie.homepage.size() > 0) {
        info << tr("Homepage") << ": " << movie.homepage << "\n\n";
    }
    if (movie.releaseDate.size() > 0) {
        info << tr("Release Date") << ": " << movie.releaseDate << "\n\n";
    }
    if (movie.runtime > 0) {
        info << tr("Runtime") << ": " << movie.runtime << " " << tr("minutes") << "\n\n";
    }
    if (movie.popularity > 0) {
        info << tr("TheMovieDB Popularity") << ": " << movie.popularity << "\n\n";
    }
    if (movie.voteAverage > 0) {
        info << tr("TheMovieDB Vote Average") << ": " << movie.voteAverage << "\n\n";
    }
    movieDBInfo = info.str();
}

void cMovieView::DrawImages(void) {
    int totalHeight = border;
    //Fanart Height
    int fanartWidth = width - 2 * border;
    int fanartHeight = 0;
    if (movie.fanart.width > 0 && movie.fanart.height > 0 && movie.fanart.path.size() > 0) {
        fanartHeight = movie.fanart.height * ((double)fanartWidth / (double)movie.fanart.width);
        if (fanartHeight > contentHeight - 2 * border) {
            int fanartHeightOrig = fanartHeight;
            fanartHeight = contentHeight - 2 * border;
            fanartWidth = fanartWidth * ((double)fanartHeight / (double)fanartHeightOrig);
        }
        totalHeight += fanartHeight + border;
    }
    //Collection Fanart Height
    int collectionFanartWidth = width - 2 * border;
    int collectionFanartHeight = 0;
    if (movie.collectionFanart.width > 0 && movie.collectionFanart.height > 0 && movie.collectionFanart.path.size() > 0) {
        collectionFanartHeight = movie.collectionFanart.height * ((double)collectionFanartWidth / (double)movie.collectionFanart.width);
        if (collectionFanartHeight > contentHeight - 2 * border) {
            int fanartHeightOrig = collectionFanartHeight;
            collectionFanartHeight = contentHeight - 2 * border;
            collectionFanartWidth = collectionFanartWidth * ((double)collectionFanartHeight / (double)fanartHeightOrig);
        }
        totalHeight += collectionFanartHeight + border;
    }
    //Poster Height
    if (movie.poster.width > 0 && movie.poster.height > 0 && movie.poster.path.size() > 0) {
        totalHeight += movie.poster.height + border;
    }
    //Collection Popster Height
    if (movie.collectionPoster.width > 0 && movie.collectionPoster.height > 0 && movie.collectionPoster.path.size() > 0) {
        totalHeight += movie.collectionPoster.height + border;
    }

    CreateContent(totalHeight);
    
    cImageLoader imgLoader;
    int yPic = border;
    if (movie.fanart.width > 0 && movie.fanart.height > 0 && movie.fanart.path.size() > 0) {
        if (imgLoader.LoadPoster(movie.fanart.path.c_str(), fanartWidth, fanartHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - fanartWidth) / 2, yPic), imgLoader.GetImage());
            yPic += fanartHeight + border;
        }
    }
    if (movie.collectionFanart.width > 0 && movie.collectionFanart.height > 0 && movie.collectionFanart.path.size() > 0) {
        if (imgLoader.LoadPoster(movie.collectionFanart.path.c_str(), collectionFanartWidth, collectionFanartHeight) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - collectionFanartWidth) / 2, yPic), imgLoader.GetImage());
            yPic += collectionFanartHeight + border;
        }
    }
    if (movie.poster.width > 0 && movie.poster.height > 0 && movie.poster.path.size() > 0) {
        if (imgLoader.LoadPoster(movie.poster.path.c_str(), movie.poster.width, movie.poster.height) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - movie.poster.width) / 2, yPic), imgLoader.GetImage());
            yPic += movie.poster.height + border;
        }
    }
    if (movie.collectionPoster.width > 0 && movie.collectionPoster.height > 0 && movie.collectionPoster.path.size() > 0) {
        if (imgLoader.LoadPoster(movie.collectionPoster.path.c_str(), movie.collectionPoster.width, movie.collectionPoster.height) && Running()) {
            osdManager->DrawImage(pixmapContent, cPoint((width - movie.collectionPoster.width) / 2, yPic), imgLoader.GetImage());
            yPic += movie.collectionPoster.height + border;
        }
    }
}

void cMovieView::KeyLeft(void) { 
    if (Running())
        return;
    activeView--;
    if (activeView < 0)
        activeView = mvtCount - 1; 
}

void cMovieView::KeyRight(void) { 
    if (Running())
        return;
    activeView = (activeView + 1) % mvtCount;
}

void cMovieView::Action(void) {
    ClearContent();
    if (!headerDrawn) {
        DrawHeader();
        headerDrawn = true;
    }
    if (tabs.size() == 0) {
        SetTabs();
    }
    DrawTabs();
    bool posterAvailable = (movie.poster.path.size() > 0 && movie.poster.width > 0 && movie.poster.height > 0) ? true : false;
    switch (activeView) {
        case mvtInfo:
            if (posterAvailable)
                DrawContent(&infoText, &movie.poster);
            else
                DrawContent(&infoText);
            break;
        case mvtAddInfo:
            if (posterAvailable)
                DrawContent(&addInfoText, &movie.poster);
            else
                DrawContent(&addInfoText);
            break;
        case mvtCast:
            DrawActors(&movie.actors);
            break;
        case mvtOnlineInfo:
            CreateMovieDBInfo();
            if (posterAvailable)
                DrawFloatingContent(&movieDBInfo, &movie.poster);
            else
                DrawContent(&movieDBInfo);
            break;
        case mvtImages:
            DrawImages();
            break;
    }
    DrawScrollbar();
    osdManager->Flush();
}
