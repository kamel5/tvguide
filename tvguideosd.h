#ifndef __TVGUIDE_TVGUIDEOSD_H
#define __TVGUIDE_TVGUIDEOSD_H

#include "timemanager.h"
#include "gridelement.h"
#include "channelepg.h"
#include "statusheader.h"
#include "detailview.h"
#include "timeline.h"
#include "channelgroups.h"
#include "footer.h"
#include "recmenuview.h"
#include "channeljump.h"

// --- cTvGuideOsd -------------------------------------------------------------

class cTvGuideOsd : public cOsdObject {
private:
  cList<cChannelEpg> channels;
  cGridElement *activeGrid = NULL;
  cStatusHeader *statusHeader = NULL;
  cDetailView *detailView = NULL;
  cTimeLine *timeLine = NULL;
  cChannelGroups *channelGroups = NULL;
  cFooter *footer = NULL;
  cRecMenuView *recMenuView = NULL;
  cChannelJump *channelJumper = NULL;
  int GetLastValidChannel(void);
  void DrawOsd(void);
  void CreateChannels(const cChannel *channelStart, int activeChannel = 0, bool init = false);
  void DrawGridsChannelJump(int offset = 0);
  void DrawGridsTimeJump(bool last = false);
  void KeyRed(void);
  void Key1(int Key);
  eOSState KeyBlue(const cEvent *e = NULL);
  eOSState KeyInfo(const cEvent *e = NULL);
  eOSState KeyOk(const cEvent *e = NULL);
  void NumericKey(eKeys key);
  void TimeJump(eKeys key);
  void ChannelJump(int num);
  void CheckTimeout(void);
  void SetActiveGrid(cGridElement *newActive);
  void ChannelStep(int channelDirection);
  void TimeStep(int timeDirection = eBack);
  void Scroll(int scrollMode = eBack);
  const cChannel *GetChannelNumJump(int &activeChannel, int seekMode = eBack);
  const cChannel *GetChannelGroupJump(int seekMode = eBack);
  eOSState ChannelSwitch(bool *alreadyUnlocked);
  eOSState ChannelSwitch(const cEvent *e = NULL);
  void DetailView(const cEvent *e = NULL, const cRecording *r = NULL);
  void CloseDetailedView(void);
  void DetailedEPG(void);
  void SetTimers(void);
  void Dump(void);
public:
  cTvGuideOsd(void);
  virtual ~cTvGuideOsd(void);
  virtual void Show(void);
  virtual eOSState ProcessKey(eKeys Key);
};

#endif //__TVGUIDE_TVGUIDEOSD_H
