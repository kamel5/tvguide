#include "channelepg.h"
#include "dummyelement.h"

cChannelEpg::cChannelEpg(int num, const cChannel *channel) {
    this->channel = channel;
    this->num = num;
    header = NULL;
}

cChannelEpg::~cChannelEpg(void) {
    if (header)
        delete header;
    grids.Clear();
}

void cChannelEpg::ClearGrids(void) {
    grids.Clear();
}

void cChannelEpg::CreateHeader(void) {
    header = new cHeaderGrid();
    header->CreateBackground(num);
    header->DrawChannel(channel);
}

void cChannelEpg::DrawHeader(void) {
    header->SetPosition(num);
}

bool cChannelEpg::ReadGrids(void) {
    const cSchedule *Schedule = NULL;
    {
    LOCK_SCHEDULES_READ;
    Schedule = Schedules->GetSchedule(channel);
    }
    if (!Schedule) {
        AddDummyGrid(timeManager->GetStart(), timeManager->GetEnd(), NULL, false);
        return true;
    }
    bool eventFound = false;
    bool dummyAtStart = false;
    const cEvent *startEvent = Schedule->GetEventAround(timeManager->GetStart());
    if (startEvent != NULL) {
        eventFound = true;
    } else {
        for (time_t i = timeManager->GetStart(); i < (timeManager->GetEnd() - 5 * 60); i = i + 5 * 60) { // 5 * 60 sec
            startEvent = Schedule->GetEventAround(i);
            if (startEvent) {
                eventFound = true;
                dummyAtStart = true;
                break;
            }
        }
    }
    if (eventFound) {
        bool col = true;
        if (dummyAtStart) {
            AddDummyGrid(timeManager->GetStart(), startEvent->StartTime(), NULL, col);
            col = !col;
        }
        bool dummyNeeded = true;
        bool toFarInFuture = false;
        time_t endLast = timeManager->GetStart();
        const cEvent *event = startEvent;
        const cEvent *eventLast = NULL;
        for (; event; event = Schedule->Events()->Next(event)) {
            if (endLast < event->StartTime()) {
                if (dummyAtStart) {
                    dummyAtStart = false;
                } else {
                    //gap, dummy needed
                    time_t endTime = event->StartTime();
                    if (endTime > timeManager->GetEnd()) {
                        endTime = timeManager->GetEnd();
                        toFarInFuture = true;
                    }
                    AddDummyGrid(endLast, endTime, NULL, col);
                    col = !col;
                }
            }
            if (toFarInFuture) {
                break;
            }
            if (event->StartTime() >= timeManager->GetEnd()) {
                dummyNeeded = false;
                break;
            }
            AddEpgGrid(event, NULL, col);
            col = !col;
            endLast = event->EndTime();
            if (event->EndTime() > timeManager->GetEnd()) {
                dummyNeeded = false;
                break;
            }
            eventLast = event;
        }
        if (dummyNeeded) {
            AddDummyGrid(eventLast->EndTime(), timeManager->GetEnd(), NULL, col);
        }
        return true;
    } else {
        AddDummyGrid(timeManager->GetStart(), timeManager->GetEnd(), NULL, false);
        return true;
    }
    return false;
}

void cChannelEpg::DrawGrids(void) {
    for (cGridElement *grid = grids.First(); grid; grid = grids.Next(grid)) {
        grid->SetViewportHeight();
        grid->PositionPixmap();
        grid->Draw();
    }
}

int cChannelEpg::GetX(void) {
    return geoManager.timeLineWidth + num * geoManager.colWidth;
}

int cChannelEpg::GetY(void) {
    return geoManager.statusHeaderHeight + geoManager.timeLineHeight + num * geoManager.rowHeight;
}

cGridElement *cChannelEpg::GetActive(bool last) {
    if (last)
        return grids.Last();
    for (cGridElement *grid = grids.First(); grid; grid = grids.Next(grid)) {
        if (grid->Match(timeManager->GetNow()))
            return grid;
    }
    return grids.First();
}

cGridElement *cChannelEpg::GetNext(cGridElement *activeGrid) {
    if (activeGrid == NULL)
        return NULL;
    cGridElement *next = grids.Next(activeGrid);
    if (next)
        return next;
    return NULL;
}

cGridElement *cChannelEpg::GetPrev(cGridElement *activeGrid) {
    if (activeGrid == NULL)
        return NULL;
    cGridElement *prev = grids.Prev(activeGrid);
    if (prev)
        return prev;
    return NULL;
}

cGridElement *cChannelEpg::GetNeighbor(cGridElement *activeGrid) {
    if (!activeGrid)
        return NULL;
    cGridElement *neighbor = NULL;
    int overlap = 0;
    int overlapNew = 0;
    cGridElement *grid = NULL;
    grid = grids.First();
    if (grid) {
        for (; grid; grid = grids.Next(grid)) {
            if ( (grid->StartTime() == activeGrid->StartTime()) ) {
                neighbor = grid;
                break;
            }
            overlapNew = activeGrid->CalcOverlap(grid);
            if (overlapNew > overlap) {
                neighbor = grid;
                overlap = overlapNew;
            }
        }
    }
    if (!neighbor)
        neighbor = grids.First();
    return neighbor;
}

bool cChannelEpg::IsFirst(cGridElement *grid) {
    if (grid == grids.First())
        return true;
    return false;
}

void cChannelEpg::AddNewGridsAtStart(void) {
    cGridElement *firstGrid = NULL;
    firstGrid = grids.First();
    if (firstGrid == NULL)
        return;
    //if first event is long enough, nothing to do.
    if (firstGrid->StartTime() <= timeManager->GetStart()) {
        return;
    }
    //if not, i have to add new ones to the list
    const cSchedule *Schedule = NULL;
    {
    LOCK_SCHEDULES_READ;
    Schedule = Schedules->GetSchedule(channel);
    }
    if (!Schedule) {
        if (firstGrid->IsDummy()) {
            firstGrid->SetStartTime(timeManager->GetStart());
            firstGrid->SetEndTime(timeManager->GetEnd());
        }
        return;
    }
    bool col = !(firstGrid->IsColor1());
    bool dummyNeeded = true;
    for (const cEvent *event = Schedule->GetEventAround(firstGrid->StartTime() - 60); event; event = Schedule->Events()->Prev(event)) {
        if (!event)
            break;
        if (event->EndTime() < timeManager->GetStart()) {
            break;
        }
        cGridElement *grid = AddEpgGrid(event, firstGrid, col);
        col = !col;
        firstGrid = grid;
        if (event->StartTime() <= timeManager->GetStart()) {
            dummyNeeded = false;
            break;
        }
    }
    if (dummyNeeded) {
        firstGrid = grids.First();
        if (firstGrid->IsDummy()) {
            firstGrid->SetStartTime(timeManager->GetStart());
            if (firstGrid->EndTime() >= timeManager->GetEnd())
                firstGrid->SetEndTime(timeManager->GetEnd());
        } else {
            AddDummyGrid(timeManager->GetStart(), firstGrid->StartTime(), firstGrid, col);
        }
    }
}

void cChannelEpg::AddNewGridsAtEnd(void) {
    cGridElement *lastGrid = NULL;
    lastGrid = grids.Last();
    if (lastGrid == NULL)
        return;
    //if last event is long enough, nothing to do.
    if (lastGrid->EndTime() >= timeManager->GetEnd()) {
        return;
    }
    //if not, i have to add new ones to the list
    const cSchedule *Schedule = NULL;
    {
    LOCK_SCHEDULES_READ;
    Schedule = Schedules->GetSchedule(channel);
    }
    if (!Schedule) {
        if (lastGrid->IsDummy()) {
            lastGrid->SetStartTime(timeManager->GetStart());
            lastGrid->SetEndTime(timeManager->GetEnd());
        }
        return;
    }
    bool col = !(lastGrid->IsColor1());
    bool dummyNeeded = true;
    for (const cEvent *event = Schedule->GetEventAround(lastGrid->EndTime() + 60); event; event = Schedule->Events()->Next(event)) {
        if (!event)
            break;
        if (event->StartTime() > timeManager->GetEnd()) {
            break;
        }
        AddEpgGrid(event, NULL, col);
        col = !col;
        if (event->EndTime() > timeManager->GetEnd()) {
            dummyNeeded = false;
            break;
        }
    }
    if (dummyNeeded) {
        lastGrid = grids.Last();
        if (lastGrid->IsDummy()) {
            lastGrid->SetEndTime(timeManager->GetEnd());
            if (lastGrid->StartTime() <= timeManager->GetStart())
                lastGrid->SetStartTime(timeManager->GetStart());
        } else {
            AddDummyGrid(lastGrid->EndTime(), timeManager->GetEnd(), NULL, col);
        }
    }
}

void cChannelEpg::ClearOutdatedStart(void) {
    cGridElement *firstGrid = NULL;
    while (true) {
        firstGrid = grids.First();
        if (!firstGrid)
            break;
        if (firstGrid->EndTime() <= timeManager->GetStart()) {
            grids.Del(firstGrid);
            firstGrid = NULL;
        } else {
            if (firstGrid->IsDummy()) {
                firstGrid->SetStartTime(timeManager->GetStart());
                cGridElement *next = GetNext(firstGrid);
                if (next) {
                    firstGrid->SetEndTime(next->StartTime());
                } else {
                    firstGrid->SetEndTime(timeManager->GetEnd());
                }
            }
            break;
        }
    }
}

void cChannelEpg::ClearOutdatedEnd(void) {
    cGridElement *lastGrid = NULL;
    while (true) {
        lastGrid = grids.Last();    
        if (!lastGrid)
            break;
        if (lastGrid->StartTime() >= timeManager->GetEnd()) {
            grids.Del(lastGrid);
            lastGrid = NULL;
        } else {
            if (lastGrid->IsDummy()) {
                lastGrid->SetEndTime(timeManager->GetEnd());
                cGridElement *prev = GetPrev(lastGrid);
                if (prev) {
                    lastGrid->SetStartTime(prev->EndTime());
                } else {
                    lastGrid->SetStartTime(timeManager->GetStart());
                }
            }
            break;
        }
    }
}

cGridElement *cChannelEpg::AddEpgGrid(const cEvent *event, cGridElement *firstGrid, bool color) {
    cGridElement *grid = new cEpgElement(this, event);
    grid->SetText();
    grid->SetColor(color);
    if (!firstGrid)
        grids.Add(grid);
    else
        grids.Ins(grid, firstGrid);
    return grid;
}

cGridElement *cChannelEpg::AddDummyGrid(time_t start, time_t end, cGridElement *firstGrid, bool color) {
    cGridElement *dummy = new cDummyElement(this, start, end);
    dummy->SetText();
    dummy->SetColor(color);
    if (!firstGrid)
        grids.Add(dummy);
    else
        grids.Ins(dummy, firstGrid);
    return dummy;
}

void cChannelEpg::SetTimers(void) {
    SetSwitchTimer();
    for (cGridElement *grid = grids.First(); grid; grid = grids.Next(grid)) {
        bool gridHadTimer = grid->HasTimer();
        bool timerWasActive = grid->TimerIsActive();
        grid->SetTimer();
        if (gridHadTimer != grid->HasTimer() || timerWasActive != grid->TimerIsActive())
            grid->SetDirty();
        bool gridHadSwitchTimer = grid->HasSwitchTimer();
        grid->SetSwitchTimer();
        if (gridHadSwitchTimer != grid->HasSwitchTimer())
            grid->SetDirty();
        grid->Draw();
    }
}

void cChannelEpg::DumpGrids(void) {
    esyslog("tvguide: ------Channel %s %d: %d entires ---------", channel->Name(), num, grids.Count());
    int i = 1;
    for (cGridElement *grid = grids.First(); grid; grid = grids.Next(grid)) {
        esyslog("tvguide: grid %d: start: %s, stop: %s", i, *cTimeManager::PrintTime(grid->StartTime()), *cTimeManager::PrintTime(grid->EndTime()));
        i++;
    }
}
