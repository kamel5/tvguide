#ifndef __TVGUIDE_TIMEMANAGER_H
#define __TVGUIDE_TIMEMANAGER_H

#include <vdr/tools.h>
#include "config.h"

// --- cTimeManager ------------------------------------------------------------- 

class cTimeManager {
private:
    time_t tNow;
    time_t tStart;
    time_t tEnd;
    int displaySeconds;
    eTimeFormat timeFormat;
    void Now(void);
public:
    cTimeManager(void);
    virtual ~cTimeManager(void);
    static cString PrintTime(time_t displayTime);
    bool ScrollMinutes(int timeMode = eTime, int scrollMode = eBack);
    void SetTime(time_t newTime);
    time_t GetNow(void) { return tNow; };
    time_t GetStart(void) { return tStart; };
    time_t GetEnd(void) { return tEnd; };
    cString GetCurrentTime(void);
    cString GetDate(void);
    cString GetWeekday(void);
    time_t GetPrevPrimetime(time_t current);
    time_t GetNextPrimetime(time_t current);
    bool TooFarInPast(time_t current);
    int GetTimelineOffset(void);
    time_t GetRounded(void);
    bool NowVisible(void);
    int GetDisplaySeconds(void) { return displaySeconds; };
    void Debug(void);
};

extern cTimeManager *timeManager;

// --- cTimeInterval ------------------------------------------------------------- 

class cTimeInterval {
private:
    time_t start;
    time_t stop;
public:
    cTimeInterval(time_t start, time_t stop);
    virtual ~cTimeInterval(void);
    time_t Start(void) { return start; };
    time_t Stop(void) { return stop; };
    cTimeInterval *Intersect(cTimeInterval *interval);
    cTimeInterval *Union(cTimeInterval *interval);
};

#endif //__TVGUIDE_TIMEMANAGER_H
