#include "channelepg.h"
#include "gridelement.h"

cGridElement::cGridElement(cChannelEpg *owner) {
    this->owner = owner;
    text = new cTextWrapper();
    dirty = true;
    active = false;
    viewportHeight = 0;
    borderWidth = 10;
}

cGridElement::~cGridElement(void) {
    delete text;
}

void cGridElement::SetBackground(void) {
    if (active) {
        color = theme.Color(clrHighlight);
        colorBlending = theme.Color(clrHighlightBlending);
    } else {
        if (isColor1) {
            color = theme.Color(clrGrid1);
            colorBlending = theme.Color(clrGrid1Blending);
        } else {
            color = theme.Color(clrGrid2);
            colorBlending = theme.Color(clrGrid2Blending);
        }
    }
}

void cGridElement::Draw(void) {
    if (!pixmap) {
        return;
    }
    if (dirty) {
        if (config.style == eStyleGraphical) {
            DrawBackgroundGraphical(bgGrid, active);
            DrawText();
        } else {
            SetBackground();
            DrawBackground();
            DrawText();
            DrawBorder();
        }
        osdManager->SetLayer(pixmap, 1);
        dirty = false;
    }
}

bool cGridElement::IsFirst(void) {
    if (owner->IsFirst(this))
        return true;
    return false;
}

bool cGridElement::Match(time_t t) {
    if ((StartTime() < t) && (EndTime() > t))
        return true;
    else
        return false;
}

int cGridElement::CalcOverlap(cGridElement *neighbor) {
    int overlap = 0;
    if (Intersects(neighbor)) {
        if ((StartTime() <= neighbor->StartTime()) && (EndTime() <= neighbor->EndTime())) {
            overlap = EndTime() - neighbor->StartTime();
        } else if ((StartTime() >= neighbor->StartTime()) && (EndTime() >= neighbor->EndTime())) {
            overlap = neighbor->EndTime() - StartTime();
        } else if ((StartTime() >= neighbor->StartTime()) && (EndTime() <= neighbor->EndTime())) {
            overlap = Duration();
        } else if ((StartTime() <= neighbor->StartTime()) && (EndTime() >= neighbor->EndTime())) {
            overlap = neighbor->EndTime() - neighbor->StartTime();
        }
    }
    return overlap;
}

bool cGridElement::Intersects(cGridElement *neighbor) {
    return ! ( (neighbor->EndTime() <= StartTime()) || (neighbor->StartTime() >= EndTime()) ); 
}
