#ifndef __TVGUIDE_RECMENU_H
#define __TVGUIDE_RECMENU_H

#include "config.h"
#include "footer.h"
#include "recmenuitem.h"

// --- cRecMenu  -------------------------------------------------------------

class cRecMenu : public cStyledPixmap {
private:
    std::list<cRecMenuItem*> menuItems;
    int scrollbarWidth;
    int numItems;
    int startIndex, stopIndex;
    void CreatePixmap(int scrollBarHeight = 0);
    void Activate(cRecMenuItem *itemOld, cRecMenuItem *item);
    bool ActivateNew(eScrollMode scrollMode = eBack);
    bool Scroll(eScrollMode scrollMode = eBack, bool retry = false);
    void Page(eScrollMode pageMode = eBack);
    void Jump(eJumpMode jumpMode = eBeginn);
    void DrawScrollBar(void);
protected:
    int x, y;
    int width, height;
    int headerHeight, footerHeight, buttonsHight;
    int currentHeight;
    bool deleteMenuItems;
    cPixmap *pixmapScrollBar;
    cImage *imgScrollBar;
    int border;
    bool scrollable;
    cRecMenuItem *header;
    cRecMenuItem *footer;
    cFooter *buttons;
    void SetWidthPercent(int percentOSDWidth);
    void SetWidthPixel(int pixel);
    int CalculateOptimalWidth(void);
    void CalculateHeight(bool reDraw = false);
    void AddHeader(cRecMenuItem *header);
    void AddFooter(cRecMenuItem *footer);
    void AddButtons(std::string a, std::string b, std::string c, std::string d);
    void ClearMenuItems(bool destructor = false);
    void InitMenu(bool complete);
    bool AddMenuItemInitial(cRecMenuItem *item, bool inFront = false);
    void AddMenuItem(cRecMenuItem *item, bool inFront = false);
    cImage *CreateScrollbar(int width, int height, tColor clrBgr, tColor clrBlend);
    void Arrange(bool scroll = false);
    virtual cRecMenuItem *GetMenuItem(int number) { return NULL; };
    cRecMenuItem *GetActiveMenuItem(void);
    cRecMenuItem *GetMenuItemAt(int num);
    int GetCurrentNumMenuItems(void) { return menuItems.size(); };
    virtual int GetTotalNumMenuItems(void) { return 0; };
    virtual void CreateMenuItems(void) {};
    int GetStartIndex(void) { return startIndex; };
    int GetNumActive(void);
public:
    cRecMenu(void);
    virtual ~cRecMenu(void);
    void Display(bool scroll = false);
    void Hide(void);
    void Show(void);
    void UpdateActiveMenuItem(void);
    virtual eRecMenuState ProcessKey(eKeys Key);
};
#endif //__TVGUIDE_RECMENU_H
