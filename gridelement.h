#ifndef __TVGUIDE_GRIDELEMENT_H
#define __TVGUIDE_GRIDELEMENT_H

#include <vdr/tools.h>
#include "styledpixmap.h"

class cChannelEpg;

// --- cGridElement  -------------------------------------------------------------

class cGridElement : public cListObject, public cStyledPixmap {
protected:
    cTextWrapper *text;
    int viewportHeight;
    int borderWidth;
    void SetBackground(void);
    bool isColor1;
    bool active;
    bool dirty;
    bool hasTimer;
    bool timerIsActive = false;
    bool hasSwitchTimer;
    bool Intersects(cGridElement *neighbor);
    virtual time_t Duration(void) { return 0; };
    virtual void DrawText(void) {};
    bool dummy;
public:
    cGridElement(cChannelEpg *owner);
    virtual ~cGridElement(void);
    cChannelEpg *owner;
    virtual void SetViewportHeight(void) {};
    virtual void PositionPixmap(void) {};
    virtual void SetText(void) {};
    void Draw(void);
    void SetDirty(void) { dirty = true; };
    void SetActive(void) { dirty = true; active = true; };
    void SetInActive(void) { dirty = true; active = false; };
    void SetColor(bool color) { isColor1 = color; };
    bool IsColor1(void) { return isColor1; };
    bool IsFirst(void);
    virtual const cEvent *GetEvent(void) { return NULL; };
    bool Match(time_t t);
    virtual time_t StartTime(void) { return 0; };
    virtual time_t EndTime(void) { return 0; };
    virtual void SetStartTime(time_t start) {};
    virtual void SetEndTime(time_t end) {};
    int CalcOverlap(cGridElement *neighbor);
    virtual void SetTimer(void) {};
    virtual void SetSwitchTimer(void) {};
    virtual cString GetText(void) { return cString(""); };
    virtual cString GetTimeString(void) { return cString(""); };
    bool Active(void) { return active; };
    bool HasTimer(void) { return hasTimer; };
    bool TimerIsActive(void) { return timerIsActive; };
    bool HasSwitchTimer(void) { return hasSwitchTimer; };
    bool IsDummy(void) { return dummy; };
    const cChannel *Channel(void);
    virtual void Debug(void) {};
};

#endif //__TVGUIDE_GRIDELEMENT_H
