#include <algorithm>
#include <vector>
#include "config.h"
#include "channelgroups.h"
#include "tools.h"

// --- cChannelGroup  -------------------------------------------------------------

cChannelGroup::cChannelGroup(const char *name) {
    channelStart = 0;
    channelStop = 0;
    this->name = name;
}

cChannelGroup::~cChannelGroup(void) {
}

void cChannelGroup::Dump(void) {
    esyslog("tvguide: Group %s, startChannel %d, stopChannel %d", name, channelStart, channelStop);
}

// --- cChannelGroupGrid  -------------------------------------------------------------

cChannelGroupGrid::cChannelGroupGrid(const char *name) {
    this->name = name;
}

cChannelGroupGrid::~cChannelGroupGrid(void) {
}

void cChannelGroupGrid::SetBackground() {
    if (isColor1) {
        color = theme.Color(clrGrid1);
        colorBlending = theme.Color(clrGrid1Blending);
    } else {
        color = theme.Color(clrGrid2);
        colorBlending = theme.Color(clrGrid2Blending);
    }
}

void cChannelGroupGrid::SetGeometry(int start, int end) {
    int x, y, width, height;
    if (config.displayMode == eVertical) {
        x = geoManager.timeLineWidth + start*geoManager.colWidth;
        y = geoManager.statusHeaderHeight;
        width = (end - start + 1) * geoManager.colWidth;
        height = geoManager.channelGroupsHeight;
    } else if (config.displayMode == eHorizontal) {
        x = 0;
        y = geoManager.statusHeaderHeight +  geoManager.timeLineHeight + start*geoManager.rowHeight;
        width = geoManager.channelGroupsWidth;
        height = (end - start + 1) * geoManager.rowHeight;
    }
    pixmap = osdManager->CreatePixmap(__func__, "pixmap", 1, cRect(x, y, width, height));
}

void cChannelGroupGrid::Draw(void) {
    if (config.style == eStyleGraphical) {
        DrawBackgroundGraphical(bgChannelGroup);
    } else {
        DrawBackground();
        DrawBorder();
    }
    tColor colorText = theme.Color(clrFont);
    tColor colorTextBack = (config.style == eStyleFlat)?color:clrTransparent;
    if (config.displayMode == eVertical) {
        DrawVertical(colorText, colorTextBack);
    } else if (config.displayMode == eHorizontal) {
        DrawHorizontal(colorText, colorTextBack);
    }
}

void cChannelGroupGrid::DrawVertical(tColor colorText, tColor colorTextBack) {
    int textY = (Height() - fontManager->FontChannelGroups->Height()) / 2;
    cString text = cString::sprintf("%s", CutText(name, Width() - 4, fontManager->FontChannelGroups).c_str());
    int textWidth = fontManager->FontChannelGroups->Width(*text);
    int x = (Width() - textWidth) / 2;
    osdManager->DrawText(pixmap, cPoint(x, textY), *text, colorText, colorTextBack, fontManager->FontChannelGroups);
}

void cChannelGroupGrid::DrawHorizontal(tColor colorText, tColor colorTextBack) {
    std::string groupName = name;
    int numChars = groupName.length();
    int charHeight = fontManager->FontChannelGroupsHorizontal->Height();
    int textHeight = numChars * charHeight;
    int y = 5;
    if ((textHeight + 5) < Height()) {
        y = (Height() - textHeight) / 2;
    }
    for (int i = 0; i < numChars; i++) {
        if (((y + 2 * charHeight) > Height()) && ((i + 1) < numChars)) {
            int x = (Width() - fontManager->FontChannelGroupsHorizontal->Width("...")) / 2;
            osdManager->DrawText(pixmap, cPoint(x, y), "...", colorText, colorTextBack, fontManager->FontChannelGroupsHorizontal);
            break;
        }
        cString currentChar = cString::sprintf("%s", utf8_substr(groupName.c_str(), i, 1).c_str());
        int x = (Width() - fontManager->FontChannelGroupsHorizontal->Width(*currentChar)) / 2;
        osdManager->DrawText(pixmap, cPoint(x, y), *currentChar, colorText, colorTextBack, fontManager->FontChannelGroupsHorizontal);
        y += fontManager->FontChannelGroupsHorizontal->Height();
    }
}

// --- cChannelGroups  -------------------------------------------------------------

cChannelGroups::cChannelGroups(void) {
    Init();
}

cChannelGroups::~cChannelGroups(void) {
}

void cChannelGroups::Init(void) {
    bool setStart = false;
    int lastChannelNumber = 0;
    {
    LOCK_CHANNELS_READ;
    const cChannel *first = Channels->First();
    if (!first->GroupSep()) {
        channelGroups.push_back(cChannelGroup(tr("Main Program")));
        setStart = true;
    }    
    for (const cChannel *channel = Channels->First(); channel; channel = Channels->Next(channel)) {
        if (setStart && (channelGroups.size() > 0)) {
            channelGroups[channelGroups.size() - 1].SetChannelStart(channel->Number());
            setStart = false;
        }
        if (channel->GroupSep()) {
            if (channelGroups.size() > 0) {
                channelGroups[channelGroups.size() - 1].SetChannelStop(lastChannelNumber);
            }
            channelGroups.push_back(cChannelGroup(channel->Name()));
            setStart = true;
        } else {
            lastChannelNumber = channel->Number();
        }
    }
    } // LOCK
    if (channelGroups.size() > 0) {
        channelGroups[channelGroups.size() - 1].SetChannelStop(lastChannelNumber);
        if ((config.hideLastGroup) && (channelGroups.size() > 1)) {
            channelGroups.pop_back();
        }
    }
}

int cChannelGroups::GetGroup(const cChannel *channel) {
    if (!channel)
        return -1;

    int id = 0;
    int channelNumber = channel->Number();
    for (std::vector<cChannelGroup>::iterator group = channelGroups.begin(); group != channelGroups.end(); group++) {
        if ( (*group).StartChannel() <= channelNumber && (*group).StopChannel() >= channelNumber ) {
            return id;
        }
        id++;
    }
    return -1;
}

const char* cChannelGroups::GetPrevGroupName(int group) {
    if (group <= 0 || group > (int)channelGroups.size())
        return " ";
    return channelGroups[group - 1].GetName();
}

const char* cChannelGroups::GetNextGroupName(int group) {
    if (group < 0 || group >= (int)channelGroups.size() - 1)
        return " ";
    return channelGroups[group + 1].GetName();
}

int cChannelGroups::GetPrevGroupChannelNumber(const cChannel *channel) {
    int currentGroup = GetGroup(channel);
    if (currentGroup == -1)
        return 0;
    if (currentGroup > 0) {
        return channelGroups[currentGroup - 1].StartChannel();
    }
    return 0;
}

int cChannelGroups::GetNextGroupChannelNumber(const cChannel *channel) {
    int currentGroup = GetGroup(channel);
    if (currentGroup == -1)
        return 0;
    if ((currentGroup + 1) < (int)channelGroups.size()) {
        return channelGroups[currentGroup + 1].StartChannel();
    }
    return 0;
}

int cChannelGroups::GetPrevGroupFirstChannel(int group) {
    if (group <= 0 || group > (int)channelGroups.size())
        return -1;
    return channelGroups[group - 1].StartChannel();
}

int cChannelGroups::GetNextGroupFirstChannel(int group) {
    if (group < 0 || group >= (int)channelGroups.size())
        return -1;
    return channelGroups[group + 1].StartChannel();
}

bool cChannelGroups::IsInFirstGroup(const cChannel *channel) {
    if (channelGroups.size() == 0)
        return false;
    int channelNumber = channel->Number();
    if (channelNumber >= channelGroups[0].StartChannel() && channelNumber <= channelGroups[0].StopChannel())
        return true;
    return false;
}

bool cChannelGroups::IsInLastGroup(const cChannel *channel) {
    size_t numGroups = channelGroups.size();
    if (!config.hideLastGroup || numGroups == 0)
        return false;
    int channelNumber = channel->Number();
    if (channelNumber > channelGroups[channelGroups.size() - 1].StopChannel())
        return true;
    return false;
}

bool cChannelGroups::IsInSecondLastGroup(const cChannel *channel) {
    size_t numGroups = channelGroups.size();
    if (numGroups < 2)
        return false;
    int channelNumber = channel->Number();
    if (channelNumber >= channelGroups[numGroups - 1].StartChannel() && channelNumber <= channelGroups[numGroups - 1].StopChannel())
        return true;
    return false;
}

void cChannelGroups::Draw(const cChannel *start, const cChannel *stop) {
    if (!start || !stop)
        return;

    groupGrids.Clear();
    int group = GetGroup(start);
    int groupLast = group;
    int lineStart = 0;
    int lineEnd = 0;
    const cChannels *channels = NULL;
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
    for (const cChannel *channel = channels->Next(start); channel; channel = channels->Next(channel)) {
        if (channel->GroupSep())
            continue;
        group = GetGroup(channel);
        if (group != groupLast) {
            CreateGroupGrid(channelGroups[groupLast].GetName(), group, lineStart, lineEnd);
            lineStart = lineEnd + 1;
        }
        lineEnd++;
        groupLast = group;
        if (channel == stop) {
            CreateGroupGrid(channelGroups[groupLast].GetName(), group + 1, lineStart, lineEnd);
            break;
        }
    }
    
}

void cChannelGroups::CreateGroupGrid(const char *name, int number, int start, int end) {
    cChannelGroupGrid *groupGrid = new cChannelGroupGrid(name);
    groupGrid->SetColor(number % 2);
    groupGrid->SetBackground();
    groupGrid->SetGeometry(start, end);
    groupGrid->Draw();
    groupGrids.Add(groupGrid);
}

int cChannelGroups::GetLastValidChannel(void) {
    if (config.hideLastGroup && channelGroups.size() > 1) {
        return channelGroups[channelGroups.size() - 1].StopChannel();
    }
   LOCK_CHANNELS_READ;
   return Channels->MaxNumber();
}

void cChannelGroups::DumpGroups(void) {
    for (std::vector<cChannelGroup>::iterator group = channelGroups.begin(); group!=channelGroups.end(); ++group) {
        group->Dump();
    }
}
