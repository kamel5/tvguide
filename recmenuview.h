#ifndef __TVGUIDE_RECMENUVIEW_H
#define __TVGUIDE_RECMENUVIEW_H

#include "recmenu.h"
#include "recmanager.h"
#include "services/epgsearch.h"

// --- cRecMenuView  -------------------------------------------------------------
class cRecMenuView {
private:
    bool active = false;
    cRecMenu *activeMenu = NULL;
    cRecMenu *activeMenuBuffer = NULL;
    cRecMenu *activeMenuBuffer2 = NULL;
    const cEvent *event = NULL;
    const cEvent *displayEvent = NULL;
    const cRecording *displayRecording = NULL;
    cRecManager *recManager = NULL;
    cTVGuideTimerConflicts *timerConflicts = NULL;
    cDetailView *detailView = NULL;
    cPixmap *pixmapBackground = NULL;
    void SetBackground(void);
    void DeleteBackground(void);
    void DisplaySearchTimerList(void);
    bool DisplayTimerConflict(const cTimer *timer);
    bool DisplayTimerConflict(int timerID);
    void DisplayDetailedView(const cEvent *ev = NULL, const cRecording *rec = NULL);
    void DisplayFavoriteResults(std::string header, const cEvent **result, int numResults);
    eOSState StateMachine(eRecMenuState nextState);
public:
    cRecMenuView(void);
    virtual ~cRecMenuView(void);
    bool IsActive(void) { return active; };
    void Start(const cEvent *event);
    void DisplayFavorites(void);
    void Close(void);
    void Hide(bool full = false);
    void Activate(bool full = false);
    const cEvent *GetEvent(void) { return displayEvent; };
    const cRecording *GetRecording(void) { return displayRecording; };
    eOSState ProcessKey(eKeys Key);
};

#endif //__TVGUIDE_RECMENUVIEW_H
