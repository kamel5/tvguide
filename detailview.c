#include "detailview.h"

cDetailView::cDetailView(const cEvent *event, const cRecording *recording, int fromView) : cThread("DetailView") {
    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    this->event = event;
    this->recording = recording;
    Button_Parameter_t bp;
    bp.top = geoManager.footerY;
    bp.width = geoManager.osdWidth;
    bp.height = geoManager.footerHeight;
    bp.fromView = fromDetailView | fromView;
    bp.isRecording = (this->recording) ? true : false;
    footer = new cFooter(bp);
}

cDetailView::~cDetailView(void){
    if (footer)
        delete footer;
    Cancel(3);
    while (Active())
        cCondWait::SleepMs(10);
    if (view)
        delete view;
    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
}

void cDetailView::InitiateView(void) {
    static cPlugin *pScraper = GetScraperPlugin();
    ScraperGetEventType call;
    if (event) {
        call.event = event;
        if (!pScraper) {
            view = new cEPGView();
        } else if (pScraper->Service("GetEventType", &call)) {
            if (call.type == tMovie) {
                view = new cMovieView(call.movieId);
            } else if (call.type == tSeries) {
                view = new cSeriesView(call.seriesId, call.episodeId);
            } else {
                view = new cEPGView();
            }
        } else {
            view = new cEPGView();                
        }
        view->SetTitle(event->Title());
        view->SetSubTitle(event->ShortText());
        view->SetInfoText(event->Description());
        cString dateTime;
        time_t vps = event->Vps();
        if (vps) {
            dateTime = cString::sprintf("%s  %s - %s (%d %s) VPS: %s", *event->GetDateString(), *event->GetTimeString(), *event->GetEndTimeString(), event->Duration()/60, tr("min"), *TimeString(vps));
        } else {
            dateTime = cString::sprintf("%s  %s - %s (%d %s)", *event->GetDateString(), *event->GetTimeString(), *event->GetEndTimeString(), event->Duration()/60, tr("min"));
        }
        view->SetDateTime(*dateTime);
        LOCK_CHANNELS_READ;
        view->SetChannel(Channels->GetByChannelID(event->ChannelID(), true));
        view->SetEventID(event->EventID());
        view->SetEvent(event);
    } else if (recording) {
        call.recording = recording;
        if (!pScraper) {
            view = new cEPGView();
        } else if (pScraper->Service("GetEventType", &call)) {
            if (call.type == tMovie) {
                view = new cMovieView(call.movieId);
            } else if (call.type == tSeries) {
                view = new cSeriesView(call.seriesId, call.episodeId);
            } else {
                view = new cEPGView();
            }
        } else {
            view = new cEPGView();
        }
        const cRecordingInfo *info = recording->Info();
        if (info) {
            view->SetTitle(info->Title());
            std::string shortText(info->ShortText() ? info->ShortText() : "");
            if (info->GetEvent()->ParentalRating()) {
                if (shortText.empty()) {
                    shortText += tr("Parental rating: ");
                } else {
                    shortText += " - ";
                }
                shortText += info->GetEvent()->GetParentalRatingString();
            }
            view->SetSubTitle(shortText.c_str());
            view->SetInfoText(info->Description());
            LOCK_CHANNELS_READ;
            view->SetChannel(Channels->GetByChannelID(info->ChannelID(), true));
        } else {
            view->SetTitle(recording->Name());
        }
        int recDuration = recording->LengthInSeconds();
        recDuration = (recDuration > 0) ? (recDuration / 60) : 0;
        cString dateTime;
        dateTime = cString::sprintf("%s  %s (%d %s)", *DateString(recording->Start()), *TimeString(recording->Start()), recDuration, tr("min"));
        view->SetDateTime(*dateTime);
    }
}

cList<Epgsearch_searchresults_v1_0::cServiceSearchResult> *cDetailView::LoadReruns(void) {
//    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    cPlugin *epgSearchPlugin = cPluginManager::GetPlugin("epgsearch");
    if (!epgSearchPlugin)
        return NULL;

    if (!event || isempty(event->Title()))
        return NULL;

    if (!config.displayRerunsDetailEPGView)
        return NULL;

    Epgsearch_searchresults_v1_0 data;
    std::string strQuery = event->Title();

    if (config.useSubtitleRerun && !isempty(event->ShortText())) {
        strQuery += "~";
        strQuery += event->ShortText();
    }

    data.query = (char *)strQuery.c_str();
    data.mode = 0;
    data.channelNr = 0;
    data.useTitle = true;
    data.useSubTitle = true;
    data.useDescription = false;

    int maxNumReruns = config.numReruns;
    int rerunDistance = config.rerunDistance * 3600;
    int rerunMaxChannel = config.rerunMaxChannel;
    int i = 0;

    cList<Epgsearch_searchresults_v1_0::cServiceSearchResult> *reruns = NULL;
    if (epgSearchPlugin->Service("Epgsearch-searchresults-v1.0", &data)) {
        cList<Epgsearch_searchresults_v1_0::cServiceSearchResult> *result = data.pResultList;
        if (result) {
            for (Epgsearch_searchresults_v1_0::cServiceSearchResult *r = result->First(); r && i < maxNumReruns; r = result->Next(r)) {
                time_t eventStart = event->StartTime();
                time_t rerunStart = r->event->StartTime();
                LOCK_CHANNELS_READ;
                const cChannel *channel = Channels->GetByChannelID(r->event->ChannelID(), true, true);
                //check for identical event
                if ((event->ChannelID() == r->event->ChannelID()) && (eventStart == rerunStart))
                    continue;
                //check for timely distance
                if (rerunDistance > 0)
                    if (rerunStart - eventStart < rerunDistance)
                        continue;
                //check for maxchannel
                if (rerunMaxChannel > 0)
                    if (channel && channel->Number() > rerunMaxChannel)
                        continue;
                if (!reruns) reruns = new cList<Epgsearch_searchresults_v1_0::cServiceSearchResult>;
                reruns->Add(r);
                i++;
            }
        }
    }
    return reruns;
}

std::string cDetailView::RerunstoText(void) {
//    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);

    cList<Epgsearch_searchresults_v1_0::cServiceSearchResult> *reruns =  LoadReruns();

    std::stringstream sstrReruns;
    if (!reruns || reruns->Count() < 1) {
        sstrReruns << std::endl << tr("No reruns found");
        return sstrReruns.str();
    }

    sstrReruns << reruns->Count() << " " << tr("Reruns of ") << "\"" << event->Title() << "\":" << std::endl << std::endl;

    int i = 0;
    for (Epgsearch_searchresults_v1_0::cServiceSearchResult *r = reruns->First(); r; r = reruns->Next(r)) {
        LOCK_CHANNELS_READ;
        const cChannel *channel = Channels->GetByChannelID(r->event->ChannelID(), true, true);
        sstrReruns  << *DayDateTime(r->event->StartTime());
        if (channel) {
            sstrReruns << ", " << trVDR("Channel") << " " << channel->Number() << ":";
            sstrReruns << " " << channel->ShortName(true);
        }
        sstrReruns << "\n" << r->event->Title();
        if (!isempty(r->event->ShortText()))
            sstrReruns << "~" << r->event->ShortText();
        sstrReruns << std::endl << std::endl;
        if (r == reruns->Last())
            break;
        i++;
    }

    return sstrReruns.str();
}

std::string cDetailView::LoadRecordingInformation(void) {
    const cRecordingInfo *Info = recording->Info();
    uint16_t maxFiles = (recording->IsPesRecording()) ? 999 : 65535; // 65535 should be enough
    unsigned long long nRecSize = -1;
    unsigned long long nFileSize[1000];
    nFileSize[0] = 0;
    int i = 0;
    struct stat filebuf;
    cString filename;
    int rc = 0;
    do {
        i++;
        if (recording->IsPesRecording())
            filename = cString::sprintf("%s/%03d.vdr", recording->FileName(), i);
        else
            filename = cString::sprintf("%s/%05d.ts", recording->FileName(), i);
        rc = stat(filename, &filebuf);
        if (rc == 0)
            nFileSize[i] = nFileSize[i-1] + filebuf.st_size;
        else
            if (ENOENT != errno) {
                nRecSize = -1;
            }
    } while (i <= maxFiles && !rc);
    nRecSize = nFileSize[i-1];

    cMarks marks;
    bool fHasMarks = marks.Load(recording->FileName(), recording->FramesPerSecond(), recording->IsPesRecording()) && marks.Count();
    cIndexFile *index = new cIndexFile(recording->FileName(), false, recording->IsPesRecording());

    int nCutLength = 0;
    long nCutInFrame = 0;
    unsigned long long nRecSizeCut = nRecSize < 0 ? -1 : 0;
    unsigned long long nCutInOffset = 0;

    if (fHasMarks && index) {
        uint16_t FileNumber;
        off_t FileOffset;

        bool fCutIn = true;
        cMark *mark = marks.First();
        while (mark) {
            int pos = mark->Position();
            index->Get(pos, &FileNumber, &FileOffset); //TODO: will disc spin up?
            if (fCutIn) {
                nCutInFrame = pos;
                fCutIn = false;
                if (nRecSize >= 0)
                    nCutInOffset = nFileSize[FileNumber-1] + FileOffset;
            } else {
                nCutLength += pos - nCutInFrame;
                fCutIn = true;
                if (nRecSize >= 0)
                    nRecSizeCut += nFileSize[FileNumber-1] + FileOffset - nCutInOffset;
            }
            cMark *nextmark = marks.Next(mark);
            mark = nextmark;
        }
        if (!fCutIn) {
            nCutLength += index->Last() - nCutInFrame;
            index->Get(index->Last() - 1, &FileNumber, &FileOffset);
            if (nRecSize >= 0)
                nRecSizeCut += nFileSize[FileNumber-1] + FileOffset - nCutInOffset;
        }
    }

    std::stringstream sstrInfo;

    LOCK_CHANNELS_READ;
    const cChannel *channel = Channels->GetByChannelID(Info->ChannelID());
    if (channel)
        sstrInfo << trVDR("Channel") << ": " << channel->Number() << " - " << channel->Name() << std::endl;
    if (nRecSize < 0) {
        if ((nRecSize = ReadSizeVdr(recording->FileName())) < 0) {
            nRecSize = DirSizeMB(recording->FileName());
        }
    }
    if (nRecSize >= 0) {
        cString strRecSize = "";
        if (fHasMarks) {
            if (nRecSize > MEGABYTE(1023))
                strRecSize = cString::sprintf("%s: %.2f GB (%s: %.2f GB)", tr("Size"), (float)nRecSize / MEGABYTE(1024), tr("cut"), (float)nRecSizeCut / MEGABYTE(1024));
            else
                strRecSize = cString::sprintf("%s: %lld MB (%s: %lld MB)", tr("Size"), nRecSize / MEGABYTE(1), tr("cut"), nRecSizeCut / MEGABYTE(1));
        } else {
            if (nRecSize > MEGABYTE(1023))
                strRecSize = cString::sprintf("%s: %.2f GB", tr("Size"), (float)nRecSize / MEGABYTE(1024));
            else
                strRecSize = cString::sprintf("%s: %lld MB", tr("Size"), nRecSize / MEGABYTE(1));
        }
        sstrInfo << (const char*)strRecSize << std::endl;
    }

    if (index) {
        int nLastIndex = index->Last();
        if (nLastIndex) {
            cString strLength;
            if (fHasMarks) {
                strLength = cString::sprintf("%s: %s (%s: %s)", tr("Length"), *IndexToHMSF(nLastIndex, false, recording->FramesPerSecond()), tr("cut"), *IndexToHMSF(nCutLength, false, recording->FramesPerSecond()));
            } else {
                strLength = cString::sprintf("%s: %s", tr("Length"), *IndexToHMSF(nLastIndex, false, recording->FramesPerSecond()));
            }
            sstrInfo << (const char*)strLength << std::endl;
            cString strBitrate = cString::sprintf("%s: %s\n%s: %.2f MBit/s (Video+Audio)", tr("Format"), recording->IsPesRecording() ? "PES" : "TS", tr("Est. bitrate"), (float)nRecSize / nLastIndex * recording->FramesPerSecond() * 8 / MEGABYTE(1));
            sstrInfo << (const char*)strBitrate << std::endl;
        }
    }
    delete index;

    if (Info) {
#if (APIVERSNUM >= 20605)
        if (*Info->FrameParams()) {
            sstrInfo << tr("Frame Parameters") << ": " << *Info->FrameParams() << std::endl;
        }
#endif
#if (APIVERSNUM >= 20505)
        if (Info->Errors() >= 0) {
            cString errors = cString::sprintf("%s: %i ", tr("TS Errors"), Info->Errors());
            sstrInfo << (const char*)errors << std::endl;
        }
#endif
        if (Info->GetEvent()->ParentalRating()) {
            sstrInfo << tr("Parental rating: ") << (const char*)Info->GetEvent()->GetParentalRatingString() << std::endl;
        }
        const char *aux = NULL;
        aux = Info->Aux();
        if (aux) {
            std::string strAux = aux;
            std::string auxEpgsearch = StripXmlTag(strAux, "epgsearch");
            if (!auxEpgsearch.empty()) {
                std::string searchTimer = StripXmlTag(auxEpgsearch, "searchtimer");
                if (!searchTimer.empty()) {
                    sstrInfo << tr("Search timer") << ": " << searchTimer << std::endl;
                }
            }
            std::string str_tvscraper = StripXmlTag(strAux, "tvscraper");
            if (!str_tvscraper.empty()) {
                std::string causedby = StripXmlTag(str_tvscraper, "causedBy");
                std::string reason = StripXmlTag(str_tvscraper, "reason");
                if (!causedby.empty() && !reason.empty()) {  // TVScraper
                    sstrInfo << "TVScraper: " << tr("caused by") << ": " << causedby << ", "
                             << tr("reason") << ": " << reason << std::endl;
                }
            }
            std::string str_vdradmin = StripXmlTag(strAux, "vdradmin-am");
            if (!str_vdradmin.empty()) {
                std::string pattern = StripXmlTag(str_vdradmin, "pattern");
                if (!pattern.empty()) {
                    sstrInfo << "VDRadmin-AM: " << tr("search pattern") << ": " << pattern << std::endl;
                }
            }
        }
    }

    return sstrInfo.str();
}

std::string cDetailView::StripXmlTag(std::string &Line, const char *Tag) {
        // set the search strings
        std::stringstream strStart, strStop;
        strStart << "<" << Tag << ">";
        strStop << "</" << Tag << ">";
        // find the strings
        std::string::size_type locStart = Line.find(strStart.str());
        std::string::size_type locStop = Line.find(strStop.str());
        if (locStart == std::string::npos || locStop == std::string::npos)
                return "";
        // extract relevant text
        int pos = locStart + strStart.str().size();
        int len = locStop - pos;
        return len < 0 ? "" : Line.substr(pos, len);
}

int cDetailView::ReadSizeVdr(const char *strPath) {
    int dirSize = -1;
    char buffer[20];
    char *strFilename = NULL;
    if (-1 != asprintf(&strFilename, "%s/size.vdr", strPath)) {
        struct stat st;
        if (stat(strFilename, &st) == 0) {
                int fd = open(strFilename, O_RDONLY);
            if (fd >= 0) {
                if (safe_read(fd, &buffer, sizeof(buffer)) >= 0) {
                    dirSize = atoi(buffer);
                }
                close(fd);
            }
        }
        free(strFilename);
    }
    return dirSize;
}

void cDetailView::Action(void) {
    InitiateView();
    if (!view)
        return;
    view->SetFonts();
    view->SetGeometry();
    view->LoadMedia();
    view->Start();
    if (event)
        view->SetAdditionalInfoText(RerunstoText());
    else if (recording)
        view->SetAdditionalInfoText(LoadRecordingInformation());
}

eOSState cDetailView::ProcessKey(eKeys Key) {
//    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    eOSState state = osContinue;
    if (Running())
        return state;
    switch (Key & ~k_Repeat) {
        case kUp: {
            bool scrolled = view->KeyUp();
            if (scrolled) {
                view->DrawScrollbar();
                osdManager->Flush();
            }
            break; }
        case kDown: {
            bool scrolled = view->KeyDown();
            if (scrolled) {
                view->DrawScrollbar();
                osdManager->Flush();
            }
            break; }
        case kLeft:
            view->KeyLeft();
            view->Start();
            break;
        case kRight:
            view->KeyRight();
            view->Start();
            break;
        case kOk:
        case kBack:
            state = osBack;
            break;
    }
    return state;
}
