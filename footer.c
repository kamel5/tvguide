#include <string>
#include "imageloader.h"
#include "tools.h"
#include "footer.h"

cFooter::cFooter(Button_Parameter_t bp) {
    dsyslog ("tvguide: %s %s %d %i\n", __FILE__, __func__,  __LINE__, bp.fromView);
    this->bp = bp;
    currentGroup = -1;
    channelGroupLast = -1;
    SetButtonPositions();
    buttonWidth = bp.width / 4 - 2 * geoManager.buttonBorder;
    buttonY = (bp.height - geoManager.buttonHeight) / 2;
    layer =((bp.fromView & fromSearchMenu) != 0) ? 6 : ((bp.fromView & fromRecMenu) != 0) ? 4 : 2;
    pixmapFooter = osdManager->CreatePixmap(__func__, "pixmapFooter", layer, cRect(bp.left,
                                                                                   bp.top,
                                                                                   bp.width,
                                                                                   bp.height));
    osdManager->Fill(pixmapFooter, bp.color);
    DrawFooter();
}

cFooter::~cFooter(void) {
    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    osdManager->DestroyPixmap(pixmapFooter);
}

void cFooter::UpdateGroupButtons(const cChannel *channel, bool force) {
    dsyslog ("tvguide: %s %s %d\n", __FILE__, __func__,  __LINE__);
    if (!channel)
        return;

    int group = bp.channelGroups->GetGroup(channel);
    if ((group != currentGroup) || force) {
        currentGroup = group;
        DrawFooter(group);
    }
}

void cFooter::SetButtonPositions(void) {
    for (int i=0; i < 4; i++) {
        positionButtons[i] = -1;
    }
    /*
    red button = 0
    green button = 1
    yellow button = 2
    blue button = 3
    */
    for (int button=0; button<4; button++) {
        if (Setup.ColorKey0 == button) {
            positionButtons[button] = 0;
            continue;
        }
        if (Setup.ColorKey1 == button) {
            positionButtons[button] = 1;
            continue;
        }
        if (Setup.ColorKey2 == button) {
            positionButtons[button] = 2;
            continue;
        }
        if (Setup.ColorKey3 == button) {
            positionButtons[button] = 3;
            continue;
        }
    }
}

void cFooter::Hide(void) {
    osdManager->SetLayer(pixmapFooter, -1);
}

void cFooter::Show(void) {
    osdManager->SetLayer(pixmapFooter, layer);
}

void cFooter::DrawButton(const char *text, tColor color, tColor borderColor, eOsdElementType buttonType, int num) {
    tColor colorTextBack = (config.style == eStyleFlat) ? color : clrTransparent;
    int left = num * buttonWidth + (2 * num + 1) * geoManager.buttonBorder;

    if ((config.style == eStyleBlendingMagick) || (config.style == eStyleBlendingDefault)) {
        cImageLoader imgLoader;
        imgLoader.CreateBackground(theme.Color(clrButtonBlend), color, buttonWidth - 4, geoManager.buttonHeight - 4);
        osdManager->DrawRectangle(pixmapFooter, cRect(left, buttonY, buttonWidth, geoManager.buttonHeight), borderColor);
        osdManager->DrawImage(pixmapFooter, cPoint(left + 2, buttonY + 2), imgLoader.GetImage());    
        if (config.roundedCorners) {
            int borderRadius = 12;
            int borderWidth = 2;
            DrawRoundedCorners(pixmapFooter, left, buttonY, buttonWidth, geoManager.buttonHeight, borderRadius, borderWidth, borderColor, bp.color);
        }
    } else if (config.style == eStyleGraphical) {
        cImage *button = imgCache->GetOsdElement(buttonType);
        if (button) {
            osdManager->DrawImage(pixmapFooter, cPoint(left, buttonY), *button);    
        }
    } else {
        osdManager->DrawRectangle(pixmapFooter, cRect(left, buttonY, buttonWidth, geoManager.buttonHeight), borderColor); 
        osdManager->DrawRectangle(pixmapFooter, cRect(left + 1, buttonY + 1, buttonWidth - 2, geoManager.buttonHeight - 2), color);
        if (config.roundedCorners) {
            int borderRadius = 12;
            int borderWidth = 1;
            DrawRoundedCorners(pixmapFooter, left, buttonY, buttonWidth, geoManager.buttonHeight, borderRadius, borderWidth, borderColor, bp.color);
        }
    }
    
    int textWidth = fontManager->FontButton->Width(text);
    int textHeight = fontManager->FontButton->Height();
    osdManager->DrawText(pixmapFooter, cPoint(left + (buttonWidth - textWidth) / 2, buttonY + (geoManager.buttonHeight - textHeight) / 2), text, theme.Color(clrFontButtons), colorTextBack, fontManager->FontButton);
}

void cFooter::ClearButton(int num) {
    int left = num * buttonWidth + (2 * num + 1) * geoManager.buttonBorder;
    osdManager->DrawRectangle(pixmapFooter, cRect(left, buttonY, buttonWidth, geoManager.buttonHeight), theme.Color(clrBackground)); //clrTransparent); 
}

void cFooter::DrawFooter(int currentGroup) {
    dsyslog ("tvguide: %s %s %d %i %i\n", __FILE__, __func__,  __LINE__, currentGroup, bp.fromView);
    std::string textGreen = "";
    std::string textYellow = "";
    std::string textRed = "";
    std::string textBlue = "";

    bool fromrecmenu =    ((bp.fromView & fromRecMenu) != 0);
    bool fromdetailview = ((bp.fromView & fromDetailView) != 0);
    bool fromsearchmenu = ((bp.fromView & fromSearchMenu) != 0);

    if (fromsearchmenu) {
        ClearButton(positionButtons[0]);
        ClearButton(positionButtons[1]);
        ClearButton(positionButtons[2]);
        ClearButton(positionButtons[3]);
        textRed = bp.a;
        textGreen = bp.b;
        textYellow = bp.c;
        textBlue = bp.d;
        }
    else if (fromrecmenu) {
        ClearButton(positionButtons[0]);
        ClearButton(positionButtons[1]);
        ClearButton(positionButtons[2]);
        ClearButton(positionButtons[3]);
        if (fromdetailview) {
            if (bp.isRecording)
                textBlue = trVDR("Button$Back");
	    else if (config.blueKeyMode == eBlueKeyEPG)
                textBlue = trVDR("Button$Back");
	    else if (config.blueKeyMode == eBlueKeyFavorites)
                textBlue = trVDR("Button$Switch");
        }
    } else {
        if (config.channelJumpMode == eNumJump || currentGroup < 0) {
            if (fromdetailview || (currentGroup >= 0)) {
                ClearButton(positionButtons[1]);
                ClearButton(positionButtons[2]);
            } else {
                textGreen = *cString::sprintf("%d %s", config.jumpChannels, tr("Channels back"));
                textYellow = *cString::sprintf("%d %s", config.jumpChannels, tr("Channels forward"));
            }
        } else if (config.channelJumpMode == eGroupJump) {
            if (currentGroup == channelGroupLast) {
                return;
            }
            ClearButton(positionButtons[1]);
            ClearButton(positionButtons[2]);
            channelGroupLast = currentGroup;
            textGreen = bp.channelGroups->GetPrevGroupName(currentGroup);
            textYellow = bp.channelGroups->GetNextGroupName(currentGroup);
        }
        ClearButton(positionButtons[0]);
        textRed = tr("Search & Rec");
        ClearButton(positionButtons[3]);
        if (config.blueKeyMode == eBlueKeySwitch) {
            textBlue = trVDR("Button$Switch");
        } else if (config.blueKeyMode == eBlueKeyEPG) {
            if (fromdetailview)
                textBlue = trVDR("Button$Back");
            else
                textBlue = tr("Detailed EPG");
        } else if (config.blueKeyMode == eBlueKeyFavorites) {
            if (fromdetailview)
                textBlue = trVDR("Button$Switch");
            else
                textBlue = tr("Favorites");
        }
    }

    DrawButton(*cString::sprintf("%s", textRed.c_str()), theme.Color(clrButtonRed), theme.Color(clrButtonRedBorder), oeButtonRed, positionButtons[0]);
    DrawButton(*cString::sprintf("%s", textGreen.c_str()), theme.Color(clrButtonGreen), theme.Color(clrButtonGreenBorder), oeButtonGreen, positionButtons[1]);
    DrawButton(*cString::sprintf("%s", textYellow.c_str()), theme.Color(clrButtonYellow), theme.Color(clrButtonYellowBorder), oeButtonYellow, positionButtons[2]);
    DrawButton(*cString::sprintf("%s", textBlue.c_str()), theme.Color(clrButtonBlue), theme.Color(clrButtonBlueBorder), oeButtonBlue, positionButtons[3]);
}
