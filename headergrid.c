#include "config.h"
#include "imageloader.h"
#include "tools.h"
#include "headergrid.h"

cHeaderGrid::cHeaderGrid(void) : cGridElement(NULL) {
    pixmap = NULL;
    pixmapLogo = NULL;
}

cHeaderGrid::~cHeaderGrid(void) {
    osdManager->DestroyPixmap(pixmapLogo);
}

void cHeaderGrid::CreateBackground(int num) {
    color = theme.Color(clrHeader);
    colorBlending = theme.Color(clrHeaderBlending);
    int x, y;
    if (config.displayMode == eVertical) {
        x = geoManager.timeLineWidth + num*geoManager.colWidth;
        y = geoManager.statusHeaderHeight + geoManager.channelGroupsHeight;
    } else if (config.displayMode == eHorizontal) {
        x = geoManager.channelGroupsWidth;
        y = geoManager.statusHeaderHeight + geoManager.timeLineHeight + num*geoManager.rowHeight;
    }
    pixmap = osdManager->CreatePixmap(__func__, "pixmap", 1, cRect(x, y, geoManager.channelLogoWidth, geoManager.channelLogoHeight));
    pixmapLogo = osdManager->CreatePixmap(__func__, "pixmapLogo", 2, cRect(x, y, geoManager.channelLogoWidth, geoManager.channelLogoHeight));
    if ((!pixmap) || (!pixmapLogo)){
        return;
    }
    osdManager->Fill(pixmapLogo, clrTransparent);
    if (config.style == eStyleGraphical) {
        DrawBackgroundGraphical(bgChannelHeader);
    } else {
        DrawBackground();
        DrawBorder();
    }
}

void cHeaderGrid::DrawChannel(const cChannel *channel) {
    if (config.displayMode == eVertical) {
        DrawChannelVertical(channel);
    } else if (config.displayMode == eHorizontal) {
        DrawChannelHorizontal(channel);
    }
}

// Draw Channel horizontal view

void cHeaderGrid::DrawChannelHorizontal(const cChannel *channel) {
    int logoWidth = geoManager.logoWidth;
    int logoX = config.displayChannelName ? 5 : (Width() - logoWidth) / 2;
    int textX = 5;
    int textY = (Height() - fontManager->FontChannelHeaderHorizontal->Height()) / 2;
    bool logoFound = false;
    if (!config.hideChannelLogos) {
        cImage *logo = imgCache->GetLogo(channel);
        if (logo) {
            const int logoheight = logo->Height();
            const int logowidth = logo->Width();
            osdManager->DrawImage(pixmapLogo, cPoint(logoX + ((logoWidth - logowidth) / 2), (Height() - logoheight) / 2), *logo);
            logoFound = true;
        }
    }
    bool drawText = false;
    int textWidthMax = Width() - 10;
    if (!logoFound) {
        drawText = true;
    }
    if (config.displayChannelName) {
        drawText = true;
        textX += logoWidth + 5;
        textWidthMax -= textX;
    }
    if (drawText) {
        tColor colorTextBack = (config.style == eStyleFlat)?color:clrTransparent;
        cString strChannel = cString::sprintf("%d %s", channel->Number(), channel->Name());
        cString cuttedStrChannel = cString::sprintf("%s", CutText(*strChannel, textWidthMax, fontManager->FontChannelHeaderHorizontal).c_str());
        osdManager->DrawText(pixmap, cPoint(textX, textY), *cuttedStrChannel, theme.Color(clrFontHeader), colorTextBack, fontManager->FontChannelHeaderHorizontal);
    }
}

// Draw Channel vertical view

void cHeaderGrid::DrawChannelVertical(const cChannel *channel) {
    int logoHeight = geoManager.logoHeight;
    cTextWrapper tw;
    cString headerText = cString::sprintf("%d - %s", channel->Number(), channel->Name());
    tw.Set(*headerText, fontManager->FontChannelHeader, geoManager.colWidth - 8);
    int lines = tw.Lines();
    int lineHeight = fontManager->FontChannelHeader->Height();
    int yStart = (geoManager.channelHeaderHeight - lines * lineHeight) / 2 + 8;
    bool logoFound = false;
    if (!config.hideChannelLogos) {
        cImage *logo = imgCache->GetLogo(channel);
        if (logo) {
            const int logoheight = logo->Height();
            const int logowidth = logo->Width();
            osdManager->DrawImage(pixmapLogo, cPoint((Width() - logowidth) / 2, (logoHeight - logoheight) / 2), *logo);
            logoFound = true;
        }
    }
    bool drawText = false;
    if (!logoFound) {
        drawText = true;
    } else if (config.displayChannelName) {
        drawText = true;
        yStart = logoHeight;
    }
    if (!drawText)
        return;
    tColor colorTextBack = (config.style == eStyleFlat)?color:clrTransparent;
    for (int i = 0; i < lines; i++) {
        int textWidth = fontManager->FontChannelHeader->Width(tw.GetLine(i));
        int xText = (geoManager.colWidth - textWidth) / 2;
        if (xText < 0) 
            xText = 0;
        osdManager->DrawText(pixmap, cPoint(xText, yStart + i * lineHeight), tw.GetLine(i), theme.Color(clrFontHeader), colorTextBack, fontManager->FontChannelHeader);
    }
}

void cHeaderGrid::SetPosition(int num) {
    int x, y, width, height;
    if (config.displayMode == eVertical) {
        x = geoManager.timeLineWidth + num*geoManager.colWidth;
        y = geoManager.statusHeaderHeight + geoManager.channelGroupsHeight;
        width = geoManager.colWidth;
        height = geoManager.channelHeaderHeight;
    } else if (config.displayMode == eHorizontal) {
        x = geoManager.channelGroupsWidth;
        y = geoManager.statusHeaderHeight + geoManager.timeLineHeight + num*geoManager.rowHeight;
        width = geoManager.channelHeaderWidth;
        height = geoManager.rowHeight;
    }
    osdManager->SetViewPort(pixmap, cRect(x, y, width, height));
    osdManager->SetViewPort(pixmapLogo, cRect(x, y, width, height));
}
